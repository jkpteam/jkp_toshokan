
<?php
  require_once 'assets/data/collaborations_data.php'
?>

<h1>Tankoubon Collaborations</h1>
<span class="pageinfo">This page showcases JKP's collaboration works with other artists. These were included as bonus material for the corresponding release.</span>

<?php renderCollaborationsList($contents) ?>

<h3>Additional info:</h3>
<p>Nyacozo <a href="https://twitter.com/nyacozo/status/1039040709897084931">twitter post</a>.</p>