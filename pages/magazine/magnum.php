<?php
  require_once 'assets/data/magnum_data.php'
?>

<h1><img src="/assets/images/mangalist/magnumlogo.png" title="magnumlogo" id="stamp" style="display: inline;"> Comic Magnum</h1>
<p>This section lists every comic issues JKP has done for COMIC Magnum magazine.</p>

<h3>Corresponding Tankoubon Volumes</h3>
<div class="cgwrapper">
    
		<div class="cgcenter">
				<div class="tankimgsmall"><a href="/tankoubon/maison"><img class="img3" src="/assets/images/tank/maison.jpg"></a></div>
                <div class="tankimgsmall"><a href="/tankoubon/kimochi"><img class="img3" src="/assets/images/tank/kimochi.jpg"></a></div>
	</div>
</div>
<div class="clear"></div>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>

<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Magnum which featured JKP comics. You can click on the pictures to see a larger version.</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>