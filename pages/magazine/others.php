<h1 class="page_title">Earlier Pre-Monzetsu Works</h1>

<!-- Magazine List -->
<table class="manga">

        <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Tankoubon Release</th>
        </tr>

	<tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Genocide.png">ジェノサイド<br>Genocide</a></td>
			<td><div class="mag-date">Young Magazine 1998? award</div></td>
			<td class="tank">not released yet</td>
     </tr>

    <tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Cherry_Cherrin.png">チェリーチェリーン<br>Cherry Cherrin</a></td>
			<td><div class="mag-date">Bessatsu Young Magazine Debut 1999?</div></td>
			<td class="tank">not released yet</td>
   </tr>

   <tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Blowback.png">駅前ブローバック<br>Blowback in front of the station</a></td>
			<td><div class="mag-date">Bessatsu Young Magazin 2000?</div></td>
			<td class="tank">ジョン・Ｋ・ペー太の世界<br>John K. Pe-Ta's World</td>
     </tr>

    <tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Chaotic_Cohabitation.png">カオス同棲<br>Chaotic Cohabitation</a></td>
			<td><div class="mag-date">Bessatsu Young Magazin 2000?</div></td>
			<td class="tank">ジョン・Ｋ・ペー太の世界<br>John K. Pe-Ta's World</td>
  </tr>

   <tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Gizzard.png">死霊のすなぎも<br>Gizzard of the Departed</a></td>
			<td><div class="mag-date">Bessatsu Young Magazin 2001?</div></td>
			<td class="tank">ジョン・Ｋ・ペー太の世界<br>John K. Pe-Ta's World</td>
  </tr>

      <tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Mushroom.png">キノコでポン<br>Mushroom Plop</a></td>
			<td><div class="mag-date">Bessatsu Young Magazin 2001?</div></td>
			<td class="tank">ジョン・Ｋ・ペー太の世界<br>John K. Pe-Ta's World</td>
  </tr>

   <tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Jet_Kentauros.png">ジェット・ケンタウロス<br>Jet Kentauros</a></td>
			<td><div class="mag-date">Bangaichi 2001 hentai debut ?</div></td>
			<td class="tank">ジョン・Ｋ・ペー太の世界<br>John K. Pe-Ta's World</td>
  </tr>

    <tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Water_Girl.png">ウォーターガール<br>Water Girl</a></td>
			<td><div class="mag-date">test to jumbo 2001</div></td>
			<td class="tank">not released yet</td>
  </tr>

    <tr>
			<td class="cim"><a class="nagyobb" href="/assets/images/mangalist/others/Flower_Academy.png">花がら学園Z<br>Flower Academy Z</a></td>
			<td><div class="mag-date">COMIC Jumbo No.173 2002/05</div></td>
			<td class="tank">not released yet</td>
  </tr>

</table>