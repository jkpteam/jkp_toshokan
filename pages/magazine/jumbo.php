<h1 class="page_title">COMIC Jumbo list</h1>
<p>This section lists every manuscript JKP has done for COMIC Jumbo magazine.</p>
<!-- Magazine List -->
	<table class="manga">
        <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        </tr>
      
		<tr>
			<td class="cim">花がら学園Z<br>Flower Academy Z</td>
			<td><div class="mag-date">COMIC Jumbo No.173 2002/05</div></td>
			<td class="tank">-</td>
		</tr>
		
		<tr>
			<td class="cim">ミラクル女子銅製ヒナ子<br>????</td>
			<td><div class="mag-date">COMIC Jumbo No.176 2002/08</div></td>
			<td class="tank">-</td>
		</tr>
        
		<tr>
			<td class="cim">となりの肉<br>My Neighbor's Meat</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>
		
		<tr>
			<td class="cim">湯けむり悩殺フィーバー<br>Steam - Bewitchment Fever</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>
        
		<tr>
			<td class="cim">オッパイ空手ミルク拳<br>Breast Karate, Milk Fist</td>
			<td><div class="mag-date">COMIC Jumbo No.183<br>2003/03</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>	

		<tr>
			<td class="cim">肛開モデル悶絶部長<br>Open Ass Model - Fainting Club Captain</td>
			<td><div class="mag-date">COMIC Jumbo No.184<br>2003/04</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>
    
 		<tr>
			<td class="cim">スパルタ指令<br>Spartacus Games - Directive 076</td>
			<td><div class="mag-date">COMIC Jumbo No.185<br>2003/05</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>
        
		<tr>
			<td class="cim">熱血！悶絶！しごき部長<br>Hot Blooded! Fainting in Agony! The Hard Working Captain</td>
			<td><div class="mag-date">COMIC Jumbo No.187<br>2003/07</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>
		
		<tr>
			<td class="cim">直球！悩殺オペレーション<br>Straight Pitch! Bewithchment Operation</td>
			<td><div class="mag-date">COMIC Jumbo No.188<br>2003/08</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>

		<tr>
			<td class="cim colored">第3次爆乳サミットα<br>Three Part Milk Explosion Summit Alpha</td>
			<td class="magdate colored"><div class="mag-date">COMIC Jumbo No.189<br>2003/09</div></td>
			<td class="tank colored">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
        </tr>
        
		<tr>
			<td class="cim">妹地獄ピンク風呂<br>Sister's Pink Bath Hell</td>
			<td><div class="mag-date">COMIC Jumbo No.190 2003/10</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim">拘束.肛虐.パパラッチ<br>Restraint, Anal Tyranny, Paparazzi</td>
			<td><div class="mag-date">COMIC Jumbo No.191 2003/11</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim">狂乱！泥酔行進曲<br>Frenzy! Dead Drunk March</td>
			<td><div class="mag-date">COMIC Jumbo No.192 2003/12</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim">直撃！！悶絶地獄剣<br>Direct hit!! Fainting in Agony by the Hellish Sword</td>
			<td><div class="mag-date">COMIC Jumbo No.193 2004/01</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim">薔薇十字に揉みしだけ！！<br>The Rosicrucian Massage!!</td>
			<td><div class="mag-date">COMIC Jumbo No.194 2004/02</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim">禁断のミックスジュース<br>Forbidden Juice Mix</td>
			<td><div class="mag-date">COMIC Jumbo No.195 2004/03</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim">サクラチルチル<br>Cherry Blossoms Scatter</td>
			<td><div class="mag-date">COMIC Jumbo No.196 2004/04</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim">放課後コケシ固め<br>Hard After - School Doll</td>
			<td><div class="mag-date">COMIC Jumbo No.197 2004/05 </div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim colored">桃色リベンジ祭り<br>Pink Revenge Festival</td>
			<td class="magdate colored"><div class="mag-date">COMIC Jumbo No.198 2004/06</div></td>
			<td class="tank colored">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr>
			<td class="cim">ガチん娘レッスン乱れ責め<br>Submissive Girl Lesson, Disorderly Punishment</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>		
		
		<tr>
			<td class="cim">ミルク極道下克上縛り<br>Slave Tied Milky Mistress</td>
			<td><div class="mag-date">COMIC Jumbo No.200 2004/08</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>

		<tr>
			<td class="cim">地獄温泉羞恥の湯<br>Hell Spa, Hot Bath of Shame</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>

		<tr>
			<td class="cim">ぬきうち！屈辱Vの字斬り<br>Sudden Attack! Letter V of Disgrace</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr> 		
		
		<tr>
			<td class="cim">桃乳！！欲情スピリッツ<br>Peach Breasts!! Desire Spirits</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>
		
		<tr>
			<td class="cim">屈辱の桃尻チャレンジ<br>Peach Ass Challenge of Disgrace</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>
		
		<tr>
			<td class="cim">凌辱陰陽観音開き<br>Double Doors Exorcist Raper</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>
		
		
		<tr>
			<td class="cim">惨敗おしおきMAX！！<br>Humiliating Punishment MAX!!</td>
			<td><div class="mag-date">COMIC Jumbo No.206 2005/02</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>
		
		<tr>
			<td class="cim">Let's秘密喫茶<br>Let's Secret Coffee Shop</td>
			<td><div class="mag-date">COMIC Jumbo No.207 <br>2005.03</div></td>
			<td class="tank">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>		


		<tr>
			<td class="cim">スリーピング突撃ゲリラ<br>Sleeping Assault Guerrillas</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>
		
		
 		<tr>
			<td class="cim">挑発ジェットストリーム<br>Arousal Jetstream</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>

		<tr>
			<td class="cim">禁断惑星W・C<br>Forbidden Plant W.C.</td>
			<td><div class="mag-date">COMIC Jumbo No.209 2005/05</div></td>
			<td class="tank">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>

		<tr>
			<td class="cim">つまみ出し電撃バージン<br>In Other Words, The Electric Shock Virgin</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>

		<tr>
			<td class="cim">絶倫ゴールドラッシュ<br>Peerless: Goldrush</td>
			<td><div class="mag-date">COMIC Jumbo No.214<br>2005-10</div></td>
			<td class="tank">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>

		<tr>
			<td class="cim">熱中！快感アディクト<br>Mania! Pleasure Addict</td>
			<td><div class="mag-date">COMIC Jumbo No.212 2005/08</div></td>
			<td class="tank">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>

		<tr>
			<td class="cim colored">逆襲シャインスパーク<br>Counterattack: Shine Spark</td>
			<td class="magdate colored"><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank colored">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>

		<tr>
			<td class="cim">第1話 悶絶フルオープン<br>#1 Monzetsu Fullopen</td>
			<td><div class="mag-date">COMIC Jumbo No.215 2005/11</div></td>
			<td class="tank">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>

		<tr>
			<td class="cim">第2話 討ち入りジェットデビュー<br>#2 Raid Jet Debut</td>
			<td><div class="mag-date">COMIC Jumbo No.216 2005/12</div></td>
			<td class="tank">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>

		<tr>
			<td class="cim">第3話 ナチュラルボーンエロス<br>#3 Natural Born Eros</td>
			<td><div class="mag-date">COMIC Jumbo No.217 2006/01</div></td>
			<td class="tank">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>

		<tr>
			<td class="cim">第4話 いただきデスレース<br>#4 Easy Win Death Race</td>
			<td><div class="mag-date">COMIC Jumbo No.218 2006/02</div></td>
			<td class="tank">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>

		<tr>
			<td class="cim">第5話僕たちの挽歌<br>#5 Our Elegy</td>
			<td><div class="mag-date">COMIC Jumbo No.219 2006/03</div></td>
			<td class="tank">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>

		<tr>
			<td class="cim colored">第6話 ブラックファックンロール<br>#6 Black Fuck 'N' roll</td>
			<td class="magdate colored"><div class="mag-date">COMIC Jumbo No.220 2006/04</div></td>
			<td class="tank colored">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>

			<tr>
			<td class="cim">第7話サヨナラニッポン<br>#7 Farewell Japan</td>
			<td><div class="mag-date">COMIC Jumbo No.221 2006/05</div></td>
			<td class="tank">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>

		<tr>
			<td class="cim">撃ち出せ！俺の未練スパーク!!<br>Shoot out! My Long lost Spark!</td>
			<td><div class="mag-date">COMIC Jumbo No.222 2006/06</div></td>
			<td class="tank">トキメキ悶絶バルカン!!<br>Tokimeki Fainting in Agony Balkan!!</td>
		</tr>

		<tr>
			<td class="cim">激烈！！エキサイティングストア<br>Furious!! Exciting Store!</td>
			<td><div class="mag-date">COMIC Jumbo No.223 2006/07 </div></td>
			<td class="tank">轟け！！悶絶スクリーマー<br>Roar!! Fainting in Agony Screamer</td>
		</tr>

		<tr>
			<td class="cim">ラブジェット10000<br>Love Jet 10 000</td>
			<td><div class="mag-date">COMIC Jumbo No.225 2006/09</div></td>
			<td class="tank">轟け！！悶絶スクリーマー<br>Roar!! Fainting in Agony Screamer</td>
		</tr>

		<tr>
			<td class="cim colored">青春デストロイヤー<br>Youth Destroyer</td>
			<td class="magdate colored"><div class="mag-date">COMIC Jumbo No.227 2006/11</div></td>
			<td class="tank colored">轟け！！悶絶スクリーマー<br>Roar!! Fainting in Agony Screamer</td>
		</tr>
	</table>


<!-- Cover Gallery -->
<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Jumbo which featured JKP comics. Illustrations were done by <b>Hiroshi Kawamoto</b>. These cover pictures were compiled in a release called <b>Daiyaboureki</b>.</p>

	<div class="cgwrapper">
		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-02.jpg" alt="cover" title="cover"><br>2002/02<br>?</div>
        </div>
            
		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-06.jpg" alt="cover" title="cover"><br>2002/06<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-07.jpg" alt="cover" title="cover"><br>2002/07<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-08.jpg" alt="cover" title="cover"><br>2002/08<br>JKP</div>
        </div>

		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-10.jpg" alt="cover" title="cover"><br>2002/10<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-12.jpg" alt="cover" title="cover"><br>2002/12<br>?</div>
        </div>

 		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-02.jpg" alt="cover" title="cover"><br>2003/02<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-03.jpg" alt="cover" title="cover"><br>2003/03<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-05.jpg" alt="cover" title="cover"><br>2003/05<br><!-- Spartacus Games - Directive 076 -->?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-06.jpg" alt="cover" title="cover"><br>2003/06<br>?</div>
        </div>

 		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-07.jpg" alt="cover" title="cover"><br>2003/07<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-08.jpg" alt="cover" title="cover"><br>2003/08<br>?</div><!-- Straight Pitch! Bewithchment Operation -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-09.jpg" alt="cover" title="cover"><br>2003/09<br></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-10.jpg" alt="cover" title="cover"><br>2003/10<br><!--Sister's Pink Bath Hell--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-11.gif" alt="cover" title="cover"><br>2003/11<br><!--Restraint, Anal Tyranny, Paparazzi--></div>
        </div>

 		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-12.jpg" alt="cover" title="cover"><br>2003/12<br><!--Frenzy! Dead Drunk March--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-01.jpg" alt="cover" title="cover"><br>2004/01<br><!--Direct hit!! Fainting in Agony by the Hellish Sword--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-02.jpg" alt="cover" title="cover"><br>2004/02<br><!--The Rosicrucian Massage!--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-03.jpg" alt="cover" title="cover"><br>2004/03<br><!--Forbidden Juice Mix--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-04.jpg" alt="cover" title="cover"><br>2004/04<br><!--Cherry Blossoms Scatter--></div>
        </div>

 		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-05.jpg" alt="cover" title="cover"><br>2004/05<br><!--Hard After - School Doll--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-06.jpg" alt="cover" title="cover"><br>2004/06<br><!--Pink Revenge Festival--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-07.jpg" alt="cover" title="cover"><br>2004/07<br>?</div>
            <div class="kepszoveg"><a class="nagyobb" href="/assets/images/mangalist/jumbo/2004-08.jpg"><img src="/assets/images/mangalist/jumbo/2004-08.jpg" alt="cover" title="cover"></a><br>2004/08<br><!--Slave Tied Milky Mistress--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-09.jpg" alt="cover" title="cover"><br>2004/09<br>?</div>
        </div>

  		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-10.jpg" alt="cover" title="cover"><br>2004/10<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-11.jpg" alt="cover" title="cover"><br>2004/11<br><!--Pink Revenge Festival--></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-12.jpg" alt="cover" title="cover"><br>2004/12<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-01.jpg" alt="cover" title="cover"><br>2005/01<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-02.jpg" alt="cover" title="cover"><br>2005/02<br><!--Humiliating Punishment MAX!!--></div>
        </div>

 		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-03.jpg" alt="cover" title="cover"><br>2005/03<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-04.jpg" alt="cover" title="cover"><br>2005/04<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-05.jpg" alt="cover" title="cover"><br>2005/05<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-06.jpg" alt="cover" title="cover"><br>2005/06<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-07.jpg" alt="cover" title="cover"><br>2005/07<br>?</div>
        </div>

  		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-08.jpg" alt="cover" title="cover"><br>2005/08<br></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-09.jpg" alt="cover" title="cover"><br>2005/09<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-10.jpg" alt="cover" title="cover"><br>2005/10<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-11.jpg" alt="cover" title="cover"><br>2005/11<br></div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-12.jpg" alt="cover" title="cover"><br>2005/12<br></div>
        </div>

  		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-01.jpg" alt="cover" title="cover"><br>2006/01<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-02.jpg" alt="cover" title="cover"><br>2006/02<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-03.jpg" alt="cover" title="cover"><br>2006/03<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-04.jpg" alt="cover" title="cover"><br>2006/04<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-05.jpg" alt="cover" title="cover"><br>2006/05<br>?</div>
        </div>

  		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-06.jpg" alt="cover" title="cover"><br>2006/06<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-07.jpg" alt="cover" title="cover"><br>2006/07<br>?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-09.jpg" alt="cover" title="cover"><br>2006/09<br></div>
        </div>

   		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-11.jpg" alt="cover" title="cover"><br>2006/11<br><!--Humiliating Punishment MAX!!--></div>
        </div>
</div>
<div class="clear"></div>

<br>
<span class="marker">
<h3>Notes:</h3>
<p>In Comic Jumbo No. 184 (2003/04) table of contents the story's title is オシリ美術部 (Oshiri bijutsu-bu) that means Butt Art Department. But in the Tankoubon version the title changed to 肛開モデル悶絶部長.
<img src="/assets/images/mangalist/jumbo/toc200304.png" alt="toc" title="toc">
    </p>
<p>These stories had no Tankoubon format prints made, they were only available in the magazine printing from Jumbo no.173 and 176.</p>
            <ul>
            <li>花がら学園Z - Flower Academy Z</li>
            <li>ミラクル女子銅製ヒナ子 - ????</li>
        </ul>
</span>

