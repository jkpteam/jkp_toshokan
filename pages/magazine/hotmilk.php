<?php
  require_once 'assets/data/hotmilk_data.php'
?>

<h1>COMIC Hotmilk</h1>
<p>This section lists every comic issues JKP has done for COMIC Hotmilk magazine by release dates. At the bottom of the list you can browse through these magazines cover illustrations including the name of the illustrator</p>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>

<br>
<span class="marker">
<h3>Notes:</h3>
<p>Henshin Switch [変身スイッチ] from 2020. Vol. 07 was also released in COMIC Megastore Deep Vol. 23</p>
</span>


<!-- Cover Gallery -->
<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Hotmilk which featured JKP comics. Illustration were done by illustrator Bota Mochito [牡丹もちと].</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>