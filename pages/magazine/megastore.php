<?php
  require_once 'assets/data/megastore_data.php'
?>

<h1>COMIC Megastore</h1>
<p>This section lists every comic issues JKP has done for COMIC Megastore magazine by release dates. At the bottom of the list you can browse through these magazines cover illustrations including the name of the illustrator.</p>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>


<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of COMIC Megastore which featured JKP comics. Illustrations were done by illustrators ebecus omom (腿之助兵衛) and Kizuki Aruchu.</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>