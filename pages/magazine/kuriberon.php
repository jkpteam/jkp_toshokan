<?php
  require_once 'assets/data/kuriberon_data.php'
?>

<h1>COMIC Kuriberon list</h1>
<p>This section lists every comic issues JKP has done for Comic Kuriberon magazine.</p>

<h3>Corresponding Tankoubon Volumes</h3>
<div class="cgwrapper">
    
		<div class="cgcenter">
				<div class="tankimgsmall"><a href="/tankoubon/opportunity"><img class="img3" src="/assets/images/tank/opportunity.jpg"></a></div>
				<div class="tankimgsmall"><a href="/tankoubon/freestyle"><img class="img3" src="/assets/images/tank/freestyle.jpg"></a></div>
	</div>
</div>
<div class="clear"></div>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>

<!-- Cover Gallery -->
<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Kuriberon which featured JKP comics.</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>
<!---
Work in Progress Photos
<h2 class="subtitle">Work in Progress Photos</h2>
<p>After JKP opened up his twitter account, he started posting photos of his upcoming titles. This section showcases these.</p>
<span class="wipp">
<h3>Vol.52 - Empress Caligula</h3>
    <p>Original location is <a href="https://twitter.com/jkp55645/status/811809810475687937">here</a> and <a href="https://twitter.com/jkp55645/status/816835459141640192">here</a>.</p>
                <a class="nagyobb" href="/assets/images/mangalist/kuriberon/ch/w/kb52_w1.jpg">
                <img src="/assets/images/mangalist/kuriberon/ch/w/kb52_w1.jpg" alt="1" title="1"></a>
                <a class="nagyobb" href="/assets/images/mangalist/kuriberon/ch/w/kb52_w2.jpg">
                <img src="/assets/images/mangalist/kuriberon/ch/w/kb52_w2.jpg" alt="2" title="2"></a>
                <a class="nagyobb" href="/assets/images/mangalist/kuriberon/ch/w/kb52_w3.jpg">
                <img src="/assets/images/mangalist/kuriberon/ch/w/kb52_w3.jpg" alt="3" title="3"></a>
</span>
 -->
