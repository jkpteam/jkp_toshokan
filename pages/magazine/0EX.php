<?php
  require_once 'assets/data/0ex_data.php'
?>

<h1 class="page_title"><img src="/assets/images/mangalist/exlogo.png" title="0exlogo" id="stamp" style="display: inline;"> COMIC 0EX list</h1>
<p>This section lists every comic issues JKP has done for COMIC 0EX magazine by release dates.</p>

<h3>Corresponding Tankoubon Volumes</h3>
<div class="cgwrapper">
    
<div class="cgcenter">
    <div class="tankimgsmall"><a href="/tankoubon/balkan">
        <img class="img3" src="/assets/images/tank/balkan.jpg"></a></div>
    <div class="tankimgsmall"><a href="/tankoubon/monfest">
        <img class="img3" src="/assets/images/tank/mofesu.jpg"></a></div>
    <div class="tankimgsmall"><a href="/tankoubon/megabitch">
        <img class="img3" src="/assets/images/tank/mega_bitch.jpg"></a></div>
    <div class="tankimgsmall"><a href="/tankoubon/destroy">
        <img class="img3" src="/assets/images/tank/mon_destroy.jpg"></a></div>
    <div class="tankimgsmall"><a href="/tankoubon/operationplus">
        <img class="img3" src="/assets/images/tank/operation_plus.jpg"></a></div>
    <div class="tankimgsmall"><a href="/tankoubon/explosionplus">
        <img class="img3" src="/assets/images/tank/explosion_plus.jpg"></a></div>
 </div>
</div>
<div class="clear"></div>


<span class="marker">
<h3>Notes:</h3>
	<p><a name="comment1"></a>
		<b>1. Danger Saint 24</b><br>
In the original magazine printing the title was mistyped. An earlier story's name titled <b>"The High-tension Crazy driving!!"</b> was given to it. This was corrected in the tankobon release <a href="/tankoubon/balkan">Monzetsu Balkan</a> to the correct title.</p>
<div style="text-align:center;">
<img src="/assets/images/mangalist/ex/ch/dangersaint_1.png" alt="dangersaint" title="dangersaint">
<img src="/assets/images/arrow.png" alt="arrow" title="arrow">
<img src="/assets/images/mangalist/ex/ch/dangersaint_2.png" alt="dangersaint" title="dangersaint">
</div>

	<p><a name="comment2"></a>
		<b>2. Summer Riot'08</b><br>
In the original magazine printing the title was named <b>Summer Riot'08</b> but this was later changed in the tankoubon release <a href="/tankoubon/monfest">Monfest XI</a> to the title Summer Riot'09.</p>
	
<div style="text-align:center;">
<img src="/assets/images/mangalist/ex/ch/summerriot08.png" alt="summerriot08" title="summerriot08">
<img src="/assets/images/arrow.png" alt="arrow" title="arrow">
<img src="/assets/images/mangalist/ex/ch/summerriot09.png" alt="summerriot09" title="summerriot09">
</div>
	
<p><a name="comment3"></a><b>3. Ruinous Catastrophe</b><br>
On the <a href="http://www.coremagazine.co.jp/zeroex/zeroex16.html">official website</a> there was a typo, they have typed the word "ochifurete" (落ちぶれて) with a kanji (落) instead of the kana version what was on the title page.</p>
	
<div style="text-align:center;">
<img src="/assets/images/mangalist/ex/ch/ruinousc.png" alt="ruinousc" title="ruinousc">
</div>
	
<p><a name="comment4"></a><b>4. The soul's Blasting Scrum!!</b><br>
In the original magazine printing the title was misprinted. The word 逆襲 - gyakushuu (english: counterattack) replaced the original word 魂 - tamashii (english: spirit). This was corrected in the tankoubon release.</p>
	
<div style="text-align:center;">
<img src="/assets/images/mangalist/ex/ch/scrum_1.png" alt="scrum" title="scrum">
<img src="/assets/images/arrow.png" alt="arrow" title="arrow">
<img src="/assets/images/mangalist/ex/ch/scrum_2.png" alt="scrum" title="scrum">
</div>

<p><a name="comment5"></a><b>5. 0EX Vol.29</b><br>
	In the issue of 0EX Volume 29 there was a color advertisement to promote the new tankoubon <a href="/tankoubon/megabitch">Super Monzetsu Mega Bitch</a>. This illustration is later used for the cover of that tankobon.</p>
	
	<div class="cgwrapper">
		<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/ex/ex29_c.jpg"><img src="/assets/images/mangalist/ex/ex29_c.jpg" alt="ex29_c" title="ex29_c"></a></div>
	<div class="clear"></div>
		</div>
	</div>
</span>


<table class="manga">
    <tr>
        <th>Japanese/English Title</th>
        <th>Original Publication</th>
        <th>Corresponding Tankoubon</th>
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>

<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic 0EX which featured JKP comics. From volume 1 till 8 illustrations were done by Hentai Mangaka <a href="http://www.nixinamo-lens.jp">NIXinamo：LENS</a>. From Volume 9 Hentai Mangaka and illustrator <a href="http://glam.x0.com/glam/">Komiya Yuuta</a> was put in charge of the covers until the end of the magazine. You can click on the pictures to see a larger version.</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>


<h2 class="subtitle">TOC Comments</h2>
<p>Most magazines contains a Table of Contents page (TOC) where the authors can write a few words for the fans. These comments are usually not too interesting or informative, but they show some inside of the author's everyday life.</p>

<table class="comment">
    <?php renderMagazineComment($contents) ?>       
</table>