
<?php
  require_once 'assets/data/megastoredeep_data.php'
?>

<img src="/assets/images/mangalist/megastoredlogo.png" title="msd" id="stamp" style="display: inline;">
<h1>COMIC Megastore Deep</h1>
<p>This section lists every comic issues JKP has done for COMIC Megastore Deep magazine by release dates. At the bottom of the list you can browse through these magazines cover illustrations including the name of the illustrator.</p>

<!-- Magazine List -->

<h3>Corresponding Tankoubon Volumes</h3>
<div class="cgwrapper">
    
		<div class="cgcenter">
				<div class="tankimgsmall"><a href="/tankoubon/acmate"><img class="img3" src="/assets/images/tank/acmate.jpg"></a><br>Acmate</div>
                <div class="tankimgsmall"><a href="/tankoubon/mesuana"><img class="img3" src="/assets/images/tank/mesuana.jpg"></a><br>Mesuana</div>
                <div class="tankimgsmall"><a href="/tankoubon/anameito"><img class="img3" src="/assets/images/tank/anameito.jpg"></a><br>Anameito</div>
	   </div>
</div>
<div class="clear"></div>

<span class="marker">
<h3>Notes:</h3>
<p>Henshin Switch [変身スイッチ] from Vol. 23 was also released in COMIC Hotmilk 2020. Vol. 07.</p>
</span>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>

<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Megastore Deep which featured JKP comics. These illustrations are done by <a href="https://twitter.com/kotoyoshi_y">Yumisuke Kotoyoshi</a> until Volume 17.</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>

<!-- Cover Illustrations -->
<!--
<h2 class="subtitle">Cover Illustration Gallery</h2>
<p>This section showcases cover illustrations of Comic Megastore Deep which featured JKP comics. These illustrations are done by <a href="https://twitter.com/kotoyoshi_y">Yumisuke Kotoyoshi</a>.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kepszoveg">
                    <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/msd001c.jpg">
                        <img src="/assets/images/mangalist/megastoredeep/msd001c.jpg" alt="Megastore Deep Volume 01 Cover" title="Megastore Deep Volume 01 Cover"></a><br>Volume 01</div>
				<div class="kepszoveg">
                    <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/msd002c.jpg">
                        <img src="/assets/images/mangalist/megastoredeep/msd002c.jpg" alt="Megastore Deep Volume 02 Cover" title="Megastore Deep Volume 02 Cover"></a><br>Volume 02</div>
				<div class="kepszoveg">
                    <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/msd003c.jpg">
                        <img src="/assets/images/mangalist/megastoredeep/msd003c.jpg" alt="Megastore Deep Volume 03 Cover" title="Megastore Deep Volume 03 Cover"></a><br>Volume 03</div>
				
                <div class="kepszoveg">
                    <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/msd005.jpg">
                        <img src="/assets/images/mangalist/megastoredeep/msd005.jpg" alt="Megastore Deep Volume 05 Cover" title="Megastore Deep Volume 05 Cover"></a><br>Volume 05</div>
                <div class="kepszoveg">
                    <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/msd007.jpg">
                        <img src="/assets/images/mangalist/megastoredeep/msd007.jpg" alt="Megastore Deep Volume 07 Cover" title="Megastore Deep Volume 07 Cover"></a><br>Volume 07</div>

		<div class="clear"></div>
			</div>
</div>
-->
<!-- Work in Progress Photos 
<h2 class="subtitle">Work in Progress Photos</h2>
<p>After JKP opened up his twitter account, he started posting photos of his upcoming titles. This section showcases these.</p>
<span class="wipp">
<h3>Megastore Deep Vol. 05 - Ushimitsu Bomber</h3>
    <p>Page 2 and Page 5.</p>
                <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/ch/w/msd005-1.jpg">
                <img src="/assets/images/mangalist/megastoredeep/ch/w/msd005-1.jpg" alt="1" title="Page 2 WIP"></a>
                <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/ch/w/msd005-2.jpg">
                <img src="/assets/images/mangalist/megastoredeep/ch/w/msd005-2.jpg" alt="2" title="Page 5 WIP"></a>
                <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/ch/w/msd005-3.jpg">
                <img src="/assets/images/mangalist/megastoredeep/ch/w/msd005-3.jpg" alt="3" title="Page 14 WIP"></a>
</span>


<span class="wipp">
<h3>Megastore Deep Vol. 07 - Kemonist Family</h3>
    <p>Characters and page 1-2.</p>
                <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/ch/w/msd007-1.jpg">
                <img src="/assets/images/mangalist/megastoredeep/ch/w/msd007-1.jpg" alt="1" title="Kemonist Family - Character Design 1"></a>
                <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/ch/w/msd007-2.jpg">
                <img src="/assets/images/mangalist/megastoredeep/ch/w/msd007-2.jpg" alt="2" title="Kemonist Family - Character Design 2"></a>
                <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/ch/w/msd007-3.jpg">
                <img src="/assets/images/mangalist/megastoredeep/ch/w/msd007-3.jpg" alt="3" title="Kemonist Family - Character Design 3"></a>
                <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/ch/w/msd007-4.jpg">
                <img src="/assets/images/mangalist/megastoredeep/ch/w/msd007-4.jpg" alt="4" title="Kemonist Family - Page 1 WIP"></a>
                <a class="nagyobb" href="/assets/images/mangalist/megastoredeep/ch/w/msd007-5.jpg">
                <img src="/assets/images/mangalist/megastoredeep/ch/w/msd007-5.jpg" alt="5" title="Kemonist Family - Page 1 WIP"></a>
</span>
-->