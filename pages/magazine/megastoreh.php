<?php
  require_once 'assets/data/megastoreh_data.php'
?>

<h1><img src="/assets/images/mangalist/mshlogo.png" title="mshlogo" id="stamp" style="display: inline;"> COMIC Megastore H</h1>
<p>This section lists every comic issues JKP has done for COMIC Megastore H magazine.</p>

<h3>Corresponding Tankoubon Volumes</h3>
<div class="cgwrapper">
    
		<div class="cgcenter">
                <div class="tankimgsmall"><a href="/tankoubon/daigaku"><img class="img3" src="/assets/images/tank/daigaku.jpg"></a></div>
				<div class="tankimgsmall"><a href="/tankoubon/miraclehole"><img class="img3" src="/assets/images/tank/kiseki_no_ana.jpg"></a></div>
				<div class="tankimgsmall"><a href="/tankoubon/acmate"><img class="img3" src="/assets/images/tank/acmate.jpg"></a></div>
	</div>
</div>
<div class="clear"></div>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>

<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Megastore H which featured JKP comics. You can click on the pictures to see a larger version. Cover illustrations were done by illustrator <b>Toe Gojuu</b> (Japanese: 十重五重).</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>

<h2 class="subtitle">TOC Comments</h2>
<p>Most magazines contains a Table of Contents page (TOC) where the authors can write a few words for the fans. These comments are usually not very interesting or informative, but they show some inside of the author's everyday life.</p>

<table class="comment">
    <?php renderMagazineComment($contents) ?>       
</table>