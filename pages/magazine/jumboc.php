<h1>This is Heading 1</h1>

<h1>This is Heading 1 News</h1>

<h1 class="tank_title_en">This is Heading 1 Tankobon Title ENG</h1>

<h1 class="tank_title_jp">This is Heading 1 Tankobon Title JAP</h1>

<h1 class="page_title">This is Heading 1 Page Title for IMG</h1>

<h2>This is Heading 2</h2>

<h3>This is Heading 3</h3>

<h4>This is Heading 4</h4>

<h5>This is Heading 5</h5>

<h3>This is Heading 6</h3>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean volutpat luctus purus, non ullamcorper eros ultricies ut. Donec in felis efficitur, molestie est id, vulputate turpis. Suspendisse bibendum eu tortor sit amet efficitur. Integer ut turpis non sem gravida pharetra quis nec leo. Vivamus vitae turpis in neque scelerisque viverra. Donec laoreet ultrices velit, id mollis felis tempor quis. Aenean eget mi in tellus bibendum ornare et a lectus. Aliquam ultricies id purus vitae luctus. Curabitur ullamcorper semper tellus eu condimentum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec a laoreet magna. Aliquam velit quam, convallis in nibh vitae, pellentesque pulvinar enim. Maecenas eget dignissim orci. Etiam quis velit pharetra, rhoncus orci egestas, malesuada diam.
</p>

<!-- Cover Gallery -->
<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Jumbo. Illustrations were done by <b>Hiroshi Kawamoto</b>. These cover pictures were compiled in a release called <b>Daiyaboureki</b>.</p>

	<div class="cgwrapper">

  		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-01.jpg" alt="cover" title="cover"><br>1988/01<br>No.001</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-02.jpg" alt="cover" title="cover"><br>1988/02<br>No.002</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-03.jpg" alt="cover" title="cover"><br>1988/03<br>No.003</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-04.jpg" alt="cover" title="cover"><br>1988/04<br>No.004</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-05.jpg" alt="cover" title="cover"><br>1988/05<br>No.005</div>
        </div>
        
  		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-06.jpg" alt="cover" title="cover"><br>1988/06<br>No.006</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-07.jpg" alt="cover" title="cover"><br>1988/07<br>No.007</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-08.jpg" alt="cover" title="cover"><br>1988/08<br>No.008</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-09.jpg" alt="cover" title="cover"><br>1988/09<br>No.009</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-10.jpg" alt="cover" title="cover"><br>1988/10<br>No.010</div>
        </div>
        
  		<div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-11.jpg" alt="cover" title="cover"><br>1988/11<br>No.011</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1988-12.jpg" alt="cover" title="cover"><br>1988/12<br>No.012</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-01.jpg" alt="cover" title="cover"><br>1989/01<br>No.013</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-02.jpg" alt="cover" title="cover"><br>1989/02<br>No.014</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-03.jpg" alt="cover" title="cover"><br>1989/03<br>No.015</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-04.jpg" alt="cover" title="cover"><br>1989/04<br>No.016</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-05.jpg" alt="cover" title="cover"><br>1989/05<br>No.017</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-06.jpg" alt="cover" title="cover"><br>1989/06<br>No.018</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-07.jpg" alt="cover" title="cover"><br>1989/07<br>No.019</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-08.jpg" alt="cover" title="cover"><br>1989/08<br>No.020</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-09.jpg" alt="cover" title="cover"><br>1989/09<br>No.021</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-10.jpg" alt="cover" title="cover"><br>1989/10<br>No.022</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-11.jpg" alt="cover" title="cover"><br>1989/11<br>No.023</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1989-12.jpg" alt="cover" title="cover"><br>1989/12<br>No.024</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-01.jpg" alt="cover" title="cover"><br>1990/01<br>No.025</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-02.jpg" alt="cover" title="cover"><br>1990/02<br>No.026</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-03.jpg" alt="cover" title="cover"><br>1990/03<br>No.027</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-04.jpg" alt="cover" title="cover"><br>1990/04<br>No.028</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-05.jpg" alt="cover" title="cover"><br>1990/05<br>No.029</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-06.jpg" alt="cover" title="cover"><br>1990/06<br>No.030</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-07.jpg" alt="cover" title="cover"><br>1990/07<br>No.031</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-08.jpg" alt="cover" title="cover"><br>1990/08<br>No.032</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-09.jpg" alt="cover" title="cover"><br>1990/09<br>No.033</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-10.jpg" alt="cover" title="cover"><br>1990/10<br>No.034</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-11.jpg" alt="cover" title="cover"><br>1990/11<br>No.035</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1990-12.jpg" alt="cover" title="cover"><br>1990/12<br>No.036</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-01.jpg" alt="cover" title="cover"><br>1991/01<br>No.037</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-02.jpg" alt="cover" title="cover"><br>1991/02<br>No.038</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-03.jpg" alt="cover" title="cover"><br>1991/03<br>No.039</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-04.jpg" alt="cover" title="cover"><br>1991/04<br>No.040</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-05.jpg" alt="cover" title="cover"><br>1991/05<br>No.041</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-06.jpg" alt="cover" title="cover"><br>1991/06<br>No.042</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-07.jpg" alt="cover" title="cover"><br>1991/07<br>No.043</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-08.jpg" alt="cover" title="cover"><br>1991/08<br>No.044</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-09.jpg" alt="cover" title="cover"><br>1991/09<br>No.045</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-10.jpg" alt="cover" title="cover"><br>1991/10<br>No.046</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-11.jpg" alt="cover" title="cover"><br>1991/11<br>No.047</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1991-12.jpg" alt="cover" title="cover"><br>1991/12<br>No.048</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-01.jpg" alt="cover" title="cover"><br>1992/01<br>No.049</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-02.jpg" alt="cover" title="cover"><br>1992/02<br>No.050</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-03.jpg" alt="cover" title="cover"><br>1992/03<br>No.051</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-04.jpg" alt="cover" title="cover"><br>1992/04<br>No.052</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-05.jpg" alt="cover" title="cover"><br>1992/05<br>No.053</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-06.jpg" alt="cover" title="cover"><br>1992/06<br>No.054</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-07.jpg" alt="cover" title="cover"><br>1992/07<br>No.055</div>
        </div>
        
        <div class="cgcenter">      
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-08.jpg" alt="cover" title="cover"><br>1992/08<br>No.056</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-09.jpg" alt="cover" title="cover"><br>1992/09<br>No.057</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-10.jpg" alt="cover" title="cover"><br>1992/10<br>No.058</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-11.jpg" alt="cover" title="cover"><br>1992/11<br>No.059</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1992-12.jpg" alt="cover" title="cover"><br>1992/12<br>No.060</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-01.jpg" alt="cover" title="cover"><br>1993/01<br>No.061</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-02.jpg" alt="cover" title="cover"><br>1993/02<br>No.062</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-03.jpg" alt="cover" title="cover"><br>1993/03<br>No.063</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-04.jpg" alt="cover" title="cover"><br>1993/04<br>No.064</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-05.jpg" alt="cover" title="cover"><br>1993/05<br>No.065</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-06.jpg" alt="cover" title="cover"><br>1993/06<br>No.066</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-07.jpg" alt="cover" title="cover"><br>1993/07<br>No.067</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-08.jpg" alt="cover" title="cover"><br>1993/08<br>No.068</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-09.jpg" alt="cover" title="cover"><br>1993/09<br>No.069</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-10.jpg" alt="cover" title="cover"><br>1993/10<br>No.070</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-11.jpg" alt="cover" title="cover"><br>1993/11<br>No.071</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1993-12.jpg" alt="cover" title="cover"><br>1993/12<br>No.072</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-01.jpg" alt="cover" title="cover"><br>1994/01<br>No.073</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-02.jpg" alt="cover" title="cover"><br>1994/02<br>No.074</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-03.jpg" alt="cover" title="cover"><br>1994/03<br>No.075</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-04.jpg" alt="cover" title="cover"><br>1994/04<br>No.076</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-05.jpg" alt="cover" title="cover"><br>1994/05<br>No.077</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-06.jpg" alt="cover" title="cover"><br>1994/06<br>No.078</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-07.jpg" alt="cover" title="cover"><br>1994/07<br>No.079</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-08.jpg" alt="cover" title="cover"><br>1994/08<br>No.080</div>
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-09.jpg" alt="cover" title="cover"><br>1994/09<br>No.081</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-10.jpg" alt="cover" title="cover"><br>1994/10<br>No.082</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-11.jpg" alt="cover" title="cover"><br>1994/11<br>No.083</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1994-12.jpg" alt="cover" title="cover"><br>1994/12<br>No.084</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-01.jpg" alt="cover" title="cover"><br>1995/01<br>No.085</div>
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-02.jpg" alt="cover" title="cover"><br>1995/02<br>No.086</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-03.jpg" alt="cover" title="cover"><br>1995/03<br>No.087</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-04.jpg" alt="cover" title="cover"><br>1995/04<br>No.088</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-05.jpg" alt="cover" title="cover"><br>1995/05<br>No.089</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-06.jpg" alt="cover" title="cover"><br>1995/06<br>No.090</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-07.jpg" alt="cover" title="cover"><br>1995/07<br>No.091</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-08.jpg" alt="cover" title="cover"><br>1995/08<br>No.092</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-09.jpg" alt="cover" title="cover"><br>1995/09<br>No.093</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-10.jpg" alt="cover" title="cover"><br>1995/10<br>No.094</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-11.jpg" alt="cover" title="cover"><br>1995/11<br>No.095</div>
        </div>
        
        <div class="cgcenter">    
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1995-12.jpg" alt="cover" title="cover"><br>1995/12<br>No.096?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-01.jpg" alt="cover" title="cover"><br>1996/01<br>No.097?</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-02.jpg" alt="cover" title="cover"><br>1996/02<br>No.098?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-03.jpg" alt="cover" title="cover"><br>1996/03<br>No.099?</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-04.jpg" alt="cover" title="cover"><br>1996/04<br>No.100?</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-05.jpg" alt="cover" title="cover"><br>1996/05<br>No.101</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-06.jpg" alt="cover" title="cover"><br>1996/06<br>No.102</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-07.jpg" alt="cover" title="cover"><br>1996/07<br>No.103</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-08.jpg" alt="cover" title="cover"><br>1996/08<br>No.104</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-09.jpg" alt="cover" title="cover"><br>1996/09<br>No.105</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-10.jpg" alt="cover" title="cover"><br>1996/10<br>No.106</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-11.jpg" alt="cover" title="cover"><br>1996/11<br>No.107</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1996-12.jpg" alt="cover" title="cover"><br>1996/12<br>No.108</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-01.jpg" alt="cover" title="cover"><br>1997/01<br>No.109</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-02.jpg" alt="cover" title="cover"><br>1997/02<br>No.110</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-03.jpg" alt="cover" title="cover"><br>1997/03<br>No.111</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-04.jpg" alt="cover" title="cover"><br>1997/04<br>No.112</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-05.jpg" alt="cover" title="cover"><br>1997/05<br>No.113</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-06.jpg" alt="cover" title="cover"><br>1997/06<br>No.114</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-07.jpg" alt="cover" title="cover"><br>1997/07<br>No.115</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-08.jpg" alt="cover" title="cover"><br>1997/08<br>No.116</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-09.jpg" alt="cover" title="cover"><br>1997/09<br>No.117</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-10.jpg" alt="cover" title="cover"><br>1997/10<br>No.118</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-11.jpg" alt="cover" title="cover"><br>1997/11<br>No.119</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1997-12.jpg" alt="cover" title="cover"><br>1997/12<br>No.120</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-01.jpg" alt="cover" title="cover"><br>1998/01<br>No.121</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-02.jpg" alt="cover" title="cover"><br>1998/02<br>No.122</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-03.jpg" alt="cover" title="cover"><br>1998/03<br>No.123</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-04.jpg" alt="cover" title="cover"><br>1998/04<br>No.124</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-05.jpg" alt="cover" title="cover"><br>1998/05<br>No.125</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-06.jpg" alt="cover" title="cover"><br>1998/06<br>No.126</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-07.jpg" alt="cover" title="cover"><br>1998/07<br>No.127</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-08.jpg" alt="cover" title="cover"><br>1998/08<br>No.128</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-09.jpg" alt="cover" title="cover"><br>1998/09<br>No.129</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-10.jpg" alt="cover" title="cover"><br>1998/10<br>No.130</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-11.jpg" alt="cover" title="cover"><br>1998/11<br>No.131</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1998-12.jpg" alt="cover" title="cover"><br>1998/12<br>No.132</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-01.jpg" alt="cover" title="cover"><br>1999/01<br>No.133</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-02.jpg" alt="cover" title="cover"><br>1999/02<br>No.134</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-03.jpg" alt="cover" title="cover"><br>1999/03<br>No.135</div>
        </div>
                    
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-04.jpg" alt="cover" title="cover"><br>1999/04<br>No.136</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-05.jpg" alt="cover" title="cover"><br>1999/05<br>No.137</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-06.jpg" alt="cover" title="cover"><br>1999/06<br>No.138</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-07.jpg" alt="cover" title="cover"><br>1999/07<br>No.139</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-08.jpg" alt="cover" title="cover"><br>1999/08<br>No.140</div>
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-09.jpg" alt="cover" title="cover"><br>1999/09<br>No.141</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-10.jpg" alt="cover" title="cover"><br>1999/10<br>No.142</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-11.jpg" alt="cover" title="cover"><br>1999/11<br>No.143</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/1999-12.jpg" alt="cover" title="cover"><br>1999/12<br>No.144</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-01.jpg" alt="cover" title="cover"><br>2000/01<br>No.145</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-02.jpg" alt="cover" title="cover"><br>2000/02<br>No.146</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-03.jpg" alt="cover" title="cover"><br>2000/03<br>No.147</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-04.jpg" alt="cover" title="cover"><br>2000/04<br>No.148</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-05.jpg" alt="cover" title="cover"><br>2000/05<br>No.149</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-06.jpg" alt="cover" title="cover"><br>2000/06<br>No.150</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-07.jpg" alt="cover" title="cover"><br>2000/07<br>No.151</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-08.jpg" alt="cover" title="cover"><br>2000/08<br>No.152</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-09.jpg" alt="cover" title="cover"><br>2000/09<br>No.153</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-10.jpg" alt="cover" title="cover"><br>2000/10<br>No.154</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-11.jpg" alt="cover" title="cover"><br>2000/11<br>No.155</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2000-12.jpg" alt="cover" title="cover"><br>2000/12<br>No.156</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-01.jpg" alt="cover" title="cover"><br>2001/01<br>No.157</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-02.jpg" alt="cover" title="cover"><br>2001/02<br>No.159 ?</div> <!-- Somewhere in 2001: ウォーターガール // Water Girl -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-03.jpg" alt="cover" title="cover"><br>2001/03<br>No.159</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-04.jpg" alt="cover" title="cover"><br>2001/04<br>No.160 ?</div>
        </div>

        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-05.jpg" alt="cover" title="cover"><br>2001/05<br>No.161</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-06.jpg" alt="cover" title="cover"><br>2001/06<br>No.162</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-07.jpg" alt="cover" title="cover"><br>2001/07<br>No.163</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-08.jpg" alt="cover" title="cover"><br>2001/08<br>No.164</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-09.jpg" alt="cover" title="cover"><br>2001/09<br>No.165</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-10.jpg" alt="cover" title="cover"><br>2001/10<br>No.166</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-11.jpg" alt="cover" title="cover"><br>2001/11<br>No.167</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2001-12.jpg" alt="cover" title="cover"><br>2001/12<br>No.168</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-01.jpg" alt="cover" title="cover"><br>2002/01<br>No.169</div> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-02.jpg" alt="cover" title="cover"><br>2002/02<br>No.170</div>
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-03.jpg" alt="cover" title="cover"><br>2002/03<br>No.171</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-04.jpg" alt="cover" title="cover"><br>2002/04<br>No.172</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-05.jpg" alt="cover" title="cover"><br>2002/05<br>No.173 ?</div> <!-- Maybe: 花がら学園Z // Flower Academy Z ???--> 
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-06.jpg" alt="cover" title="cover"><br>2002/06<br>No.174</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-07.jpg" alt="cover" title="cover"><br>2002/07<br>No.175</div>
        </div>
     
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-08.jpg" alt="cover" title="cover"><br>2002/08<br>No.176 ?</div> <!-- Maybe: となりの肉 // My Neighbor's Meat ???-->  
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-09.jpg" alt="cover" title="cover"><br>2002/09<br>No.177</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-10.jpg" alt="cover" title="cover"><br>2002/10<br>No.178</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-11.jpg" alt="cover" title="cover"><br>2002/11<br>No.179</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2002-12.jpg" alt="cover" title="cover"><br>2002/12<br>No.180 ?</div>  <!-- ??????--> 
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-01.jpg" alt="cover" title="cover"><br>2003/01<br>No.181</div>  
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-02.jpg" alt="cover" title="cover"><br>2003/02<br>No.182 ?</div> <!-- Maybe: 逆噴射インチキメガネ // Reverse Thrust! Bespectacled Cheater ???-->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-03.jpg" alt="cover" title="cover"><br>2003/03<br>No.183</div> <!-- Breast Karate, Milk Fist // オッパイ空手ミルク拳 -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-04.jpg" alt="cover" title="cover"><br>2003/04<br>No.184</div> <!-- Open Ass Model - Fainting Club Captain // オシリ美術部 ??/ 肛開モデル悶絶部長 -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-05.jpg" alt="cover" title="cover"><br>2003/05<br>No.185</div> <!-- Spartacus Games - Directive 076 //スパルタ指令 -->
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-06.jpg" alt="cover" title="cover"><br>2003/06<br>No.186 ?</div> <!-- Maybe: 湯けむり悩殺フィーバー // Steam - Bewitchment Fever -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-07.jpg" alt="cover" title="cover"><br>2003/07<br>No.187</div> <!-- Hot Blooded! Fainting in Agony! The Hard Working Captain // 熱血！悶絶！しごき部長 -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-08.jpg" alt="cover" title="cover"><br>2003/08<br>No.188</div> <!-- Straight Pitch! Bewithchment Operation	// 直球！悩殺オペレーション -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-09.jpg" alt="cover" title="cover"><br>2003/09<br>No.189</div> <!-- Three Part Milk Explosion Summit Alpha	// 第3次爆乳サミットα -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-10.jpg" alt="cover" title="cover"><br>2003/10<br>No.190</div> <!-- Sister's Pink Bath Hell	// 妹地獄ピンク風呂 -->
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-11.gif" alt="cover" title="cover"><br>2003/11<br>No.191</div> <!-- Restraint, Anal Tyranny, Paparazzi -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2003-12.jpg" alt="cover" title="cover"><br>2003/12<br>No.192</div> <!-- Frenzy! Dead Drunk March-->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-01.jpg" alt="cover" title="cover"><br>2004/01<br>No.193</div> <!-- Direct hit!! Fainting in Agony by the Hellish Sword -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-02.jpg" alt="cover" title="cover"><br>2004/02<br>No.194</div> <!-- The Rosicrucian Massage! -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-03.jpg" alt="cover" title="cover"><br>2004/03<br>No.195</div> <!-- Forbidden Juice Mix -->
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-04.jpg" alt="cover" title="cover"><br>2004/04<br>No.196</div> <!-- Cherry Blossoms Scatter -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-05.jpg" alt="cover" title="cover"><br>2004/05<br>No.197</div> <!-- Hard After - School Doll -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-06.jpg" alt="cover" title="cover"><br>2004/06<br>No.198</div> <!-- Pink Revenge Festival -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-07.jpg" alt="cover" title="cover"><br>2004/07<br>No.199 ?</div> <!-- Maybe: ぬきうち！屈辱Vの字斬り -->
            <div class="kepszoveg"><a class="nagyobb" href="/assets/images/mangalist/jumbo/2004-08.jpg"><img src="/assets/images/mangalist/jumbo/2004-08.jpg" alt="cover" title="cover"></a><br>2004/08<br>No.200</div> <!--Slave Tied Milky Mistress // ミルク極道下克上縛り -->
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-09.jpg" alt="cover" title="cover"><br>2004/09<br>No.201 ?</div> <!-- 屈辱の桃尻チャレンジ -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-10.jpg" alt="cover" title="cover"><br>2004/10<br>No.202 ?</div> <!-- ガチん娘レッスン乱れ責め -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-11.jpg" alt="cover" title="cover"><br>2004/11<br>No.203 ?</div> <!-- 桃乳！！欲情スピリッツ -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2004-12.jpg" alt="cover" title="cover"><br>2004/12<br>No.204 ?</div> <!-- 地獄温泉羞恥の湯 -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-01.jpg" alt="cover" title="cover"><br>2005/01<br>No.205 ?</div> <!-- 逆襲シャインスパーク -->
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-02.jpg" alt="cover" title="cover"><br>2005/02<br>No.206 ?</div> <!-- Maybe: つまみ出し電撃バージン ??? -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-03.jpg" alt="cover" title="cover"><br>2005/03<br>No.207</div> <!-- 熱中！快感アディクト -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-04.jpg" alt="cover" title="cover"><br>2005/04<br>No.208</div> <!-- スリーピング突撃ゲリラ -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-05.jpg" alt="cover" title="cover"><br>2005/05<br>No.209</div> <!-- 挑発ジェットストリーム-->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-06.jpg" alt="cover" title="cover"><br>2005/06<br>No.210</div> <!-- 禁断惑星W・C-->
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-07.jpg" alt="cover" title="cover"><br>2005/07<br>No.211 ?</div> <!-- Maybe: 絶倫ゴールドラッシュ  ???-->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-08.jpg" alt="cover" title="cover"><br>2005/08<br>No.212</div> <!-- Let's秘密喫茶-->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-09.jpg" alt="cover" title="cover"><br>2005/09<br>No.213</div> <!-- 惨敗おしおきMAX！！-->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-10.jpg" alt="cover" title="cover"><br>2005/10<br>No.214</div> <!-- 凌辱陰陽観音開き-->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-11.jpg" alt="cover" title="cover"><br>2005/11<br>No.216</div> <!-- 第1話 悶絶フルオープン-->
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2005-12.png" alt="cover" title="cover"><br>2005/12<br>No.216</div> <!-- 第2話 討ち入りジェットデビュー -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-01.png" alt="cover" title="cover"><br>2006/01<br>No.217</div> <!-- 第3話 ナチュラルボーンエロス -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-02.jpg" alt="cover" title="cover"><br>2006/02<br>No.218</div> <!-- 第4話 いただきデスレース -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-03.jpg" alt="cover" title="cover"><br>2006/03<br>No.219</div> <!-- 第5話僕たちの挽歌 -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-04.jpg" alt="cover" title="cover"><br>2006/04<br>No.220</div> <!-- 第6話 ブラックファックンロール -->
        </div>
        
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-05.jpg" alt="cover" title="cover"><br>2006/05<br>No.221</div> <!-- 第7話サヨナラニッポン -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-06.jpg" alt="cover" title="cover"><br>2006/06<br>No.222</div> <!-- 撃ち出せ！俺の未練スパーク!! -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-07.jpg" alt="cover" title="cover"><br>2006/07<br>No.223</div> <!-- 激烈！！エキサイティングストア -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-08.jpg" alt="cover" title="cover"><br>2006/08<br>No.224</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-09.jpg" alt="cover" title="cover"><br>2006/09<br>No.225</div> <!-- ラブジェット10000 -->
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-10.jpg" alt="cover" title="cover"><br>2006/10<br>No.226</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-11.jpg" alt="cover" title="cover"><br>2006/11<br>No.227</div> <!-- 青春デストロイヤー -->
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2006-12.jpg" alt="cover" title="cover"><br>2006/12<br>No.228</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2007-01.jpg" alt="cover" title="cover"><br>2007/01<br>No.229</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2007-02.jpg" alt="cover" title="cover"><br>2007/02<br>No.230</div>
        </div>
            
        <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2007-03.jpg" alt="cover" title="cover"><br>2007/03<br>No.231</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2007-04.jpg" alt="cover" title="cover"><br>2007/04<br>No.232</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2007-05.jpg" alt="cover" title="cover"><br>2007/05<br>No.233</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2007-06.jpg" alt="cover" title="cover"><br>2007/06<br>No.234</div>
        </div>
        
       <div class="cgcenter">
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2007-07.jpg" alt="cover" title="cover"><br>2007/07<br>No.235</div>
            <div class="kepszoveg"><img src="/assets/images/mangalist/jumbo/2007-08.jpg" alt="cover" title="cover"><br>2007/08<br>No.236</div>
        </div>       
</div>
<div class="clear"></div>