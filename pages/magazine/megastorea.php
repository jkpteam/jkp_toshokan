<?php
  require_once 'assets/data/megastorea_data.php'
?>

<h1>COMIC Megastore Alpha</h1>
<p>This section lists every comic issues JKP has done for Comic Megastore Alpha magazine by release dates. At the bottom of the list you can browse through these magazines cover illustrations including the name of the illustrator.</p>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>

<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Megastore Alpha which featured JKP comics. Illustrations were done by illustrator Sasaoka Gungu and Akino Sora.</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>

<h2 class="subtitle">TOC Comments</h2>
<p>Most magazines contains a Table of Contents page (TOC) where the authors can write a few words for the fans. These comments are usually not too informative, but they show some inside information of the author's everyday life.</p>

<table class="comment">
    <?php renderMagazineComment($contents) ?>       
</table>


