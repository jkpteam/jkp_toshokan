<?php
  require_once 'assets/data/megaplus_data.php'
?>
<h1 class="page_title"><img src="/assets/images/mangalist/mplogo.png" title="mplogo" id="stamp" style="display: inline;"> COMIC Megaplus</h1>
<p class='pageinfo'>This section lists every manuscript JKP has done for Comic Megaplus magazine by release dates. At the bottom of the list you can browse through these magazines cover illustrations including the name of the illustrator.</p>


<h3>Corresponding Tankoubon Volumes</h3>
<div class="cgwrapper">
    
		<div class="cgcenter">
				<div class="tankimgsmall"><a href="/tankoubon/license"><img class="img3" src="/assets/images/tank/license.jpg"></a></div>
                <div class="tankimgsmall"><a href="/tankoubon/screamer"><img class="img3" src="/assets/images/tank/screamer.jpg"></a></div>
                <div class="tankimgsmall"><a href="/tankoubon/balkan"><img class="img3" src="/assets/images/tank/balkan.jpg"></a></div>
	</div>
</div>
<div class="clear"></div>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>

<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Megaplus which featured JKP comics. You can click on the pictures to see a larger version. From volume 25 till volume 33 cover illustrations were done by Hentai Mangaka <a href="http://www.mangaupdates.com/authors.html?id=1866">Hiyo Hiyo</a>. From Volume 34 Hentai Mangaka <b>ISAO</b> was put in charge of the covers until the end of the magazine.</p>

<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>

<h2 class="subtitle">TOC Comments</h2>
<p>Most magazines contains a Table of Contents page (TOC) where the authors can write a few sentences for the fans. These comments are usually not too interesting or informative, but they show some insight of the author's everyday life.</p>

<table class="comment">
    <?php renderMagazineComment($contents) ?>       
</table>
<!-- Colored illustrations Gallery -->
<h2 class="subtitle">Colored Illustrations</h2>
<p>This section showcases all of the colored illustrations.</p>
	<div class="cgwrapper">
<h3>Vol.34 - Certain Kill Peach Butt Burning!!</h3>
		<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp34_002.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp34_002.jpg" alt="Certain Kill Peach Butt Burning!!" title="Certain Kill Peach Butt Burning!! color page"></a></div>
		</div>
		
<h3>Vol.36 - Love Rock Fucking Live!!</h3>
		<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp36_002.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp36_002.jpg" alt="Love Rock Fucking Live!!" title="Love Rock Fucking Live!! color page 1"></a></div>
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp36_003.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp36_003.jpg" alt="Love Rock Fucking Live!!" title="Love Rock Fucking Live!! color page 2"></a></div>
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp36_004.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp36_004.jpg" alt="Love Rock Fucking Live!!" title="Love Rock Fucking Live!! color page 3"></a></div>
			<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp36_005.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp36_005.jpg" alt="Love Rock Fucking Live!!" title="Love Rock Fucking Live!! color page 4"></a></div>
		</div>

<h3>Vol.45 - Sneaking Bakuretsu Misson</h3>
		<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp45_203.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp45_203.jpg" alt="Sneaking Bakuretsu Misson" title="Sneaking Bakuretsu Misson color page 1"></a></div>
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp45_204.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp45_204.jpg" alt="Sneaking Bakuretsu Misson" title="Sneaking Bakuretsu Misson color page 2"></a><br></div>
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp45_205.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp45_205.jpg" alt="Sneaking Bakuretsu Misson" title="Sneaking Bakuretsu Misson color page 3"></a><br></div>
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp45_206.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp45_206.jpg" alt="Sneaking Bakuretsu Misson" title="Sneaking Bakuretsu Misson color page 4"></a><br></div>
        </div>

<h3>Vol.49 - The High-tension Crazy driving!!</h3>
		<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp49_002.jpg"><img src="/assets/images/mangalist/megaplus/ch/mp49_002.jpg" alt="The High-tension Crazy driving!!" title="The High-tension Crazy driving!! color page"></a></div>
		</div>
<div class="clear"></div>
	</div>

<!-- Megaplus Illustrator column -->
<h2 class="subtitle">Illustrator Column</h2>
<div class="illcol">
    <a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp38-ic-.jpg">
        <img src="/assets/images/mangalist/megaplus/ch/mp38-ic-.jpg" alt="Illustrator Column" title="Megaplus Volume 38 Illustrator Column">
    </a>
<p>Megaplus Volume 38 Illustrator Column</p>
    <p>Translation needed!!</p>
    <p>真魂斗羅(Contra: Shattered Soldier PS2 game)<br>

ステージ1のボス奴隷獣タカ改タン (Stage 1 Boss Slave Beast fight)<br>

最近ユーチュブでゲームセンターCXを見てたまにはレトロな２Dのシューティングゲームもいいなマーと思ってて思い出したのが４年前に発売日買ってそのあまりのXXXXの高さにクリアをあきらめて</p>
</div>
