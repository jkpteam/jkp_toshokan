<?php
  require_once 'assets/data/megamilk_data.php'
?>
<!--
<div class="magazine_header">
    <div class="logo"><img src="/assets/images/mangalist/mmlogo.png" title="mmlogo" id="stamp" style="display: inline;"></div>
    <div class="header"><h1 class="magazine_eng">COMIC Megamilk list</h1>
        <span class="magazine_jp">コミックメガミルク</span>/div>
</div>
-->

<div class="magazine_header"><img src="/assets/images/mangalist/mmlogo.png" title="mmlogo" id="stamp" style="display: inline;">
      <h2>COMIC Megamilk list</h2>
  <p>コミックメガミルク</p>
</div>

<p>This section lists every comic issues JKP has done for COMIC Megamilk magazine.</p>

<h3>Corresponding Tankoubon Volumes</h3>
<div class="cgwrapper">
    
		<div class="cgcenter">
				<div class="tankimgsmall"><a href="/tankoubon/destroy"><img class="img3" src="/assets/images/tank/mon_destroy.jpg"></a></div>
                <div class="tankimgsmall"><a href="/tankoubon/system"><img class="img3" src="/assets/images/tank/system.jpg"></a></div>
                <div class="tankimgsmall"><a href="/tankoubon/miraclehole"><img class="img3" src="/assets/images/tank/kiseki_no_ana.jpg"></a></div>
                <div class="tankimgsmall"><a href="/tankoubon/caligulaplus"><img class="img3" src="/assets/images/tank/caligula_plus.jpg"></a></div>
	</div>
</div>
<div class="clear"></div>

<table class="manga">
    <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        
        <?php renderMagazineManga($contents) ?>         
    </tr>
</table>


<!-- Cover Gallery -->
<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Megamilk which featured JKP comics. You can click on the pictures to see a larger version. Cover illustrations were done by a number of illustrators like <b>Ashi Shun, ISAO</b>, and many others. See list below for complete information.</p>


<div class="cgwrapper">
    <?php renderCoverGallery($contents) ?>  
<div class="clear"></div>
</div>

<h2 class="subtitle">TOC Comments</h2>
<p>Most magazines contains a Table of Contents page (TOC) where the authors can write a few words for the fans. These comments are usually not very interesting or informative, but they show some inside of the author's everyday life.</p>

<table class="comment">
    <?php renderMagazineComment($contents) ?>       
</table>