<?php
    $contents = array(
        array(
            'image' => 'mangalist/jumbo/1988-01.png',
            'date' => '1988/01',
            'issue' => 'No.001',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-02.jpg',
            'date' => '1988/02',
            'issue' => 'No.002',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-03.jpg',
            'date' => '1988/03',
            'issue' => 'No.003',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-04.jpg',
            'date' => '1988/04',
            'issue' => 'No.004',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-05.jpg',
            'date' => '1988/05',
            'issue' => 'No.005',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-06.jpg',
            'date' => '1988/06',
            'issue' => 'No.006',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-07.jpg',
            'date' => '1988/07',
            'issue' => 'No.007',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-08.jpg',
            'date' => '1988/08',
            'issue' => 'No.008',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-09.png',
            'date' => '1988/09',
            'issue' => 'No.009',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-10.jpg',
            'date' => '1988/10',
            'issue' => 'No.010',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-11.jpg',
            'date' => '1988/11',
            'issue' => 'No.011',
        ),
        array(
            'image' => 'mangalist/jumbo/1988-12.jpg',
            'date' => '1988/12',
            'issue' => 'No.012',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-01.jpg',
            'date' => '1989/01',
            'issue' => 'No.013',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-02.jpg',
            'date' => '1989/02',
            'issue' => 'No.014',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-03.jpg',
            'date' => '1989/03',
            'issue' => 'No.015',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-04.jpg',
            'date' => '1989/04',
            'issue' => 'No.016',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-05.jpg',
            'date' => '1989/05',
            'issue' => 'No.017',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-06.jpg',
            'date' => '1989/06',
            'issue' => 'No.018',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-07.jpg',
            'date' => '1989/07',
            'issue' => 'No.019',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-08.jpg',
            'date' => '1989/08',
            'issue' => 'No.020',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-09.jpg',
            'date' => '1989/09',
            'issue' => 'No.021',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-10.jpg',
            'date' => '1989/10',
            'issue' => 'No.022',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-11.jpg',
            'date' => '1989/11',
            'issue' => 'No.023',
        ),
        array(
            'image' => 'mangalist/jumbo/1989-12.jpg',
            'date' => '1989/12',
            'issue' => 'No.024',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-01.jpg',
            'date' => '1990/01',
            'issue' => 'No.025',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-02.jpg',
            'date' => '1990/02',
            'issue' => 'No.026',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-03.jpg',
            'date' => '1990/03',
            'issue' => 'No.027',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-04.jpg',
            'date' => '1990/04',
            'issue' => 'No.028',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-05.jpg',
            'date' => '1990/05',
            'issue' => 'No.029',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-06.jpg',
            'date' => '1990/06',
            'issue' => 'No.030',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-07.jpg',
            'date' => '1990/07',
            'issue' => 'No.031',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-08.jpg',
            'date' => '1990/08',
            'issue' => 'No.032',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-09.jpg',
            'date' => '1990/09',
            'issue' => 'No.033',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-10.jpg',
            'date' => '1990/10',
            'issue' => 'No.034',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-11.jpg',
            'date' => '1990/11',
            'issue' => 'No.035',
        ),
        array(
            'image' => 'mangalist/jumbo/1990-12.jpg',
            'date' => '1990/12',
            'issue' => 'No.036',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-01.jpg',
            'date' => '1991/01',
            'issue' => 'No.037',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-02.jpg',
            'date' => '1991/02',
            'issue' => 'No.038',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-03.jpg',
            'date' => '1991/03',
            'issue' => 'No.039',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-04.jpg',
            'date' => '1991/04',
            'issue' => 'No.040',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-05.jpg',
            'date' => '1991/05',
            'issue' => 'No.041',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-06.jpg',
            'date' => '1991/06',
            'issue' => 'No.042',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-07.jpg',
            'date' => '1991/07',
            'issue' => 'No.043',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-08.jpg',
            'date' => '1991/08',
            'issue' => 'No.044',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-09.jpg',
            'date' => '1991/09',
            'issue' => 'No.045',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-10.jpg',
            'date' => '1991/10',
            'issue' => 'No.046',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-11.jpg',
            'date' => '1991/11',
            'issue' => 'No.047',
        ),
        array(
            'image' => 'mangalist/jumbo/1991-12.jpg',
            'date' => '1991/12',
            'issue' => 'No.048',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-01.jpg',
            'date' => '1992/01',
            'issue' => 'No.049',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-02.jpg',
            'date' => '1992/02',
            'issue' => 'No.050',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-03.jpg',
            'date' => '1992/03',
            'issue' => 'No.051',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-04.jpg',
            'date' => '1992/04',
            'issue' => 'No.052',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-05.jpg',
            'date' => '1992/05',
            'issue' => 'No.053',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-06.jpg',
            'date' => '1992/06',
            'issue' => 'No.054',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-07.jpg',
            'date' => '1992/07',
            'issue' => 'No.055',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-08.jpg',
            'date' => '1992/08',
            'issue' => 'No.056',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-09.jpg',
            'date' => '1992/09',
            'issue' => 'No.057',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-10.jpg',
            'date' => '1992/10',
            'issue' => 'No.058',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-11.jpg',
            'date' => '1992/11',
            'issue' => 'No.059',
        ),
        array(
            'image' => 'mangalist/jumbo/1992-12.jpg',
            'date' => '1992/12',
            'issue' => 'No.060',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-01.jpg',
            'date' => '1993/01',
            'issue' => 'No.061',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-02.jpg',
            'date' => '1993/02',
            'issue' => 'No.062',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-03.jpg',
            'date' => '1993/03',
            'issue' => 'No.063',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-04.jpg',
            'date' => '1993/04',
            'issue' => 'No.064',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-05.jpg',
            'date' => '1993/05',
            'issue' => 'No.065',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-06.jpg',
            'date' => '1993/06',
            'issue' => 'No.066',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-07.jpg',
            'date' => '1993/07',
            'issue' => 'No.067',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-08.jpg',
            'date' => '1993/08',
            'issue' => 'No.068',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-09.jpg',
            'date' => '1993/09',
            'issue' => 'No.069',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-10.jpg',
            'date' => '1993/10',
            'issue' => 'No.070',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-11.jpg',
            'date' => '1993/11',
            'issue' => 'No.071',
        ),
        array(
            'image' => 'mangalist/jumbo/1993-12.jpg',
            'date' => '1993/12',
            'issue' => 'No.072',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-01.jpg',
            'date' => '1994/01',
            'issue' => 'No.073',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-02.jpg',
            'date' => '1994/02',
            'issue' => 'No.074',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-03.jpg',
            'date' => '1994/03',
            'issue' => 'No.075',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-04.jpg',
            'date' => '1994/04',
            'issue' => 'No.076',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-05.jpg',
            'date' => '1994/05',
            'issue' => 'No.077',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-06.jpg',
            'date' => '1994/06',
            'issue' => 'No.078',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-07.jpg',
            'date' => '1994/07',
            'issue' => 'No.079',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-08.jpg',
            'date' => '1994/08',
            'issue' => 'No.080',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-09.jpg',
            'date' => '1994/09',
            'issue' => 'No.081',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-10.jpg',
            'date' => '1994/10',
            'issue' => 'No.082',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-11.jpg',
            'date' => '1994/11',
            'issue' => 'No.083',
        ),
        array(
            'image' => 'mangalist/jumbo/1994-12.jpg',
            'date' => '1994/12',
            'issue' => 'No.084',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-01.jpg',
            'date' => '1995/01',
            'issue' => 'No.085',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-02.jpg',
            'date' => '1995/02',
            'issue' => 'No.086',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-03.jpg',
            'date' => '1995/03',
            'issue' => 'No.087',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-04.jpg',
            'date' => '1995/04',
            'issue' => 'No.088',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-05.jpg',
            'date' => '1995/05',
            'issue' => 'No.089',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-06.jpg',
            'date' => '1995/06',
            'issue' => 'No.090',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-07.jpg',
            'date' => '1995/07',
            'issue' => 'No.091',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-08.jpg',
            'date' => '1995/08',
            'issue' => 'No.092',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-09.jpg',
            'date' => '1995/09',
            'issue' => 'No.093',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-10.jpg',
            'date' => '1995/10',
            'issue' => 'No.094',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-11.jpg',
            'date' => '1995/11',
            'issue' => 'No.095',
        ),
        array(
            'image' => 'mangalist/jumbo/1995-12.jpg',
            'date' => '1995/12',
            'issue' => 'No.096',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-01.jpg',
            'date' => '1996/01',
            'issue' => 'No.097',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-02.jpg',
            'date' => '1996/02',
            'issue' => 'No.098',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-03.jpg',
            'date' => '1996/03',
            'issue' => 'No.099',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-04.jpg',
            'date' => '1996/04',
            'issue' => 'No.100',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-05.jpg',
            'date' => '1996/05',
            'issue' => 'No.101',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-06.jpg',
            'date' => '1996/06',
            'issue' => 'No.102',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-07.jpg',
            'date' => '1996/07',
            'issue' => 'No.103',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-08.jpg',
            'date' => '1996/08',
            'issue' => 'No.104',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-09.jpg',
            'date' => '1996/09',
            'issue' => 'No.105',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-10.jpg',
            'date' => '1996/10',
            'issue' => 'No.106',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-11.jpg',
            'date' => '1996/11',
            'issue' => 'No.107',
        ),
        array(
            'image' => 'mangalist/jumbo/1996-12.jpg',
            'date' => '1996/12',
            'issue' => 'No.108',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-01.jpg',
            'date' => '1997/01',
            'issue' => 'No.109',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-02.jpg',
            'date' => '1997/02',
            'issue' => 'No.110',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-03.jpg',
            'date' => '1997/03',
            'issue' => 'No.111',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-04.jpg',
            'date' => '1997/04',
            'issue' => 'No.112',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-05.jpg',
            'date' => '1997/05',
            'issue' => 'No.113',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-06.jpg',
            'date' => '1997/06',
            'issue' => 'No.114',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-07.jpg',
            'date' => '1997/07',
            'issue' => 'No.115',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-08.jpg',
            'date' => '1997/08',
            'issue' => 'No.116',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-09.jpg',
            'date' => '1997/09',
            'issue' => 'No.117',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-10.jpg',
            'date' => '1997/10',
            'issue' => 'No.118',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-11.jpg',
            'date' => '1997/11',
            'issue' => 'No.119',
        ),
        array(
            'image' => 'mangalist/jumbo/1997-12.jpg',
            'date' => '1997/12',
            'issue' => 'No.120',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-01.jpg',
            'date' => '1998/01',
            'issue' => 'No.121',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-02.jpg',
            'date' => '1998/02',
            'issue' => 'No.122',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-03.jpg',
            'date' => '1998/03',
            'issue' => 'No.123',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-04.jpg',
            'date' => '1998/04',
            'issue' => 'No.124',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-05.jpg',
            'date' => '1998/05',
            'issue' => 'No.125',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-06.jpg',
            'date' => '1998/06',
            'issue' => 'No.126',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-07.jpg',
            'date' => '1998/07',
            'issue' => 'No.127',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-08.jpg',
            'date' => '1998/08',
            'issue' => 'No.128',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-09.jpg',
            'date' => '1998/09',
            'issue' => 'No.129',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-10.jpg',
            'date' => '1998/10',
            'issue' => 'No.130',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-11.jpg',
            'date' => '1998/11',
            'issue' => 'No.131',
        ),
        array(
            'image' => 'mangalist/jumbo/1998-12.jpg',
            'date' => '1998/12',
            'issue' => 'No.132',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-01.jpg',
            'date' => '1999/01',
            'issue' => 'No.133',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-02.jpg',
            'date' => '1999/02',
            'issue' => 'No.134',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-03.jpg',
            'date' => '1999/03',
            'issue' => 'No.135',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-04.jpg',
            'date' => '1999/04',
            'issue' => 'No.136',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-05.jpg',
            'date' => '1999/05',
            'issue' => 'No.137',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-06.jpg',
            'date' => '1999/06',
            'issue' => 'No.138',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-07.jpg',
            'date' => '1999/07',
            'issue' => 'No.139',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-08.jpg',
            'date' => '1999/08',
            'issue' => 'No.140',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-09.jpg',
            'date' => '1999/09',
            'issue' => 'No.141',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-10.jpg',
            'date' => '1999/10',
            'issue' => 'No.142',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-11.jpg',
            'date' => '1999/11',
            'issue' => 'No.143',
        ),
        array(
            'image' => 'mangalist/jumbo/1999-12.jpg',
            'date' => '1999/12',
            'issue' => 'No.144',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-01.jpg',
            'date' => '2000/01',
            'issue' => 'No.145',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-02.jpg',
            'date' => '2000/02',
            'issue' => 'No.146',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-03.jpg',
            'date' => '2000/03',
            'issue' => 'No.147',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-04.jpg',
            'date' => '2000/04',
            'issue' => 'No.148',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-05.jpg',
            'date' => '2000/05',
            'issue' => 'No.149',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-06.jpg',
            'date' => '2000/06',
            'issue' => 'No.150',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-07.jpg',
            'date' => '2000/07',
            'issue' => 'No.151',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-08.jpg',
            'date' => '2000/08',
            'issue' => 'No.152',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-09.jpg',
            'date' => '2000/09',
            'issue' => 'No.153',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-10.jpg',
            'date' => '2000/10',
            'issue' => 'No.154',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-11.jpg',
            'date' => '2000/11',
            'issue' => 'No.155',
        ),
        array(
            'image' => 'mangalist/jumbo/2000-12.jpg',
            'date' => '2000/12',
            'issue' => 'No.156',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-01.jpg',
            'date' => '2001/01',
            'issue' => 'No.157',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-02.jpg',
            'date' => '2001/02',
            'issue' => 'No.158',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-03.jpg',
            'date' => '2001/03',
            'issue' => 'No.159',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-04.jpg',
            'date' => '2001/04',
            'issue' => 'No.160',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-05.jpg',
            'date' => '2001/05',
            'issue' => 'No.161',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-06.jpg',
            'date' => '2001/06',
            'issue' => 'No.162',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-07.jpg',
            'date' => '2001/07',
            'issue' => 'No.163',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-08.jpg',
            'date' => '2001/08',
            'issue' => 'No.164',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-09.jpg',
            'date' => '2001/09',
            'issue' => 'No.165',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-10.jpg',
            'date' => '2001/10',
            'issue' => 'No.166',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-11.jpg',
            'date' => '2001/11',
            'issue' => 'No.167',
        ),
        array(
            'image' => 'mangalist/jumbo/2001-12.jpg',
            'date' => '2001/12',
            'issue' => 'No.168',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-01.jpg',
            'date' => '2002/01',
            'issue' => 'No.169',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-02.jpg',
            'date' => '2002/02',
            'issue' => 'No.170',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-03.jpg',
            'date' => '2002/03',
            'issue' => 'No.171',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-04.jpg',
            'date' => '2002/04',
            'issue' => 'No.172',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-05.jpg',
            'date' => '2002/05',
            'issue' => 'No.173',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-06.jpg',
            'date' => '2002/06',
            'issue' => 'No.174',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-07.jpg',
            'date' => '2002/07',
            'issue' => 'No.175',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-08.jpg',
            'date' => '2002/08',
            'issue' => 'No.176',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-09.jpg',
            'date' => '2002/09',
            'issue' => 'No.177',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-10.jpg',
            'date' => '2002/10',
            'issue' => 'No.178',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-11.jpg',
            'date' => '2002/11',
            'issue' => 'No.179',
        ),
        array(
            'image' => 'mangalist/jumbo/2002-12.jpg',
            'date' => '2002/12',
            'issue' => 'No.180',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-01.jpg',
            'date' => '2003/01',
            'issue' => 'No.181',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-02.jpg',
            'date' => '2003/02',
            'issue' => 'No.182',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-03.jpg',
            'date' => '2003/03',
            'issue' => 'No.183',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-04.jpg',
            'date' => '2003/04',
            'issue' => 'No.184',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-05.jpg',
            'date' => '2003/05',
            'issue' => 'No.185',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-06.jpg',
            'date' => '2003/06',
            'issue' => 'No.186',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-07.jpg',
            'date' => '2003/07',
            'issue' => 'No.187',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-08.jpg',
            'date' => '2003/08',
            'issue' => 'No.188',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-09.jpg',
            'date' => '2003/09',
            'issue' => 'No.189',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-10.jpg',
            'date' => '2003/10',
            'issue' => 'No.190',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-11.jpg',
            'date' => '2003/11',
            'issue' => 'No.191',
        ),
        array(
            'image' => 'mangalist/jumbo/2003-12.jpg',
            'date' => '2003/12',
            'issue' => 'No.192',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-01.jpg',
            'date' => '2004/01',
            'issue' => 'No.193',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-02.jpg',
            'date' => '2004/02',
            'issue' => 'No.194',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-03.jpg',
            'date' => '2004/03',
            'issue' => 'No.195',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-04.jpg',
            'date' => '2004/04',
            'issue' => 'No.196',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-05.jpg',
            'date' => '2004/05',
            'issue' => 'No.197',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-06.jpg',
            'date' => '2004/06',
            'issue' => 'No.198',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-07.jpg',
            'date' => '2004/07',
            'issue' => 'No.199',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-08.jpg',
            'date' => '2004/08',
            'issue' => 'No.200',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-09.jpg',
            'date' => '2004/09',
            'issue' => 'No.201',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-10.jpg',
            'date' => '2004/10',
            'issue' => 'No.202',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-11.jpg',
            'date' => '2004/11',
            'issue' => 'No.203',
        ),
        array(
            'image' => 'mangalist/jumbo/2004-12.jpg',
            'date' => '2004/12',
            'issue' => 'No.204',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-01.jpg',
            'date' => '2005/01',
            'issue' => 'No.205',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-02.jpg',
            'date' => '2005/02',
            'issue' => 'No.206',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-03.jpg',
            'date' => '2005/03',
            'issue' => 'No.207',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-04.jpg',
            'date' => '2005/04',
            'issue' => 'No.208',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-05.jpg',
            'date' => '2005/05',
            'issue' => 'No.209',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-06.jpg',
            'date' => '2005/06',
            'issue' => 'No.210',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-07.jpg',
            'date' => '2005/07',
            'issue' => 'No.211',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-08.jpg',
            'date' => '2005/08',
            'issue' => 'No.212',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-09.jpg',
            'date' => '2005/09',
            'issue' => 'No.213',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-10.jpg',
            'date' => '2005/10',
            'issue' => 'No.214',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-11.jpg',
            'date' => '2005/11',
            'issue' => 'No.215',
        ),
        array(
            'image' => 'mangalist/jumbo/2005-12.jpg',
            'date' => '2005/12',
            'issue' => 'No.216',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-01.jpg',
            'date' => '2006/01',
            'issue' => 'No.217',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-02.jpg',
            'date' => '2006/02',
            'issue' => 'No.218',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-03.jpg',
            'date' => '2006/03',
            'issue' => 'No.219',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-04.jpg',
            'date' => '2006/04',
            'issue' => 'No.220',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-05.jpg',
            'date' => '2006/05',
            'issue' => 'No.221',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-06.jpg',
            'date' => '2006/06',
            'issue' => 'No.222',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-07.jpg',
            'date' => '2006/07',
            'issue' => 'No.223',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-08.jpg',
            'date' => '2006/08',
            'issue' => 'No.224',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-09.jpg',
            'date' => '2006/09',
            'issue' => 'No.225',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-10.jpg',
            'date' => '2006/10',
            'issue' => 'No.226',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-11.jpg',
            'date' => '2006/11',
            'issue' => 'No.227',
        ),
        array(
            'image' => 'mangalist/jumbo/2006-12.jpg',
            'date' => '2006/12',
            'issue' => 'No.228',
        ),
        array(
            'image' => 'mangalist/jumbo/2007-01.jpg',
            'date' => '2007/01',
            'issue' => 'No.229',
        ),
        array(
            'image' => 'mangalist/jumbo/2007-02.jpg',
            'date' => '2007/02',
            'issue' => 'No.230',
        ),
        array(
            'image' => 'mangalist/jumbo/2007-03.jpg',
            'date' => '2007/03',
            'issue' => 'No.231',
        ),
        array(
            'image' => 'mangalist/jumbo/2007-04.jpg',
            'date' => '2007/04',
            'issue' => 'No.232',
        ),
        array(
            'image' => 'mangalist/jumbo/2007-05.jpg',
            'date' => '2007/05',
            'issue' => 'No.233',
        ),
        array(
            'image' => 'mangalist/jumbo/2007-06.jpg',
            'date' => '2007/06',
            'issue' => 'No.234',
        ),
        array(
            'image' => 'mangalist/jumbo/2007-07.jpg',
            'date' => '2007/07',
            'issue' => 'No.235',
        ),
        array(
            'image' => 'mangalist/jumbo/2007-08.jpg',
            'date' => '2007/08',
            'issue' => 'No.236',
        ),
    );
?>

<!-- Cover Gallery -->
<h2 class="subtitle">Cover Gallery</h2>
<p>This section showcases every cover of Comic Jumbo. Illustrations were done by <b>Hiroshi Kawamoto</b>. These cover pictures were compiled in a release called <b>Daiyaboureki</b>.</p>
	
    <div class="cgwrapper">
            <?php renderJumboCoverGallery($contents) ?>
    <div class="clear"></div>
    </div>