<h1>Tankoubon Book released</h1>
<span class="pageinfo">
Commercial Hentai comics are released in specialized Hentai magazines on monthly or bi-monthly bases.
    
Commercial hentai comics are released in monthly magazines such as <b>COMIC Megaplus</b> and then later compiled into seperate volumes. These volumes referred as Tankōbon (単行本) amongst western Manga fans but for the Japanese it simply means "comic book". On this page you can see a list of all JKP's tankōbons. You can click on the pictures to see detailed information of the particular volumes.</span>

<h2>Toen Shobo</h2>
<p>From 2003 until 2007 JKP's comics were released in <a href="/magazine/jumbo">Comic Jumbo</a>. Its publisher <b>Toen Shobo</b> (桃園書房) went bankrupt in mid-2007.</p>

<div class="cgwrapper">
		<div class="cgcenter">
				<div class="tankimg"><a href="/tankoubon/caligula"><img class="img3" src="/assets/images/tank/caligula.jpg"></a><br>Caligula Machine</div>
				<div class="tankimg"><a href="/tankoubon/curriculum"><img class="img3" src="/assets/images/tank/curriculum.jpg"></a><br>Curriculum</div>
				<div class="tankimg"><a href="/tankoubon/operation"><img class="img3" src="/assets/images/tank/operation.jpg"></a><br>Operation</div>
				<div class="tankimg"><a href="/tankoubon/fever"><img class="img3" src="/assets/images/tank/fever.jpg"></a><br>Fever</div>

	</div>
		<div class="cgcenter">
				<div class="tankimg"><a href="/tankoubon/jkpworld"><img class="img3" src="/assets/images/tank/jkpworld.jpg"></a><br>John K. Pe-Ta's World</div>
				<div class="tankimg"><a href="/tankoubon/explosion"><img class="img3" src="/assets/images/tank/explosion.jpg"></a><br>Explosion</div>
	</div>
</div>
<div class="clear"></div>

<h2>Core Magazine</h2>
<p>Since Toen Shobo is out of buisness JKP's comics released by <a href="http://www.coremagazine.co.jp/comic/comic/jyon_k_peta/">Core Magazine</a> in the HotMilk Comics line.</p>

<div class="cgwrapper">
		<div class="cgcenter">
				<div class="tankimg"><a href="/tankoubon/license"><img class="img3" src="/assets/images/tank/license.jpg"></a><br>License</div>
				<div class="tankimg"><a href="/tankoubon/screamer"><img class="img3" src="/assets/images/tank/screamer.jpg"></a><br>Screamer</div>
				<div class="tankimg"><a href="/tankoubon/balkan"><img class="img3" src="/assets/images/tank/balkan.jpg"></a><br>Balkan!!</div>
				<div class="tankimg"><a href="/tankoubon/monfest"><img class="img3" src="/assets/images/tank/mofesu.jpg"></a><br>Monfest XI</div>
	</div>
		<div class="cgcenter">    
				<div class="tankimg"><a href="/tankoubon/megabitch"><img class="img3" src="/assets/images/tank/mega_bitch.jpg"></a><br>Mega Bitch</div>
				<div class="tankimg"><a href="/tankoubon/destroy"><img class="img3" src="/assets/images/tank/mon_destroy.jpg"></a><br>Destroy</div>
				<div class="tankimg"><a href="/tankoubon/system"><img class="img3" src="/assets/images/tank/system.jpg"></a><br>System!</div>
				<div class="tankimg"><a href="/tankoubon/miraclehole"><img class="img3" src="/assets/images/tank/kiseki_no_ana.jpg"></a><br>Miracle Hole</div>
	</div>
</div>
<div class="clear"></div>

<h2>Reprints</h2>
<p>Core Magazine Re-released the older Toen Shobo tankoubons in it's own comics library. These Releases contain new contents and the cover art has been redrawn.</p>

<div class="cgwrapper">
		<div class="cgcenter">
				<div class="tankimg"><a href="/tankoubon/curriculumplus"><img class="img3" src="/assets/images/tank/curriculum_plus.jpg"></a><br>Curriculum Plus</div>
				<div class="tankimg"><a href="/tankoubon/operationplus"><img class="img3" src="/assets/images/tank/operation_plus.jpg"></a><br>Operation Plus</div>
				<div class="tankimg"><a href="/tankoubon/feverplus"><img class="img3" src="/assets/images/tank/fever_plus.jpg"></a><br>Fever Plus</div>
                <div class="tankimg"><a href="/tankoubon/explosionplus"><img class="img3" src="/assets/images/tank/explosion_plus.jpg"></a><br>Explosion Plus</div>
				<div class="tankimg"><a href="/tankoubon/caligulaplus"><img class="img3" src="/assets/images/tank/caligula_plus.jpg"></a><br>Caligula Plus</div>
	    </div>
</div>
<div class="clear"></div>

<h2>Digital Releases</h2>
<p>These releases first came out in Digital format.</p>

<div class="cgwrapper">
		<div class="cgcenter">
				<div class="tankimg"><a href="/tankoubon/maison"><img class="img3" src="/assets/images/tank/maison.jpg"></a><br>Maison</div>
				<div class="tankimg"><a href="/tankoubon/daigaku"><img class="img3" src="/assets/images/tank/daigaku.jpg"></a><br>Private Slug Holes Academy</div>
                <div class="tankimg"><a href="/tankoubon/acmate"><img class="img3" src="/assets/images/tank/acmate.jpg"></a><br>My Lovely Acmate</div>
                <div class="tankimg"><a href="/tankoubon/opportunity"><img class="img3" src="/assets/images/tank/opportunity.jpg"></a><br>Opportunity</div>
                <div class="tankimg"><a href="/tankoubon/kimochi"><img class="img3" src="/assets/images/tank/kimochi.jpg"></a><br>Every hole feels Good</div>
	    </div>
<div class="cgcenter">  
                <div class="tankimg"><a href="/tankoubon/freestyle"><img class="img3" src="/assets/images/tank/freestyle.jpg"></a><br>Freestyle</div>
                <div class="tankimg"><a href="/tankoubon/mesuana"><img class="img3" src="/assets/images/tank/mesuana.jpg"></a><br>Mesuana</div>
                <div class="tankimg"><a href="/tankoubon/anameito"><img class="img3" src="/assets/images/tank/anameito.jpg"></a><br>Gucchiri Ana Meito</div>
                <div class="tankimg"><a href="/tankoubon/meathole"><img class="img3" src="/assets/images/tank/meathole.jpg"></a><br>Meat Hole Full Package</div>
    </div>
</div>
<div class="clear"></div>