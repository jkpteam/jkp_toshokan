<h1>List of Magazine Releases</h1>
<span class="pageinfo">Commercial hentai comics are released in monthly magazines and then later compiled into seperate volumes. These volumes referred as Tankōbon (単行本) amongst western Manga fans but for the Japanese it simply means "comic book". On this page you can see a list of JKP comics sorted by magazine releases. You can click on the pictures to see detailed information of the particular magazines.</span>

<!-- Jumbo -->

<h2 class="subtitle">Serialization in COMIC Jumbo</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/jumbo">
    <img class="img3" src="/assets/images/mangalist/jumbo/2004-01.jpg">
    <img class="img2" src="/assets/images/mangalist/jumbo/2004-02.jpg">
    <img class="img1" src="/assets/images/mangalist/jumbo/2004-03.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Jumbo was a long running adult bishoujo manga magazine published by the now defunct Toen Shobo company. In total 236 issues were released from 1987 to 2007. Serialization of JKP's works started late 2002 with Issue 180.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミックジャンボ<br>
	<span>Circulation:</span>1987.12 – 2007.08<br>
	<span>Retail:</span>¥300 - ¥390<br>
	<span>Release Type:</span>Monthly; 6th<br>
	<span>Publisher:</span>Toen Shobou<br>
	<span>JKP Serialization:</span>2002.12 – 2006.11<br>
	<span>Issues:</span>45 issues<br>
	<span><a href="http://web.archive.org/web/20060407095717/http://www.touen.co.jp/co_ju.htm">Official website</a></span>(webarchive)
</p>
</div>
</div>

<!-- Megaplus -->

<h2 class="subtitle">Serialization in COMIC Megaplus</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/megaplus">
    <img class="img3" src="/assets/images/mangalist/megaplus/mp28b.jpg">
    <img class="img2" src="/assets/images/mangalist/megaplus/mp30b.jpg">
    <img class="img1" src="/assets/images/mangalist/megaplus/mp32b.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Megaplus was an adult bishoujo manga magazine released monthly by Coremagazine. It succeeded by Comic 0EX with the total number of issues by 50.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミックメガプラス<br>
	<span>Circulation:</span>2003.10 - 2007.11<br>
	<span>Retail:</span>¥680<br>
	<span>Release Type:</span>Monthly; 10th<br>
	<span>Publisher:</span>Core Magazine<br>
	<span>JKP Serialization:</span>2006.01 - 2007.11<br>
	<span>Issues:</span>20 issues<br>
	<span><a href="http://www.coremagazine.co.jp/megaplus">Official website</a></span>
</p>
</div>
</div>

<!-- 0EX -->

<h2 class="subtitle">Serialization in COMIC 0EX</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/0EX">
    <img class="img3" src="/assets/images/mangalist/ex/ex04.jpg">
    <img class="img2" src="/assets/images/mangalist/ex/ex02.jpg">
    <img class="img1" src="/assets/images/mangalist/ex/ex01.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic 0EX was an adult bishoujo manga magazine released monthly by Coremagazine. It is the successor of the defunct Comic Megaplus magazine. The magazine got renewed and became Comic MegaMilk.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミック0EX (コミックゼロエクス)<br>
	<span>Circulation:</span>2007.12 - 2010.05<br>
	<span>Retail:</span>¥680<br>
	<span>Release Type:</span>Monthly; 10th<br>
	<span>Publisher:</span>Core Magazine<br>
	<span>JKP Serialization:</span>2007.12 - 2010.05<br>
	<span>Issues:</span>29 issues<br>
	<span><a href="http://www.coremagazine.co.jp/zeroex/">Official website</a></span>
</p>
</div>
</div>

<!-- Megamilk -->

<h2 class="subtitle">Serialization in COMIC Megamilk</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/megamilk">
    <img class="img3" src="/assets/images/mangalist/megamilk/mm03b.jpg">
    <img class="img2" src="/assets/images/mangalist/megamilk/mm02b.jpg">
    <img class="img1" src="/assets/images/mangalist/megamilk/mm01b.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Megamilk was an adult bishoujo manga magazine released monthly by Coremagazine. It is the successor of the defunct Comic 0EX magazine. Since August 2012 the magazine is on a break with 26 released issues but the magazine return is unknown.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミックメガミルク<br>
	<span>Circulation:</span>2010.06 - ?<br>
	<span>Retail:</span>¥680<br>
	<span>Release Type:</span>Monthly; 10th<br>
	<span>Publisher:</span>Core Magazine<br>
	<span>JKP Serialization:</span>2010.06 - 2012.07<br>
	<span>Issues:</span>23 issues<br>
	<span><a href="http://megamilk.coremagazine.co.jp">Official blog</a></span>
</p>
</div>
</div>

<!-- Megastore H -->

<h2 class="subtitle">Serialization in COMIC Megastore H</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/megastoreh">
    <img class="img3" src="/assets/images/mangalist/megastoreh/msh110.jpg">
    <img class="img2" src="/assets/images/mangalist/megastoreh/msh109.jpg">
    <img class="img1" src="/assets/images/mangalist/megastoreh/msh108.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Megastore H was an adult bishoujo manga magazine released by Coremagazine. From 2011 publication changed to a bi-monthly status. Publication ended on 2013 and the magazine got renewed with the name Comic Megastore Alpha.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミックメガストアＨ<br>
	<span>Circulation:</span>2002.06 - 2013.07<br>
	<span>Retail:</span>¥680<br>
	<span>Release Type:</span>Monthly; Bi-monthly<br>
	<span>Publisher:</span>Core Magazine<br>
	<span>JKP Serialization:</span>2012.11 - 2013.07<br>
	<span>Issues:</span>5 issues<br>
	<span><a href="http://www.coremagazine.co.jp/comimegah/">Official website</a></span>
</p>
</div>
</div>

<!-- Magnum -->

<h2 class="subtitle">Serialization in COMIC Magnum</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/magnum">
    <img class="img3" src="/assets/images/mangalist/magnum/m058.jpg">
    <img class="img2" src="/assets/images/mangalist/magnum/m060.jpg">
    <img class="img1" src="/assets/images/mangalist/magnum/m062.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Magnum is an adult digital-only manga magazine released by online store DMM. It is released on a monthly basis on the first friday of every month. The concept of the magazine is extreme pleasure.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミックマグナム<br>
	<span>Circulation:</span>2009.05 - ?<br>
	<span>Retail:</span>¥540<br>
	<span>Release Type:</span>Monthly<br>
	<span>Publisher:</span>DMM<br>
	<span>JKP Serialization:</span>2014.02 - ?<br>
	<span>Issues:</span>13 issues<br>
	<span><a href="http://book.dmm.co.jp/series/?floor=Abook&series_id=11521">Official website</a></span>
</p></div>
</div>

<!-- Kuriberon -->

<h2 class="subtitle">Serialization in COMIC Kuriberon</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/kuriberon">
    <img class="img3" src="/assets/images/mangalist/kuriberon/kb041.jpg">
    <img class="img2" src="/assets/images/mangalist/kuriberon/kb038.jpg">
    <img class="img1" src="/assets/images/mangalist/kuriberon/kb041.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Kuriberon is an adult digital-only manga magazine.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>COMICクリベロン<br>
	<span>Circulation:</span>2012.10 -<br>
	<span>Retail:</span>¥540<br>
	<span>Release Type:</span>Monthly<br>
	<span>Publisher:</span>Kuriberon<br>
	<span>JKP Serialization:</span>2015.09 - ?<br>
	<span>Issues:</span>4 issues<br>
	<span><a href="http://www.kuriberon.jp/">Official website</a></span>
</p></div>
</div>

<!-- Megastore Deep -->

<h2 class="subtitle">Serialization in COMIC Megastore Deep</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/megastoredeep">
    <img class="img3" src="/assets/images/mangalist/megastoredeep/msd003.jpg">
    <img class="img2" src="/assets/images/mangalist/megastoredeep/msd002.jpg">
    <img class="img1" src="/assets/images/mangalist/megastoredeep/msd001.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Megastore Deep is an adult bishoujo digital-only manga magazine from publisher Coremagazine.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミックメガストアDEEP<br>
	<span>Circulation:</span>2015.12 -<br>
	<span>Retail:</span>¥756<br>
	<span>Release Type:</span>Monthly<br>
	<span>Publisher:</span>Core Magazine<br>
	<span>JKP Serialization:</span>2015.12 -<br>
	<span>Issues:</span>3 issues<br>
	<span><a href="#">Official website</a></span>
</p>
</div>
</div>

<!-- Megastore Alpha -->

<h2 class="subtitle">Serialization in COMIC Megastore Alpha</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/megastorea">
    <img class="img3" src="/assets/images/mangalist/megastorea/msa2016_08.jpg">
    <img class="img2" src="/assets/images/mangalist/megastorea/msa2016_07.jpg">
    <img class="img1" src="/assets/images/mangalist/megastorea/msa2016_01.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Megastore Alpha is an adult bishoujo manga magazine released by Coremagazine. It is the successor of the defunct Comic Megastore H magazine.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミックメガストアα<br>
	<span>Circulation:</span>2015.08 -<br>
	<span>Retail:</span>¥722<br>
	<span>Release Type:</span>Monthly<br>
	<span>Publisher:</span>Core Magazine<br>
	<span>JKP Serialization:</span>2016.01<br>
	<span>Issues:</span>2 issue<br>
	<span><a href="http://www.coremagazine.co.jp/alpha/">Official website</a></span>
</p>
</div>
</div>

<!-- Masyo -->

<h2 class="subtitle">Serialization in COMIC Masyo</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/masyo">
    <img class="img3" src="/assets/images/mangalist/masyo/masyo2018_09.jpg">
    <img class="img2" src="/assets/images/mangalist/masyo/masyo2019_02.jpg">
    <img class="img1" src="/assets/images/mangalist/masyo/masyo2019_04.jpg"></a>
</div>

<div class="info">
<div class="descript">Comic Masyo is an adult bishoujo manga magazine released by Sanwa publishing.</div>
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>コミック・マショウ<br>
	<span>Circulation:</span>2018.09 -<br>
	<span>Retail:</span>¥650<br>
	<span>Release Type:</span>Monthly<br>
	<span>Publisher:</span>Sanwa publishing<br>
	<span>JKP Serialization:</span>2018.06<br>
	<span>Issues:</span>1 issue<br>
	<span><a href="http://www.sanwa-pub.com/masyo/top.html">Official website</a></span>
</p>
</div>
</div>

<!-- Earlier works -->

<h2 class="subtitle">Earlier Pre-Monzetsu works</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/others">
    <img class="img3" src="/assets/images/mangalist/others/Chaotic_Cohabitation.png">
    <img class="img2" src="/assets/images/mangalist/others/Gizzard.png">
    <img class="img1" src="/assets/images/mangalist/others/Mushroom.png"></a>
</div>

<div class="info">
<div class="descript">These comics are JKP's earlier works and are not part of the Monzetsu series theme. The genre is varies from guro to general hentai. A few of them were never released in tankoubon or any other format.</div>
<h3>Release Information</h3>
<p>
	<span>Date:</span>1998 - 2002<br>
	<span>Publishers:</span>Kodansha, Core Magazine, Toen Shobou<br>
	<span>Titles:</span>9 titles
</p>
</div>
</div>

<!-- Tankoubon Omakes -->

<h2 class="subtitle">Tankobon exclusives</h2>
<div class="magazine-stack magazine">

<div class="cover">
    <a href="/magazine/omake">
    <img class="img3" src="/assets/images/mangalist/omake/Extra-Manga-Tentacle-Jet.png">
    <img class="img2" src="/assets/images/collaborations/sawayaka/167.png">
    <img class="img1" src="/assets/images/mangalist/omake/jkpworld_omake.jpg"></a>
</div>

<div class="info">
<div class="descript">These comics were came with the tankoubon releases as bonus content. That means these were never printed in magazine format.</div>
<h3>Release Information</h3>
<p>
	<span>Publishers:</span>Core Magazine, Toen Shobou<br>
	<span>Titles:</span>9 titles
</p>
</div>
</div>