<div class="cgcenter">
    <h1 class="tank_title_en">Waku Waku Monzetsu Maison<br> わくわく悶絶めぞん</h1>
    <div class="tank_cover"><img src="/assets/images/tank/maison/cover.jpg" alt="Waku Waku Monzetsu Maison Cover"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Magnum Vol.58 - 2014.02.07',
            'image' => array(
                'manga' => 'mangalist/magnum/m058.jpg',
                'chapter' => 'tank/maison/ch/004.png',
            ),
            'title' => array(
                'en' => 'Waku Waku one-sans Episode 1',
                'jp' => 'わくわく one-sans 第一話',
            ),
        ),
        array(
            'release' => 'Magnum Vol.60 - 2014.04.04',
            'image' => array(
                'manga' => 'mangalist/magnum/m060.jpg',
                'chapter' => 'tank/maison/ch/028.png',
            ),
            'title' => array(
                'en' => 'Waku Waku one-sans Episode 2',
                'jp' => 'わくわく one-sans 第二話',
            ),
        ),
        array(
            'release' => 'Magnum Vol.62 - 2014.06.06',
            'image' => array(
                'manga' => 'mangalist/magnum/m062.jpg',
                'chapter' => 'tank/maison/ch/052.png',
            ),
            'title' => array(
                'en' => 'Waku Waku one-sans Episode 3',
                'jp' => 'わくわく one-sans 第三話',
            ),
        ),
        array(
            'release' => 'Magnum Vol.64 - 2014.08.01',
            'image' => array(
                'manga' => 'mangalist/magnum/m064.jpg',
                'chapter' => 'tank/maison/ch/076.png',
            ),
            'title' => array(
                'en' => 'Waku Waku one-sans Episode 4',
                'jp' => 'わくわく one-sans 第四話',
            ),
        ),
        array(
            'release' => 'Magnum Vol.66 - 2014.10.03',
            'image' => array(
                'manga' => 'mangalist/magnum/m066.jpg',
                'chapter' => 'tank/maison/ch/100.png',
            ),
            'title' => array(
                'en' => 'Waku Waku one-sans Episode 5',
                'jp' => 'わくわく one-sans 第五話',
            ),
        ),
        array(
            'release' => 'Magnum Vol.68 - 2014.12.05',
            'image' => array(
                'manga' => 'mangalist/magnum/m068.jpg',
                'chapter' => 'tank/maison/ch/124.png',
            ),
            'title' => array(
                'en' => 'Waku Waku one-sans Episode 6',
                'jp' => 'わくわく one-sans 第六話',
            ),
        ),
        array(
            'release' => 'Magnum Vol.70 - 2015.02.06',
            'image' => array(
                'manga' => 'mangalist/magnum/m070.jpg',
                'chapter' => 'tank/maison/ch/148.png',
            ),
            'title' => array(
                'en' => 'Waku Waku one-sans Episode 7',
                'jp' => 'わくわく one-sans 第七話',
            ),
        ),
        array(
            'release' => 'Magnum Vol.72 - 2015.04.03',
            'image' => array(
                'manga' => 'mangalist/magnum/m072.jpg',
                'chapter' => 'tank/maison/ch/172.png',
            ),
            'title' => array(
                'en' => 'Waku Waku one-sans Last Episode',
                'jp' => 'わくわく one-sans 最終話',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Image Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>
<p>For the buyers of the webshop <a href="http://www.toranoana.jp/mailorder/article/20/0011/52/03/200011520345.html">toranoana.jp</a> a special Illustration Card is included in this volume.<br>
For the buyers of the webshop <a href="http://www.daito-i.com/top/comics/detail.php?code=9784860849597">Daito-Item</a> a special Message Card is included in this volume.<br>
For the buyers of the webshop <a href="http://www.autumnleaf.jp/shop/detail.php?code=9784860849597">Autumnleaf</a> a special Message Card is included in this volume.<br>
</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/maison/title.jpg"><img src="/assets/images/tank/maison/title.jpg" alt="Title" title="Title"></a><br>Title</div>				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/maison/message1.jpg"><img src="/assets/images/tank/maison/message1.jpg" alt="Message" title="Message"></a><br>Bonus Illustration Card</div>
				<div class="kep"><img src="/assets/images/tank/maison/message2.jpg" alt="Message" title="Message"><br>Autumnleaf Message</div>
				<div class="kep"><img src="/assets/images/tank/maison/message3.jpg" alt="Message" title="Message"><br>Daito-Item Message</div>
			</div>

	</div>
	<div class="clear"></div>

<h2 class="subtitle">Promotional One-Time only Manga</h2>
<p>For a limited number of buyers at <a href="https://www.melonbooks.co.jp/detail/detail.php?product_id=122853&adult_view=1">Melonbooks</a> a <a href="https://www.melonbooks.co.jp/shop/event_detail.php?wp_id=27&post_id=4327&type=privilege">Bonus manga</a> was included in this volume. This special's title is <b>"Waku Waku Monzetsu Maison Promotional One-Time only Manga" [わくわく悶絶めぞん 販促読み切りマンガ]</b>.</p>

	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><img src="/assets/images/tank/maison/ch/special1.jpg" alt="Special 1" title="Special 1"><br>Bonus Chapter</div>
				<div class="kep"><img src="/assets/images/tank/maison/ch/special2.jpg" alt="Special 2" title="Special 2"><br>Bonus Chapter</div>
			</div>
			</div>
	<div class="clear"></div>