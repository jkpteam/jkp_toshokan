<div id="leftside"><!-- body of document -->

<div class="entry">
<div><h1>2012/04/04 - Monzetsu System!</h1></div>
<div class="newsinfo">Posted by: JKP Admin | Tags: news, site development, new tankobon</div>
<p>Yaaay! <strong>Monzetsu System!</strong> scan is out. At last! If you guys can remember this book was released last year September! The cover art is awesome but what I like the most is that they translated the titles. My translation was fairly accurate but if these are the official translations then I will use them. You may notice that I didn't updated the pages to cover this new information. At least not yet. So until we can finish JKPToshokan's big update here is the contents of <strong>Monzetsu System!</strong> Yes, It's title is "<strong>Monzetsu System!</strong>". I bet some idiots will call it <em>Monzetsu Kei!</em> or something like it. But the kanji 系 (kei) means System, and by that it needs to be translated!</p>
    
<table class="manga">

<tr>
    <th>#</th>
    <th>English Title</th>
    <th>Magazine</th>
</tr>    
    
<tr>
    <td>006</td>
    <td>Brutal 23 o'clock</td>
    <td>MegaMilk Vol.14</td>
</tr>
    
<tr>
    <td>029</td>
    <td>Twists at daybreak</td>
    <td>MegaMilk Vol.10</td>
</tr>
    
<tr>
    <td>053</td>
    <td>New lily hell</td>
    <td>MegaMilk Vol.11</td>
</tr>
    
<tr>
    <td>077</td>
    <td>Give me pig</td>
    <td>MegaMilk Vol.09</td>
</tr>
    
<tr>
    <td>101</td>
    <td>Hyper contortion</td>
    <td>MegaMilk Vol.12</td>
</tr>
    
<tr>
    <td>125</td>
    <td>Graduation banned book</td>
    <td>MegaMilk Vol.13</td>
</tr>
    
<tr>
    <td>149</td>
    <td>Complex only one</td>
    <td>0EX Vol.24</td>
</tr>

<tr>
    <td>173</td>
    <td>Super stomach down</td>
    <td>MegaMilk Vol.06</td>
</tr>
</table>        

<p>And another thing I made a mistake in the Megamilk list below. I accidently swapped these two comics. Sorry about this.</p>

<span class="marker">
    <span style="color: #ff0000;">2012-02 - COMIC Megamilk Vol.20 - First time Miss Destroy</span><br>
    <span style="color: #008000;">2012-03 - COMIC Megamilk Vol.21 - The Girl with the Scar</span>
</span>
<p>The correct is:</p>
<span class="marker">
    <span style="color: #ff0000;">2012-02 - COMIC Megamilk Vol.20 - The Girl with the Scar</span><br>
    <span style="color: #008000;">2012-03 - COMIC Megamilk Vol.21 - First time Miss Destroy</span>
</span>
<p>Thats all for today guys! I have some reading to do.</p>
</div>

<div class="entry">
<div><h1>2012/03/24 - Work sloooowly progressing.</h1></div>
<div class="newsinfo">Posted by: JKP Admin | Tags: news, site development, new magazine</div>
<p>Hi guys! I just would like to give you some idea what's going on. So the site will be much more different soon. One of my friends helped me a lot... I mean most of the design work is done by him. So everything is progressing nicely. I won't tell you exactly what's happening. But I don't want to keep you in the dark either so it will be more content and nicer content.</p>
<p>I've compiled a list of the MegaMilk stuff so until the "rebirth" of the site use this.</p>
<p>
<span style="color: #008000;">Green - Magazine scanned, can be found.</span><br>
<span style="color: #ff0000;">Red - Not yet scanned.</span><br>
(No title) - JKP took a break. No JKP manga in this month.
</p>

<p>
<span class="marker">
    <span style="color: #008000;">2010-07 - COMIC Megamilk Vol.01 - Toilet Spirit<br></span>
    <span style="color: #008000;">2010-08 - COMIC Megamilk Vol.02 - Pink Fire Potato<br></span>
    <span style="color: #008000;">2010-09 - COMIC Megamilk Vol.03 - Red Hot Beach<br></span>
    <span style="color: #008000;">2010-10 - COMIC Megamilk Vol.04 - Crash on the Beach<br></span>
    <span style="color: #008000;">2010-11 - COMIC Megamilk Vol.05 - Midday Enigma Hero<br></span>
    <span style="color: #008000;">2010-12 - COMIC Megamilk Vol.06 - Super Stomach Down<br></span>
    <span style="color: #008000;">2011-01 - COMIC Megamilk Vol.07 - <br></span>
    <span style="color: #008000;">2011-02 - COMIC Megamilk Vol.08 - Exterminator from today!<br></span>
    <span style="color: #008000;">2011-03 - COMIC Megamilk Vol.09 - Give Me Pig<br></span>
    <span style="color: #008000;">2011-04 - COMIC Megamilk Vol.10 - Twisted by Dawn<br></span>
    <span style="color: #008000;">2011-05 - COMIC Megamilk Vol.11 - The New Yuri Hell</span><br>
    <span style="color: #ff0000;">2011-06 - COMIC Megamilk Vol.12 - Hyper Contortion</span><br>
    <span style="color: #008000;">2011-07 - COMIC Megamilk Vol.13 - Graduation Prohibited Book</span><br>
    <span style="color: #ff0000;">2011-08 - COMIC Megamilk Vol.14 - Brutal 23 hour</span><br>
    <span style="color: #008000;">2011-09 - COMIC Megamilk Vol.15 - Neverending Summer Deathfish</span><br>
    <span style="color: #008000;">2011-10 - COMIC Megamilk Vol.16 - </span> ???<br>
    <span style="color: #008000;">2011-11 - COMIC Megamilk Vol.17 - </span><br>
    <span style="color: #ff0000;">2011-12 - COMIC Megamilk Vol.18 - Lecture on Sex for Dohtei!!!</span><br>
    <span style="color: #008000;">2012-01 - COMIC Megamilk Vol.19 - </span><br>
    <span style="color: #ff0000;">2012-02 - COMIC Megamilk Vol.20 - The Girl with the Scar</span><br>
    <span style="color: #008000;">2012-03 - COMIC Megamilk Vol.21 - First time Miss Destroy</span><br>
    <span style="color: #ff0000;">2012-04 - COMIC Megamilk Vol.22 - </span>???
</span>
</p>
</div>

<div class="entry">
<div><h1>2011/11/20 - The last of the "Plus" Tankoubons</h1></div>
<div class="newsinfo">Posted by: JKP Admin | Tags: news, site development, new tankoubon</div>
<p><img align="left" alt="caliplus" src="/assets/images/news/caliplus.jpg" style="margin-right: 10px"></p>
<p>Hey guys! The updates will be coming, I just can't decide on a nice clean looking design for the subpages. A lot of work has already been done and it's only the design what's kinda off. It maybe seems to be a little unimportant but for me, it's just as important as the content itself. Now let's put this aside and talk about the good stuff.</p>
    
<p>The new Tankoubon is coming this December. It's possible this one will be the last of the old Tankoubon's reprint line. Yes, it's <a href="/tankoubon/caligula">Caligula Machine</a>, the first Tankoubon! I'm dying to see if they include some new material. And maybe they will include the release dates for the old comics too. Pretty please Coremagazine! Updated with a cover image.</p>
    
<p>What is also interesting is that the official MegaMilk blog forgot to upload the teaser for the comic, that appeared in MegaMilk Vol.16. So until they do or release it in the next Tankoubon, we won't know what kind of comic it is.</p>
    
<p>And lastly, JKP was on a break last month. It was his second break this year. The first break was in Vol.07 and now in Vol.17. So you won't be able to find any JKP material in these issues.</p>
</div>

<div class="entry">
<div><h1>2011/09/13 - System! and update</h1></div>
<div class="newsinfo">Posted by: JKP Admin | Tags: news, site development, new tankoubon</div>

<p>The new tankoubon <strong>"Monzetsu System!"</strong> is out. At last <strong>"Only One Complex!"</strong> is printed in tankoubon format as well. It came out in 0EX in November, 2009. I wonder why they decided to wait for so long. I thought <strong>"Twisted by Dawn"</strong> was a new tankoubon only comic, but It was printed in MegaMilk Vol. 10. They just forgot to add the Vol.10 tag on Megamilk's official blog. Of course the mangalist has been updated to cover this new tankoubon and I made a temporary page for it <a href="/tankoubon/system">"Monzetsu System!"</a> as well. I am still experimenting on a nice clean looking design for all tankoubon volumes, so please be patient. Updates will come. Later I will update the home page too.
</p>
<h4>Updated pages:</h4>
<ul>
<li><a href="/magazine/0EX">0EX list</a></li>
<li><a href="/magazine/megamilk">Megamilk list</a></li>
<li><a href="/tankoubon/system">Monzetsu System page</a></li>
</ul>

</div>

<div class="entry">
<h1>2011/08/27 - New Manga List and New Monzetsu</h1>
<div class="newsinfo">Posted by: JKP Admin | Tags: news, site development, new tankoubon</div>
<p><img align="left" alt="monsys" src="/assets/images/news/monsys.jpg" style="margin-right: 10px">Ok, so the plan was, to make the biggest possible update ever. Thats not gonna happen. I don't have the time nor the energy to do it. Before anyone misunderstand what I'm saying, I'm not abadoning JKP toshokan. Just the plans for that update changed. Instead of one big update, there will be many smaller ones as I can work on them.</p>
<p>Putting this to the side, I have done something very cool. It's the new <a href="/magazine">Manga List.</a></p>
<p>This list was a big mess. Now you actually can browse it. The <i>Coloured  Illustration</i> part is not done yet, it will be a gallery when I'm done with it.</p>
<p>Another awesome news is that the new Monzetsu is coming next month. Its title is simply <a href="http://www.amazon.co.jp/gp/product/4864361290">"Monzetsu System"</a>. It will be released on September 10th. I will update its contents when it's going to be available.</p>
</div>

<div class="entry">
<h1>2011/06/28 - I'm back</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, site development</div>
<p>It's not an actual update I just want to tell you guys that I was away for the past months and now I'm back. It was my last semester at college so I had to focus on my studies. Before this semester I started working on a big ... I mean BIG update but I couldn't finish it in time. I will try to finish it as soon as possible but now that I'm out of school I have other things to do.</p>
<p>I don't know how I'm gonna do this update. This will be either one big update, or many smaller ones. If I decide on the big, than that will be slower if smaller updates then it will be faster and more regular. If you have any thoughts about this just send me an e-mail.</p>
</div>

<div class="entry">
<h1>2011/02/11 - JKP sign auction</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news</div>
<p>This is a quick update. Nothing fancy. I just came across a nice collectible item on <del>Yahoo Auction</del> <ins>Link removed</ins>. It is a copy of Monfesu XI signed by JKP himself! I've got a copy of Monfesu XI already but this is really cool one of a kind item for a fan. I love the artwork. But it is a bit expensive for me.</p>
<figure><img src="/assets/images/news/monfes_signed.jpg" alt="monfesu"></figure>
</div>

<div class="entry">
<h1>2011/02/11 - Months and Months without update</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, site development</div>
<p>Hello everyone! Yeah, sorry for the lack of updates blah blah blah... Finally everything is up to date! We have 4 new manuscript from JKP and 2 new tankoubons, so there is much content to see.</p>
<p>First of all, the 4 new manuscripts. Yes, thats right 5 months but only 4 manuscripts.</p>
<p>In November they released <strong>"Midday Enigma Hero"</strong>, and <strong>"Super Stomach Down"</strong> was released in December. You will be surprised if you try to find JKP In the January issue of MegaMilk <small>(thats Volume 7 if you are wondering)</small> because he had some time off.<br>
In February there is <strong>"Exterminator from today!".</strong><br>And the newest story is called <strong>"Give Me Pig</strong>, which is in the March issue of MegaMilk!</p>
<p>Lets move on. So there was the re-release of Monzetsu Explosion. What's changed you might ask? Well, lets see.</p>
    
<p>First of all the new cover is quite obvious as you can see on the picture.</p>
    <figure><img src="/assets/images/news/exp-plus.jpg" alt="MegaMilk 1"></figure>
    
<p>The Volume title page has not changed much.</p>
    <figure><img src="/assets/images/news/exp-plus2.jpg" alt="MegaMilk 1"></figure>

<p>The first very important fact is that the <strong>"Contents"</strong> page is missing from the reissue, and instead of that the volume has a colour page. I would like to mention the fact that this color page was actually printed in the first Explosion under the title <strong>"Mon Cafe Gaiden"</strong>! But the text from it is missing from Explosion Plus!</p>
    <figure><img src="/assets/images/news/exp-plus3.jpg" alt="MegaMilk 1"></figure>

<p>The second big change is that after <strong>"Our Elegy"</strong> there is a new 6 pages long comic called <strong>"Rising Drunk Queen"</strong>. This comic is only printed in this volume!!</p>
    <figure><img src="/assets/images/news/exp-plus4.jpg" alt="MegaMilk 1"></figure>

<p><strong>Black Fuck 'N' Roll</strong>'s first 4 pages was originally printed in colour back in the COMIC Jumbo days, and these pages are in the correct coloured format as well.</p>
    <figure><img src="/assets/images/news/exp-plus5.jpg" alt="MegaMilk 1"></figure>

<p>The next thing I would like to mention is that <strong>"Peerless Goldrush"</strong> and <strong>"Furious!! "Bareback Curriculum"</strong> have switched places. It is curious to think that this is the third print of this story!!!</p>

    <figure><img src="/assets/images/news/exp-plus6.jpg" alt="MegaMilk 1"></figure>
<p>The last and probably most important of all is the new manuscript <strong>"Toilet Spirit"</strong>! This is a fun story featuring a ghost! That about it! It's a pretty fun Monzetsu Volume, because up to date this is the only Monzetsu that have a series from JKP. I hope in the future JKP will be doing something like this again!</p>

    <p>And let's move on to the big news!!</p>
<p>The next Monzetsu Volume is coming!! It's called <strong>Search and Monzetsu Destroy!</strong> It's coming out on March 10. I can't wait!</p>
</div>

<div class="entry">
<h1>2010/10/23 - Lots of News</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, site development</div>
<p>Well, where should I begin? Megamilk Volume 3 and 4 are out so we have 2 new and fresh comics from JKP. Volume 3 have <strong>"Red Hot Beach"</strong> which looks pretty neat, classic JKP. And Volume 4 have <strong>"Crash on the Beach"</strong>. So we have two nice summer comics with some energetic f*cking on the beach.</p>
<p>Another thing is... well, I was right. I have guessed that the next tankoubon will be the reprint of Monzetsu Explosion. And guess what? It will be! So yeah, next tankoubon is called <strong>Monzetsu Explosion Plus</strong> and if we can trust the advertisement it will have some new content too. Well, as usual... Oh right, it comes out on November 10.</p>
<span class="marker">ジョン・Ｋ・ぺー太先生の「MON絶!!エクスプロージョン」がコアマガジンより復刊します。その名も「MON絶!!エクスプロージョン プラス」。発売は１１月１０日予定。新作と、描き下ろし作品を収録してパワーアップさせます。昔持ってた人も持ってなかった人も気になったら買って下さい。</span>
<p>And some annoying stuff. This "free web hosting provider" I am using deleted a lot of stuff without any notice. Including the cleaned cover of Caligula Machine. I have worked on that sh*t for hours. Yeah, and I don't have a copy of it. So is there anyone who could send it to my email address? Any case I won't upload it again...</p>
</div>

<div class="entry">
<h1>2010/07/07 - What happend to 0EX?</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, new magazine</div>
<p><img src="/assets/images/news/mm01.jpg" align="left" alt="MegaMilk 1"></p>
<p>What did happen to 0EX? You might ask if you were waiting for the July issue of it. Well, <strong>0EX's publication has come to an end</strong>. So Volume 30 was the last issue, but you don't need to worry because there is a successor! The <strong>new magazine</strong> is called <a href="http://megamilk.coremagazine.co.jp" target="_blank">Megamilk</a>「コミックメガミルク」 and the first issue is out! It contains new story from JKP of course, called <strong>Attack House 303</strong>. So you should be looking for Comic Megamilk from now on. There was Jumbo then Megaplus then 0EX and now Megamilk. I hope this magazine will run longer than 0EX. The other thing is the mangalist has been updated. Oh, I almost forgot Volume 2 issue of Megamilk will come out this friday.</p>
<p>Just a little heads up. Megamilk Vol.2 is out and it contains new JKP comic titled <strong>"Pink Fire Potato"</strong>. Thats all.</p>
<p>Some small errors have been corrected. Thank you for noticing them. Next big update? Well I don't know... I don't have the time for it. What I would like to do in the far future: First every single tankoubon release gets its individual page. Then we will see...but I eventually want a detailed analisys of every manuscript. But small updates on recent stories, books will be regular as usual.</p>
</div>

<div class="entry">
<h1>2010/04/10 - Mega Bitch released</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news</div>
<p><img src="/assets/images/news/smmb.jpg" align="left" alt="Mega Bitch"></p>
<p>Hey, everyone! So Super Monzetsu Mega Bitch is out today. I hope it will show up soon. The cover is awesome! By far this is the most appealing cover design! Well at least for me. Click <a href="/assets/images/news/smmb_big.jpg">here</a> to see it a little bigger. So the contents are?</p>
<p>Super Monzetsu Mega Bitch<br>
HotMilk Comics number: 319<br>
Pages: 198</p>
    
<table class="manga">
    
<tr>
    <td>Glasses String Enthusiasm Bitch</td>
    <td>情熱のメガネヒモビッチ</td>
</tr>
    
<tr>
    <td>Inhuman Etiquette</td>
    <td>非道のエチケット</td>
</tr>
    
<tr>
    <td>Phantom Sidekick Homerun</td>
    <td>幻のサイキックホームラン</td>
</tr>
    
<tr>
    <td>Tokyo Drunk Pudding</td>
    <td>トーキョードランクプリン</td>
</tr>
    
<tr>
    <td>The Counter attack's Blasting Scram <br><small>(I am not sure about this one the because site has the wrong title.)</small></td>
    <td>魂の爆砕スクラム!!</td>
</tr>
    
<tr>
    <td>Magma Slider</td>
    <td>マグマスライダー</td>
</tr>
    
<tr>
    <td>Shaving on Neverending Summer</td>
    <td>常夏シェービング</td>
</tr>
</table>
<p>Yes only seven comics in this one. But these are awesome!! I will update the manga list next week.</p>
</div>

<div class="entry">
<h1>2010/04/08 - New 0EX</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news</div>
<p><img src="/assets/images/news/ex29.jpg" align="left" alt="0ex26" style="margin-right: 10px">Just some heads up. <b>Super Monzetsu Mega Bitch</b> will be released this Saturday. Yaay!! You can expect the lastest comics from 0EX to be covered in this volume. And of course the May issue of 0EX is out (#29). Including <b>Shutter Fucker Sister</b> (シャッターファッカーシスター). Sorry I don't get this title, I don't know how to translate it to English. But that was the case with <i>"Female Pig Princess Cash Cow"</i> also... I hope someone will try and scanlate it. Its very funny how nowadays JKP's comics are always on the bottom of the TOC. (Often the place of GAG manga.) It's been soo long since the last comic with coloured pages! Why? It was usual in the Megaplus era! Give me some coloured pages already!</p>
</div>

<div class="entry">
<h1>2010/03/24 - Removed Links</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, site development</div>
<p>Hey everyone!! I got an email today which shocked me. The website of <a href="http://www.coremagazine.co.jp" target="_blank">Coremagazine</a> has requested me to remove the links of the downloadable comics. Coremagazine has found this place? That means something... at least for me. Well, I've never wanted this website to become a place of illegal activities. As you all know hentai comics are not free. Artists make them and companies sell them for us. If they want me to remove the links of their products I will remove them happily. So the bottom line is: there will be no more links of downloadable comics on this website.</p>
</div>

<div class="entry">
<h1>2010/02/10 - Mega Bitch</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, new tankobon</div>
<p>Because I forgot the last time. Lazy me...so...</p>
    <p>From <a href="http://www.coremagazine.co.jp/zeroex/diary.html" target="_blank">Coremagazine Diary</a>:</p>
<span class="marker">ジョン・K・ぺー太先生のファンに朗報。新刊（ホットミルクコミックス）が４月１０日発売予定です。タイトルは「スーパーモンゼツメガビッチ」。楽しみにしていて下さい。</span>
    <blockquote>Good News for the fans of John K. Pe-ta. The new book (Hotmilk Comics) is decided to go on sale April 10th. Its title is "Super Monzetsu Mega Bitch" (Suupaa Monzetsu Mega Bicchi). Please wait for it.</blockquote>
</div>

<div class="entry">
<h1>2010/02/11 - Identified!!</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, site development</div>
<p>With the nickname of "SevenSky" surprised me with an email. He said he remembers the source for that JKP manuscript which I mentioned earlier.</p>
<p><img src="/assets/images/news/yumiko.jpg" align="left" alt="yumiko">
<strong>He wrote:</strong><br>
"The story goes as follows. A girl is late for school. When she arrives, she realizes she lost her D. Then a guy shows up asking if she lost golden D or whatever. Instead, she wants his P. The guy does her and it turns out her D was with her all along.</p>
<p>The character name is Yumiko, and I am sure I saw it from Machino Henmaru book scan. I was surprised to see JKP drawing in another author's book. I do not remember which book. It might have been from ゆみこ倶楽部 (yumiko club)."</p>
	<p>I knew some of Machino Henmaru's work but I am not really a fan of it. His style is very simplistic and too grotesque for my taste. It's weird... really weird. So with SevenSky's help it was easy to find this manuscript.</p>
<p>It's a Machino Henmaru book titled <a href="http://www.amazon.co.jp/gp/product/4807844180/ref=cm_cr_pr_product_top?ie=UTF8&amp;redirect=true">Sawayaka Abnormal</a>. It was released in 2007 before TOEN SHOBO went bankrupt.</p>
<span class="marker">『ご無沙汰ゆみこちゃん』以下16編のショートや短編と、オマケ漫画３頁と、『ジョン・Ｋ・ペー太の世界』で著者が寄贈した漫画のお返しみたいなペー太の『ゆみこvs謎仮面』３頁も収録。</span>
    <p>This 3 page JKP comic was an "omake", a gift for this book in return of "John K Pe-ta's World" Machino Henmaru's omake comic.<br>
LINK REMOVED  Machino Henmaru - Sawayaka Abnormal (2007)<br>
The next big update I think will be sometime in the summer at best.</p>
</div>

<div class="entry">
<h1>2010/01/05 - Lot of News</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, site development</div>
<p><img src="/assets/images/news/mutimuti.jpg" align="left" alt="mutimuti" style="margin-right: 10px">Site is up and running, everything works! I have a huge update for today. It was a huge amount of work but I think it was worth it. The <a href="/magazine" title="Manga List">Manga list</a> is now more accurate than ever! It still has missing information though. All of the manga titles are now translated for your reading pleasure. Also, check out the awesome new <a href="/anthology" title="Anthology Volume List"> Anthology list!</a> The <a href="/about">About</a> and <a href="/tankoubon">Tankoubon</a> pages are now near done as well.</p>
<p>The Reprint of Muchi Muchi! Monzetsu Fever was released in early December so grab a copy if you'd like. Its cover was redrawn but did not changed that much. It's still awesome. See the image on the left. This volume also contains a new story never released in tankoubon format called <a href="http://www.coremagazine.co.jp/zeroex/image/zeroex/20/21.jpg">Burning Jet Peach</a> and it's rather rough.
Well it's JKP alright. This story has a 5 page bonus drawn specifically for this book. It's.... well it's an animal thing. You'll see... </p>
<a class="nagyobb" href="/assets/images/news/170.jpg"><img src="/assets/images/news/170_sm.gif" align="right" title="Unknown JKP comic" alt="Unknown JKP comic" style="margin-left: 10px"></a>
<p>I've bought a copy of the popular &quot;<strong>Ingyaku! Nakadashi Onna Kyoushi</strong>&quot;, check it out <del>here</del> <ins>(link removed)</ins>! I couldn't scan the whole book only the cover. You can get it <del>here</del> <ins>LINK REMOVED</ins>. I'm going to make individual pages for every tankoubon volume released so far as the next update. Check out the first <a href="/tankoubon/caligula">test page</a> and another test page with a list of the contents of <a href="/tankoubon/jkpworld">John K. Pe-ta's World</a>.</p>
<p>Lastly I have something I cannot identify. It's a page from a JKP comic I've never see before. Maybe someone can help me identify it. See this image on the right.</p>
</div>

<div class="entry">
<h1>2009/10/22 - Fever Plus</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, new tankobon</div>
<p>From <a href="http://www.coremagazine.co.jp/zeroex/diary.html" target="_blank">Coremagazine Diary</a>:</p>
<span class="marker">ジョン・K・ペー太先生ファンの皆様に朗報。 2009年12月10日に「ムチムチ悶絶!!フィーバープラス」が発売決定。内容は「ムチムチ悶絶!!フィーバー」に描き下ろし作品１本、そして単行本未収録作品１本プラスしたものです。クリスマスプレゼントはこれで決定!!</span>
<blockquote>Good news for all John K. Pe-Ta sensei fans! It is decided that "Muchi Muchi Monzetsu!! Fever Plus" will be sold on 2009 december 10. The contents: the work drawn for "Muchi Muchi Monzetsu!! Fever", and a story not yet published in tankoubon format. Now you know what to get for christmas!!</blockquote>
</div>

<div class="entry">
<h1>2009/07/13 - Monfest 11 released</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, new tankobon, update</div>
<p>MonfestXI (Eleven) is released. You can find download links on the internet but please support the author by buying it instead. Manga list is updated!</p>
</div>

<div class="entry">
<h1>2009/06/15 - Monfest 11 release info</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news</div>
    <p>From <a href="http://www.coremagazine.co.jp/zeroex/diary.html" target="_blank">Coremagazine Diary</a>:</p>
<span class="marker">2009年7月10日発売予定、ジョン・K・ペー太先生の最新刊「モンフェスXI」。見るものを魅了するエロスと肉体芸術は必見!!発売日の7月10日を指折り数えて待っててください。</span>
</div>

<div class="entry">
<h1>2009/03/21 - Monfest 11</h1>
    <div class="newsinfo">Posted by: JKP Admin | Tags: news, new tankobon</div>
    <p>From <a href="http://www.coremagazine.co.jp/zeroex/diary.html" target="_blank">Coremagazine Diary</a>:</p>
<span class="marker">ジョン・K・ペー太先生ファンの皆様に朗報。2009年7月10日に最新単行本「モンフェスXI(イレブン)」が発売決定。楽しみに待ってて下さい。</span>
    <blockquote>Good news for all John K. Pe-Ta sensei's fans! The lastest tankoubon "Monfes XI (eleven)" is decided to  go on sale 2009 July 10th.</blockquote>
    <p>Go and pre-order it as I did!!</p>
</div>

</div>
<!-- End of main body of document -->