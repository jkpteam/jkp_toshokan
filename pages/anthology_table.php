<h1>Anthology releases</h1>
<span class="pageinfo">This page showcases all of the TOEN Comics anthology volumes which included a manuscript from JKP. Each compilation features a particular theme. Some of them are specifically drawn for the given release and featured with a cover illustration. There are eight volumes where JKP is featured on the cover. These are highlited in the table of the bottom of this page.</span>

<!-- Magazine List -->
	<table class="manga">
        
        <tr>
            <th>Japanese/English Title</th>
            <th>Original Publication</th>
            <th>Corresponding Tankoubon</th>
        </tr>
        
		<tr>
			<td class="cim">逆噴射インチキメガネ<br>Reverse Thrust! Bespectacled Cheater</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>
        
		<tr>
			<td class="cim">熱血！悶絶！しごき部長<br>Hot Blooded! Fainting in Agony! The Hard Working Captain</td>
			<td><div class="mag-date">COMIC Jumbo No.187<br>2003/07</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>

		<tr>
			<td class="cim">拘束.肛虐.パパラッチ<br>Restraint, Anal Tyranny, Paparazzi</td>
			<td><div class="mag-date">COMIC Jumbo No.191 2003/11</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>        
        
		<tr>
			<td class="cim colored">放課後ブローバック<br>Afterschool Blowback</td>
			<td class="magdate colored"><div class="mag-date colored">Huge Breast Rape<br>Excellent</div></td>
			<td class="tank colored">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>        
        
		<tr>
			<td class="cim">屈辱の桃尻チャレンジ<br>Peach Ass Challenge of Disgrace</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>

		<tr>
			<td class="cim">ガチん娘レッスン乱れ責め<br>Submissive Girl Lesson, Disorderly Punishment</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>
        
		<tr>
			<td class="cim">妹地獄ピンク風呂<br>Sister's Pink Bath Hell</td>
			<td><div class="mag-date">COMIC Jumbo No.190 2003/10</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>

		<tr >
			<td class="cim colored">さよなら絶頂先生<br>Goodbye, Climax Sensei</td>
			<td class="magdate colored"><div class="mag-date">Women Manga-ka<br>Excellent </div></td>
			<td class="tank colored">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>

        <tr>
			<td class="cim colored">夜勤4:44<br>Night Shift 4:44</td>
			<td class="magdate colored"><div class="mag-date">Nurse Rape<br>Excellent</div></td>
			<td class="tank colored">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>

 		<tr>
			<td class="cim">地獄温泉羞恥の湯<br>Hell Spa, Hot Bath of Shame</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr> 

 		<tr>
			<td class="cim colored">第3次爆乳サミットα<br>Three Part Milk Explosion Summit Alpha</td>
			<td class="magdate colored"><div class="mag-date">COMIC Jumbo No.189<br>2003/09</div></td>
			<td class="tank colored">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
		</tr>         
            
		<tr>
			<td class="cim colored">乱乳パンツ仮面<br>Milk Riot - Panty Mask</td>
			<td class="magdate colored"><div class="mag-date">Married Woman<br>Rape Excellent</div></td>
			<td class="tank colored">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>        
        
 		<tr>
			<td class="cim">薔薇十字に揉みしだけ！！<br>The Rosicrucian Massage!!</td>
			<td><div class="mag-date">COMIC Jumbo No.194 2004/02</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr> 
        
 		<tr>
			<td class="cim">禁断のミックスジュース<br>Forbidden Juice Mix</td>
			<td><div class="mag-date">COMIC Jumbo No.195 2004/03</div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr> 
        
  		<tr>
			<td class="cim colored">どしゃぶり行進曲<br>Downpour March</td>
			<td class="magdate colored"><div class="mag-date">After School FUCK<br>Excellent</div></td>
			<td class="tank colored">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr> 
        
		<tr>
			<td class="cim">ぬきうち！屈辱Vの字斬り<br>Sudden Attack! Letter V of Disgrace</td>
			<td><div class="mag-date">COMIC Jumbo<br>N/D</div></td>
			<td class="tank">激！！悶絶オペレーション<br>Geki!! Fainting in Agony Operation</td>
		</tr>        
        
 		<tr>
			<td class="cim colored">爆裂妹シミュレーション<br>Explosion: Little Sister Simulation</td>
			<td class="magdate colored"><div class="mag-date">Imouto Moe<br>Excellent</div></td>
			<td class="tank colored">ムチムチ！！悶絶フィーバー<br>Muchi Muchi!! Fainting in Agony Fever</td>
		</tr>     
        
			<td class="cim">直球！悩殺オペレーション<br>Straight Pitch! Bewithchment Operation</td>
			<td><div class="mag-date">COMIC Jumbo No.188<br>2003/08</div></td>
			<td class="tank">悶絶カリギュラマシーン<br>Fainting in Agony Caligula Machine</td>
        
		<tr>
			<td class="cim colored">目覚めよ！！万力体操<br>Wake Up! Vise Calisthenics</td>
			<td class="magdate colored"><div class="mag-date">Hentai Virginity<br>Loss Excellent</div></td>
            <td class="tank colored">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>        
        
 		<tr>
			<td class="cim">放課後コケシ固め<br>Hard After - School Doll</td>
			<td><div class="mag-date">COMIC Jumbo No.197 2004/05 </div></td>
			<td class="tank">超悶絶カリキュラム<br>Ultra Fainting in Agony Curriculum</td>
		</tr>     
        
  		<tr>
			<td class="cim colored">激烈！！生ハメカリキュラム<br>Furious!! Bareback Curriculum</td>
			<td><div class="mag-date">Ingyaku! Female<br>Teacher Cream Pie</div></td>
			<td class="tank colored">MON絶!!エクスプロージョン<br>Fainting in Agony!! Explosion</td>
		</tr>      
        
</table>

<!--

<h2 class="subtitle">Excellent Series</h2>
<div class="anthology-stack anthology">

<div class="anthology_cover">
    <a href="/anthology/excellent">
        <img class="img3" src="/assets/images/anthology/excellent_3l.jpg">
        <img class="img2" src="/assets/images/anthology/excellent_abnormalvirginloss.jpg">
        <img class="img1" src="/assets/images/anthology/excellent_afterschoolfuck.jpg">
    </a>
</div>

<div class="anthology_info">
<h3>Release Information</h3>
<p>
	<span>Japanese:</span>眼鏡っ娘Excellent<br>
	<span>Released:</span>2003/12/29<br>
	<span>Series:</span>Excellent<br>
	<span>Publisher:</span>Touen Shobo<br>
	<span>Genre:</span>Futanari, Huge Breasts, Megane<br>
	<span>Theme:</span>Glasses<br>
	<span>Featured JKP</span>Reverse Thrust! Bespectacled Cheater
</p>
</div>
</div>

<h2 class="subtitle">Paradise Series</h2>

<h2 class="subtitle">Best Of Series</h2>

<h2 class="subtitle">Climax Series</h2>

-->