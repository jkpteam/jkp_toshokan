<div class="cgcenter">
        <h1 class="tank_title_en">Meat Hole Full Package</h1>
        <h1 class="tank_title_jp">肉穴フルパッケージ</h1>
    <div class="tank_cover"><img src="/assets/images/tank/meathole/cover-full.jpg" alt="Meat Hole Full Package" style="max-height: 250px"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Masyo 2021.03 - 2021.01.22',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2021_03.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2021_03.jpg',
            ),
            'title' => array(
                'en' => 'Meat hole full package',
                'jp' => '肉穴フルパッケージ',
            ),
        ),  
        array(
            'release' => 'Masyo 2020.07 - 2019.05.23',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2020_07.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2020_07.jpg',
            ),
            'title' => array(
                'en' => 'A veteran female warrior and my journey',
                'jp' => '歴戦の女戦士と僕の旅',
            ),
        ),
        array(
            'release' => 'Masyo 2020.10 - 2020.08.23',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2020_10.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2020_10.jpg',
            ),
            'title' => array(
                'en' => 'A veteran female warrior and my journey Part #2',
                'jp' => '歴戦の女戦士と僕の旅 ２話',
            ),
        ),
        array(
            'release' => 'Masyo 2021.01 - 2020.12.02',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2021_01.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2021_01.jpg',
            ),
            'title' => array(
                'en' => 'Fatality Academy',
                'jp' => '学園フェイタリティ',
            ),
        ),
        array(
            'release' => 'Masyo 2020.04 - 2019.02.22',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2020_04.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2020_04.jpg',
            ),
            'title' => array(
                'en' => 'Peeing Yankee Onihara',
                'jp' => 'おもらしヤンキー鬼原さん',
            ),
        ),
        array(
            'release' => 'Masyo 2019.12 - 2019.10.24',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2019_12.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2019_12.jpg',
            ),
            'title' => array(
                'en' => 'Mr. Shinobu Kunoichi Shinozaki',
                'jp' => '新米くノ一忍崎さん',
            ),
        ),
        array(
            'release' => 'Masyo 2019.09 - 2019.07.24',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2019_09.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2019_09.jpg',
            ),
            'title' => array(
                'en' => 'Endless Summer JetStream',
                'jp' => '常夏ジェットストリーム',
            ),
        ),
        array(
            'release' => 'Masyo 2019.04 - 2019.02.23',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2019_04.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2019_04.jpg',
            ),
            'title' => array(
                'en' => 'Child subject A',
                'jp' => '被験体A子',
            ),
        ),
        array(
            'release' => 'Masyo 2019.02 - 2019.01.02',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2019_02.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2019_02.jpg',
            ),
            'title' => array(
                'en' => 'The Ripe Orange Venus Fly Trap',
                'jp' => '色仕掛けの完熟オレンジ',
            ),
        ),
        array(
            'release' => 'Masyo 2018.09 - 2018.07.24',
            'image' => array(
                'manga' => 'mangalist/masyo/masyo2018_09.jpg',
                'chapter' => 'mangalist/masyo/ch/masyo2018_09.jpg',
            ),
            'title' => array(
                'en' => 'Super Hall Mr. Ise',
                'jp' => 'スーパーホール伊加瀬さん',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>



<span class="marker">
<h3>Notes:</h3>
    <p>The contents in the cover is wrong. "Fatality Academy" comes before "Peeing Yankee Onihara" and "Mr. Shinobu Kunoichi Shinozaki".</p>
	<div class="cgwrapper">
			<div class="cgcenter">				
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/meathole/contents_wrong.jpg">
                    <img src="/assets/images/tank/meathole/contents_wrong.jpg" alt="contents" title="contents"></a></div>
			</div>
	</div>
</span>

<!-- Image Gallery-->

<h2 class="subtitle">Colored Pages Gallery</h2>

<p>This volume features colored illustrations.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/meathole/color01.jpg">
					<img src="/assets/images/tank/meathole/color01.jpg" alt="Color 1" title="Color 1"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/meathole/color02.jpg">
					<img src="/assets/images/tank/meathole/color02.jpg" alt="Color 2" title="Color 2"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/meathole/back.jpg">
					<img src="/assets/images/tank/meathole/back.jpg" alt="Back" title="Back"></a>
				</div>
        </div>
			<div class="clear"></div>
</div>

<h2 class="subtitle">Promotional One-Time only Manga</h2>
<p>For a limited number of buyers at <a href="https://www.melonbooks.co.jp/detail/detail.php?product_id=772396">Melonbooks</a> a bonus 4 page was included in this volume.</p>
<p>For a limited number of buyers at <a href="?">Toraoana</a> a bonus 4 page story was included in this volume.</p>

	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><img src="/assets/images/tank/meathole/omake/melon-leaflet1-4.jpg" alt="Melonbooks bonus" title="Melonbooks bonus"><br>Melonbooks bonus</div>
				<div class="kep"><img src="/assets/images/tank/meathole/omake/melon-leaflet3-4.jpg" alt="Melonbooks bonus ad" title="Melonbooks bonus ad"><br>Advertisement</div>
				<div class="kep"><img src="/assets/images/tank/meathole/omake/melon-leaflet-ad.png" alt="Melonbooks bonus ad" title="Melonbooks bonus ad"><br>Advertisement</div>
                <div class="kep"><img src="/assets/images/tank/meathole/omake/to-leaflet-1.jpg" alt="to" title="to"><br>Toraoana</div>
			</div>
    </div>
	<div class="clear"></div>

<h2 class="subtitle">Sign</h2>

<p>Twitter <a href="https://twitter.com/jkp55645/status/1352392756740558853">post</a>.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/meathole/meathole-sign.jpg">
					<img src="/assets/images/tank/meathole/meathole-sign.jpg" alt="Sign" title="Sign"></a>
				</div>
        </div>
			<div class="clear"></div>
</div>
