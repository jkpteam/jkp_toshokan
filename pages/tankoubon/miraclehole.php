<div class="cgcenter">
        <h1 class="tank_title_en">Miracle Hole</h1>
        <h1 class="tank_title_jp">奇跡の穴</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/miraclehole/cover.jpg"><img src="/assets/images/tank/miraclehole/cover.jpg" alt="Miracle Hole Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Megastore H Vol. 108 - 2012.11',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh108.jpg',
                'chapter' => 'tank/miraclehole/ch/003.png',
            ),
            'title' => array(
                'en' => 'Spring Napalm Stretch',
                'jp' => '青春ナパームストレッチ！！',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.26 - 2012.07.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm26b.jpg',
                'chapter' => 'tank/miraclehole/ch/023.png',
            ),
            'title' => array(
                'en' => 'Pink Bath Death',
                'jp' => '桃色BathDeath',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.25 - 2012.06.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm25b.jpg',
                'chapter' => 'tank/miraclehole/ch/047.png',
            ),
            'title' => array(
                'en' => 'Somersault 30 liters',
                'jp' => 'サマーソルト30リットル',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.24 - 2012.05.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm24b.jpg',
                'chapter' => 'tank/miraclehole/ch/071.png',
            ),
            'title' => array(
                'en' => 'Light blue Chocolate',
                'jp' => '水色チョコレート',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.23 - 2012.04.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm23b.jpg',
                'chapter' => 'tank/miraclehole/ch/095.png',
            ),
            'title' => array(
                'en' => 'Two people just hanging out',
                'jp' => 'はみ出す二人',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.22 - 2012.03.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm22b.jpg',
                'chapter' => 'tank/miraclehole/ch/119.png',
            ),
            'title' => array(
                'en' => 'LSD! Lucky Sensei Dynamite',
                'jp' => 'L・S・D！ ラッキー先生ダイナマイト',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.21 - 2012.02.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm21b.jpg',
                'chapter' => 'tank/miraclehole/ch/143.png',
            ),
            'title' => array(
                'en' => 'First time Miss Destroy',
                'jp' => '第一回ミスデストロイ',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.20 - 2012.01.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm20b.jpg',
                'chapter' => 'tank/miraclehole/ch/167.png',
            ),
            'title' => array(
                'en' => 'The Girl with the Scar',
                'jp' => 'スカーな彼女',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.16 - 2011.09.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm16b.jpg',
                'chapter' => 'tank/miraclehole/ch/191.png',
            ),
            'title' => array(
                'en' => 'Lucky Summer Lesson',
                'jp' => 'ラッキーサマーレッスン',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.15 - 2011.08.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm15b.jpg',
                'chapter' => 'tank/miraclehole/ch/215.png',
            ),
            'title' => array(
                'en' => 'Neverending Summer Deathfish',
                'jp' => '常夏デスフィッシュ',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
<p><b>1. Neverending Summer Deathfish</b> have a 3 page <b>"Bonus Tentacle" [おまけの触手]</b> addition for this release only.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><img src="/assets/images/tank/miraclehole/ch/239.png" alt="p1" title="p1"><br>Bonus Tentacle</div>
				<div class="kep"><img src="/assets/images/tank/miraclehole/ch/240.png" alt="p2" title="p2"><br>Page 2</div>
				<div class="kep"><img src="/assets/images/tank/miraclehole/ch/241.png" alt="p3" title="p3"><br>Page 3</div>
			</div>
			<div class="clear"></div>
	</div>
<p><b>2. Spring Napalm Stretch</b> was released in <a href="/tankoubon/daigaku">Private Slug Holes Academy</a> as well.</p>
</span>


<h2 class="subtitle">Sign</h2>
<div class="atogaki">
    <img src="/assets/images/tank/miraclehole/sign.jpg" alt="atogaki" title="sign">
    <p></p>
</div>