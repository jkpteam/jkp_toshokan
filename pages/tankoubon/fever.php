<div class="cgcenter">
        <h1 class="tank_title_en">Full Buxom!! Fainting in Agony Fever</h1>
        <h1 class="tank_title_jp">ムチムチ！！悶絶 フィーバー</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/fever/cover.jpg"><img src="/assets/images/tank/fever/cover.jpg" alt="Full Buxom!! Fainting in Agony Fever Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No.208 - 2005.04',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/fever/ch/005.png',
            ),
            'title' => array(
                'en' => 'Sleeping Assault Guerrillas',
                'jp' => 'スリーピング突撃ゲリラ',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.209 - 2005.05',
            'image' => array(
                'manga' => 'mangalist/jumbo/2005-05.jpg',
                'chapter' => 'tank/fever/ch/023.png',
            ),
            'title' => array(
                'en' => 'Forbidden Planet W.C.',
                'jp' => '禁断惑星W・C',
            ),
        ),
        array(
            'release' => 'Married Woman Rape Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_hitozumaraipe.jpg',
                'chapter' => 'tank/fever/ch/041.png',
            ),
            'title' => array(
                'en' => 'Milk Riot - Panty Mask',
                'jp' => '乱乳パンツ仮面',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.209 - 2005.05',
            'image' => array(
                'manga' => 'mangalist/jumbo/2005-05.jpg',
                'chapter' => 'tank/fever/ch/049.png',
            ),
            'title' => array(
                'en' => 'Arousal Jetstream',
                'jp' => '挑発ジェットストリーム',
            ),
        ),
        array(
            'release' => 'After School FUCK Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_afterschoolfuck.jpg',
                'chapter' => 'tank/fever/ch/067.png',
            ),
            'title' => array(
                'en' => 'Downpour March',
                'jp' => 'どしゃぶり行進曲',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo 2005?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/fever/ch/075.png',
            ),
            'title' => array(
                'en' => 'In Other Words, The Electric Shock Virgin',
                'jp' => 'つまみ出し電撃バージン',
            ),
        ),
		array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/fever.jpg',
                'chapter' => 'tank/fever/ch/093.png',
            ),
            'title' => array(
                'en' => 'What if Series',
                'jp' => 'もしもシリーズ',
            ),
        ),	
        array(
            'release' => 'COMIC Jumbo No.207 - 2005.03',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/fever/ch/095.png',
            ),
            'title' => array(
                'en' => 'Mania! Pleasure Addict',
                'jp' => '熱中！快感アディクト',
            ),
        ),
		array(
            'release' => 'Nurse Rape Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_nursrape.jpg',
                'chapter' => 'tank/fever/ch/113.png',
            ),
            'title' => array(
                'en' => 'Night Shift 4:44',
                'jp' => '夜勤4:44',
            ),
        ),
		array(
            'release' => 'COMIC Jumbo 2005?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/fever/ch/121.png',
            ),
            'title' => array(
                'en' => 'Counterattack: Shine Spark',
                'jp' => '逆襲シャインスパーク',
            ),
        ),	
		array(
            'release' => 'Imouto Moe Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_imoutomoe.jpg',
                'chapter' => 'tank/fever/ch/139.png',
            ),
            'title' => array(
                'en' => 'Explosion: Little Sister Simulation',
                'jp' => '爆裂妹シミュレーション',
            ),
        ),
		array(
            'release' => 'COMIC Jumbo No.212 - 2005.08',
            'image' => array(
                'manga' => 'mangalist/jumbo/2005-08.jpg',
                'chapter' => 'tank/fever/ch/147.png',
            ),
            'title' => array(
                'en' => 'Let\'s Secret Coffee Shop',
                'jp' => 'Let\'s秘密喫茶',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Images Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

<p>The colored title image is for the chapter called <strong>Explosion: Little Sister Simulation</strong> was first printed on the cover of a book titled <strong>Imouto Moe Excellent</strong> which you can find on the <a href="/anthology">anthology</a> list. The colored image of <strong>Downpour March</strong> was also printed on the cover for a similar anthology book titled <strong>After School FUCK Excellent</strong>. The chapter called <strong>Milk Riot - Panty Mask</strong> was also printed first in a similar book called <strong>Married Woman Rape Excellent</strong> and it was also featured on the cover. <strong>Night Shift 4:44</strong> colored page was not included in this book but it was also the featured cover illustration for a similar anthology book called <strong>Nurse Rape Excellent</strong>. Later this image made a comeback in <a href="/tankoubon/feverplus">Full Buxom!! Fainting in Agony Fever Plus.</a></p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/fever/contents.png">
					<img src="/assets/images/tank/fever/contents.png" alt="Contents" title="Contents"></a><br>Contents
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/fever/title.jpg">
					<img src="/assets/images/tank/fever/title.jpg" alt="title" title="Title Image (Explosion: Little Sister Simulation)"></a><br>Title Page</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/fever/excellent_afterschoolfuck_color.jpg">
					<img src="/assets/images/tank/fever/excellent_afterschoolfuck_color.jpg" alt="color" title="Downpour March color"></a><br>Foldout #1</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/fever/excellent_hitozumaraipe_color.jpg">
					<img src="/assets/images/tank/fever/excellent_hitozumaraipe_color.jpg" alt="color" title="Milk Riot - Panty Mask color"></a><br>Foldout #2</div>
				
			</div>
			<div class="clear"></div>
	</div>