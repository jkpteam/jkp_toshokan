<div class="cgcenter">
        <h1 class="tank_title_en">Roar!! Fainting in Agony Screamer</h1>
        <h1 class="tank_title_jp">轟け！！悶絶スクリーマー</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/screamer/cover.jpg"><img src="/assets/images/tank/screamer/cover.jpg" alt="Roar!! Fainting in Agony Screamer Cover"></a></div>
</div>


<?php
    $contents = array(
        array(
            'release' => 'Megaplus Vol.45 - 2007.06.08',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp45b.jpg',
                'chapter' => 'tank/screamer/ch/007.png',
            ),
            'title' => array(
                'en' => 'Sneaking Bakuretsu Mission',
                'jp' => 'スニーキング爆裂ミッション！',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.44 - 2007.05.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp44b.jpg',
                'chapter' => 'tank/screamer/ch/023.png',
            ),
            'title' => array(
                'en' => 'Twilight Snapping Turtle',
                'jp' => '夕暮れスナッピングタートル',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.43 - 2007.04.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp43b.jpg',
                'chapter' => 'tank/screamer/ch/043.png',
            ),
            'title' => array(
                'en' => 'Massive Ten Commandments!!',
                'jp' => '十戒のマッシブ!!',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.42 - 2007.03.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp42b.jpg',
                'chapter' => 'tank/screamer/ch/063.png',
            ),
            'title' => array(
                'en' => 'Lust Overdose',
                'jp' => '欲求オーバードーズ',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.41 - 2007.02.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp41b.jpg',
                'chapter' => 'tank/screamer/ch/083.png',
            ),
            'title' => array(
                'en' => 'Avalanche Thunder Road',
                'jp' => '雪崩式サンダーロード',
            ),
        ),		
        array(
            'release' => 'Jumbo No.227 -  2006.11',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-11.jpg',
                'chapter' => 'tank/screamer/ch/103.png',
            ),
            'title' => array(
                'en' => 'Youth Destroyer',
                'jp' => '青春デストロイヤー',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.46 - 2007.07.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp46b.jpg',
                'chapter' => 'tank/screamer/ch/121.png',
            ),
            'title' => array(
                'en' => 'Fire Starter',
                'jp' => 'ファイアースタータ',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.47 - 2007.08.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp47b.jpg',
                'chapter' => 'tank/screamer/ch/141.png',
            ),
            'title' => array(
                'en' => 'Summer Riot \'07',
                'jp' => 'サマーライアット\'07',
            ),
        ),
        array(
            'release' => 'Jumbo No.223 -  2006.07',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-07.jpg',
                'chapter' => 'tank/screamer/ch/161.png',
            ),
            'title' => array(
                'en' => 'Furious!! Exciting Store!',
                'jp' => '激烈！！エキサイティングストア',
            ),
        ),
        array(
            'release' => 'Jumbo No.225 - 2006.09',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-09.jpg',
                'chapter' => 'tank/screamer/ch/179.png',
            ),
            'title' => array(
                'en' => 'Love Jet 10 000',
                'jp' => 'ラブジェット10000',
            ),
        ),
        array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/screamer.jpg',
                'chapter' => 'tank/screamer/ch/197.png',
            ),
            'title' => array(
                'en' => 'Extra Manga Tentacle Jet!!',
                'jp' => 'オマケマンガ触手JET!!',
            ),
        ),
    );
?>


<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Image Gallery-->

<h2 class="subtitle">Colored Pages Gallery</h2>

<p>This volume features two chapters with colored illustrations. <b>"Sneaking Bakuretsu Mission"</b> have 4 colored pages as an introduction. <b>"Youth Destroyer"</b> have a colored title page in No.223 of COMIC Jumbo but this illustration is only featured in its original colored format in this volume's cover foldout.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/screamer/003.jpg">
					<img src="/assets/images/tank/screamer/003.jpg" alt="Sneaking Bakuretsu Mission" title="Sneaking Bakuretsu Mission"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/screamer/004.jpg">
					<img src="/assets/images/tank/screamer/004.jpg" alt="Sneaking Bakuretsu Mission" title="Sneaking Bakuretsu Mission"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/screamer/005.jpg">
					<img src="/assets/images/tank/screamer/005.jpg" alt="Sneaking Bakuretsu Mission" title="Sneaking Bakuretsu Mission"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/screamer/006.jpg">
					<img src="/assets/images/tank/screamer/006.jpg" alt="Sneaking Bakuretsu Mission" title="Sneaking Bakuretsu Mission"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/screamer/color_youthdestroyer.jpg">
					<img src="/assets/images/tank/screamer/color_youthdestroyer.jpg" alt="Youth Destroyer" title="Youth Destroyer"></a>
				</div>
			<div class="clear"></div>
	</div>
</div>