<div class="cgcenter">
        <h1 class="tank_title_en">FAINTING in agony!! Explosion Plus</h1>
        <h1 class="tank_title_jp">MON絶!! エクスプロージョンプラス</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/explosionplus/cover.jpg"><img src="/assets/images/tank/explosionplus/cover.jpg" alt="FAINTING in agony!! Explosion Plus Cover"></a></div>
</div>


<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No.215 - 2005.11 ',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/explosionplus/ch/005.png',
            ),
            'title' => array(
                'en' => '#1 Monzetsu Fullopen',
                'jp' => '第1話 悶絶フルオープン',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.216 - 2005.12 ',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/explosionplus/ch/023.png',
            ),
            'title' => array(
                'en' => '#2 Raid Jet Debut',
                'jp' => '第2話 討ち入りジェットデビュー',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.217 - 2006.01',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/explosionplus/ch/041.png',
            ),
            'title' => array(
                'en' => '#3 Natural Born Eros',
                'jp' => '第3話 ナチュラルボーンエロス',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.218 - 2006.02',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-02.jpg',
                'chapter' => 'tank/explosionplus/ch/059.png',
            ),
            'title' => array(
                'en' => '#4 Easy Win Death Race',
                'jp' => '第4話 いただきデスレース',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.219 - 2006.03',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-03.jpg',
                'chapter' => 'tank/explosionplus/ch/077.png',
            ),
            'title' => array(
                'en' => '#5 Our Elegy',
                'jp' => '第5話僕たちの挽歌',
            ),
        ),
        array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/explosion_plus.jpg',
                'chapter' => 'tank/explosionplus/ch/095.png',
            ),
            'title' => array(
                'en' => 'Extra chapter: Raising Drunk Queen',
                'jp' => '番外編　ライジングドランククイーン',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.220 - 2006.04',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-04.jpg',
                'chapter' => 'tank/explosionplus/ch/101.jpg',
            ),
            'title' => array(
                'en' => '#6 Black Fuck \'N\' Roll',
                'jp' => '第6話 ブラックファックンロール',
            ),
        ),
		array(
            'release' => 'COMIC Jumbo No.221 - 2006.05 ',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-05.jpg',
                'chapter' => 'tank/explosionplus/ch/119.png',
            ),
            'title' => array(
                'en' => '#7 Farewell Japan',
                'jp' => '第7話サヨナラニッポン',
            ),
        ),	
        array(
            'release' => 'Hentai Virginity Loss Excellent',
            'image' => array(
                'manga' => '/anthology/excellent_abnormalvirginloss.jpg',
                'chapter' => 'tank/explosionplus/ch/137.png',
            ),
            'title' => array(
                'en' => 'Wake Up! Vise Calisthenics',
                'jp' => '目覚めよ！！万力体操',
            ),
        ),
		array(
            'release' => 'Ingyaku! Female Teacher Cream Pie',
            'image' => array(
                'manga' => '/anthology/nakadashionnakyoushi.jpg',
                'chapter' => 'tank/explosionplus/ch/145.png',
            ),
            'title' => array(
                'en' => 'Furious!! Bareback Curriculum',
                'jp' => '激烈！！生ハメカリキュラム',
            ),
        ),		
		array(
            'release' => 'COMIC Jumbo No.2014 - 2005.10',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/explosionplus/ch/153.png',
            ),
            'title' => array(
                'en' => 'Peerless: Goldrush',
                'jp' => '絶倫ゴールドラッシュ',
            ),
        ),
		array(
            'release' => '0EX Vol.30 - 2010.05.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex30.jpg',
                'chapter' => 'tank/explosionplus/ch/171.png',
            ),
            'title' => array(
                'en' => 'Toilet Spirit',
                'jp' => 'ウォシュレットテラー',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
<ul>
<li>The title of <b>"Chapter 5: Our Elegy" [僕たちの挽歌]</b> have a misprint on the title page, where the title is shown as [僕達の挽歌] where the たち part is in kanji 達. The official site also don't show the correct title, it's [僕たちの晩夏] where the last kanji 夏 got mistyped to 歌.</li>

<li>The title of the Extra Chapter <b>"Raising Drunk Queen"</b> got mistyped [イラジングドランククィーン] on the official site.</li>
</ul>
</span>

<!-- Image Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

<p>This book's colored title image is the title image of the chapter <b>"Tentacle God's Fist, Last Mystery: Out of Season Bloom Destroyer"</b> which was first printed in <a href="jkpworld">John K. Pe-ta's World</a>, and later printed in <a href="curriculumplus">Super Fainting in Agony Curriculum Plus</a> but this chapter was not included in this volume.</p>

<p>Altough the text of <b>"Mon Caffee Gaiden"</b> is missing from this volume, the illustration of it is here and in color for the first time.</p>

<p>The colored illustration for the chapter titled <b>"Furious!! Bareback Curriculum"</b> was only included in its first release <b>"Ingyaku! Female Teacher Cream Pie"</b> which you can find in the <a href="/anthology">Anthology</a> list. The colored illustration of <b>"Wake Up! Vise Calisthenics"</b> was unfortunately was left out from this release.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/explosionplus/title.jpg">
					<img src="/assets/images/tank/explosionplus/title.jpg" alt="Title page" title="Title page"></a><br>Title page
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/explosionplus/moncafe.jpg">
					<img src="/assets/images/tank/explosionplus/moncafe.jpg" alt="Mon Caffe Gaiden" title="Mon Caffe Gaiden"></a><br>Mon Caffe Gaiden</div>
			</div>
			<div class="clear"></div>
	</div>