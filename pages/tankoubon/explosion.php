<div class="cgcenter">
        <h1 class="tank_title_en">FAINTING in agony!! Explosion</h1>
        <h1 class="tank_title_jp">MON絶!! エクスプロージョン</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/explosion/cover.jpg"><img src="/assets/images/tank/explosion/cover.jpg" alt="FAINTING in agony!! Explosion Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No.215 - 2005.11 ',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/explosion/ch/005.png',
            ),
            'title' => array(
                'en' => '#1 Monzetsu Fullopen',
                'jp' => '第1話 悶絶フルオープン',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.216 - 2005.12 ',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/explosion/ch/023.png',
            ),
            'title' => array(
                'en' => '#2 Raid Jet Debut',
                'jp' => '第2話 討ち入りジェットデビュー',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.217 - 2006.01',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/explosion/ch/041.png',
            ),
            'title' => array(
                'en' => '#3 Natural Born Eros',
                'jp' => '第3話 ナチュラルボーンエロス',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.218 - 2006.02',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-02.jpg',
                'chapter' => 'tank/explosion/ch/059.png',
            ),
            'title' => array(
                'en' => '#4 Easy Win Death Race',
                'jp' => '第4話 いただきデスレース',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.219 - 2006.03',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-03.jpg',
                'chapter' => 'tank/explosion/ch/077.png',
            ),
            'title' => array(
                'en' => '#5 Our Elegy',
                'jp' => '第5話僕たちの挽歌',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.220 - 2006.04',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-04.jpg',
                'chapter' => 'tank/explosion/ch/095.png',
            ),
            'title' => array(
                'en' => '#6 Black Fuck \'N\' Roll',
                'jp' => '第6話 ブラックファックンロール',
            ),
        ),
		array(
            'release' => 'COMIC Jumbo No.221 - 2006.05 ',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-05.jpg',
                'chapter' => 'tank/explosion/ch/113.png',
            ),
            'title' => array(
                'en' => '#7 Farewell Japan',
                'jp' => '第7話サヨナラニッポン',
            ),
        ),	
        array(
            'release' => 'Hentai Virginity Loss Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_abnormalvirginloss.jpg',
                'chapter' => 'tank/explosion/ch/131.png',
            ),
            'title' => array(
                'en' => 'Wake Up! Vise Calisthenics',
                'jp' => '目覚めよ！！万力体操',
            ),
        ),
		array(
            'release' => 'COMIC Jumbo No.214 - 2005.10?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/explosion/ch/139.png',
            ),
            'title' => array(
                'en' => 'Peerless: Goldrush',
                'jp' => '絶倫ゴールドラッシュ',
            ),
        ),
		array(
            'release' => 'Ingyaku! Female Teacher Cream Pie',
            'image' => array(
                'manga' => 'anthology/nakadashionnakyoushi.jpg',
                'chapter' => 'tank/explosion/ch/157.png',
            ),
            'title' => array(
                'en' => 'Furious!! Bareback Curriculum',
                'jp' => '激烈！！生ハメカリキュラム',
            ),
        ),
		array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/explosion.jpg',
                'chapter' => 'tank/explosion/166.png',
            ),
            'title' => array(
                'en' => 'Mon Caffee Gaiden',
                'jp' => '悶カフェ外伝',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>
<!--
<p>Interesting to see that this book's coloured title image is the title image of the chapter called <strong>"Tentacle God's Fist, Last Mystery: Out of Season Bloom Destroyer"</strong> which was first printed in <a href="/jkpworld">John K. Pe-ta's World</a>, and later printed in <a href="/curriculumplus">Super Fainting in Agony Curriculum Plus</a>. This chapter was not included in this book! <strong>"Furious!! Bareback Curriculum"</strong> colour was only included in its first release in the book which title is <strong>"Ingyaku! Female Teacher Cream Pie"</strong> which you can find in the <a href="/jkptank">Anthology books</a> listing on this site. The second image is from <strong>"Wake Up! Vise Calisthenics"</strong> which was also featured on the cover for a similar volume called <strong>"Hentai Virginity Loss Excellent"</strong>. The brunette cover girl's origin are yet unknown. She wasn't featured in this book. </p>
-->