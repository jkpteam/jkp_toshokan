<div class="cgcenter">
        <h1 class="tank_title_en">Mesu ana Kanojo</h1>
        <h1 class="tank_title_jp">メス穴彼女</h1>
    <div class="tank_cover"><img src="/assets/images/tank/mesuana.jpg" alt="Mesu ana Kanojo" style="max-height: 250px"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Megastore Deep Vol. 09 - 2017.05.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd009.jpg',
                'chapter' => 'tank/mesuana/ch/003.jpg',
            ),
            'title' => array(
                'en' => 'Onee Trap',
                'jp' => 'お姉トラップ',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 13 - 2018.01.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd013.jpg',
                'chapter' => 'tank/mesuana/ch/023.jpg',
            ),
            'title' => array(
                'en' => 'Flesh Maman Life',
                'jp' => '肉欲ママンライフ',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 11 - 2017.09.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd011.jpg',
                'chapter' => 'tank/mesuana/ch/043.jpg',
            ),
            'title' => array(
                'en' => 'Summer Tentacle',
                'jp' => 'サマーテンタクル',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 17 - 2018.09.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd017.jpg',
                'chapter' => 'tank/mesuana/ch/063.jpg',
            ),
            'title' => array(
                'en' => 'Amazing ! Every holes girl',
                'jp' => '怪奇！全穴女',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 15 - 2018.05.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd015.jpg',
                'chapter' => 'tank/mesuana/ch/083.jpg',
            ),
            'title' => array(
                'en' => 'She is a black hunter',
                'jp' => '彼女は黒いハンター',
            ),
        ),
    );
?>


<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
<p>Summer Tentacle and Amazing! Every Holes Girl were republished in the next tankoubon Gucchiri Ana Meito [ぐっちょり穴メイト] on July 2020.</p>
</span>