<div class="cgcenter">
        <h1 class="tank_title_en">Super Fainting in Agony Curriculum Plus</h1>
        <h1 class="tank_title_jp">超悶絶カリキュラムプラス</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/curriculumplus/cover.jpg"><img src="/assets/images/tank/curriculumplus/cover.jpg" alt="Super Fainting in Agony Curriculum Plus Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No.198 - 2004.06',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-06.jpg',
                'chapter' => 'tank/curriculumplus/ch/004.jpg',
            ),
            'title' => array(
                'en' => 'Pink Revenge Festival',
                'jp' => '桃色リベンジ祭り',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.195 - 2004.03',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-03.jpg',
                'chapter' => 'tank/curriculumplus/ch/021.png',
            ),
            'title' => array(
                'en' => 'Forbidden Juice Mix',
                'jp' => '禁断のミックスジュース',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.190 - 2003.10',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-10.jpg',
                'chapter' => 'tank/curriculumplus/ch/039.png',
            ),
            'title' => array(
                'en' => 'Sister\'s Pink Bath Hell',
                'jp' => '妹地獄ピンク風呂',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.197 - 2004.05',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-05.jpg',
                'chapter' => 'tank/curriculumplus/ch/057.png',
            ),
            'title' => array(
                'en' => 'Hard After - School Doll',
                'jp' => '放課後コケシ固め',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.193 - 2004.01',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-01.jpg',
                'chapter' => 'tank/curriculumplus/ch/075.png',
            ),
            'title' => array(
                'en' => 'Direct hit!! Fainting in Agony by the Hellish Sword',
                'jp' => '直撃！！悶絶地獄剣',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.192 - 2003.12',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-12.jpg',
                'chapter' => 'tank/curriculumplus/ch/093.png',
            ),
            'title' => array(
                'en' => 'Frenzy! Dead Drunk March',
                'jp' => '狂乱！泥酔行進曲',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.194 - 2004.02',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-02.jpg',
                'chapter' => 'tank/curriculumplus/ch/111.png',
            ),
            'title' => array(
                'en' => 'The Rosicrucian Massage!!',
                'jp' => '薔薇十字に揉みしだけ！！',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.196 - 2004.04',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-04.jpg',
                'chapter' => 'tank/curriculumplus/ch/129.png',
            ),
            'title' => array(
                'en' => 'Cherry Blossoms Scatter',
                'jp' => 'サクラチルチル',
            ),
        ),
		array(
            'release' => 'COMIC Jumbo No.191 - 2003.11',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-11.gif',
                'chapter' => 'tank/curriculumplus/ch/147.png',
            ),
            'title' => array(
                'en' => 'Restraint, Anal Tyranny, Paparazzi',
                'jp' => '拘束.肛虐.パパラッチ',
            ),
        ),
	    array(
            'release' => 'John K. Pe-Ta\'s World - 2005.12.25',
            'image' => array(
                'manga' => 'tank/jkpworld.jpg',
                'chapter' => 'tank/curriculumplus/ch/165.png',
            ),
            'title' => array(
                'en' => 'Tentacle God\'s Fist, Last Mystery: Out of Season Bloom Destroyer',
                'jp' => '触手神拳最終奥義 狂い咲きデストロイ',
            ),
        ),
		array(
            'release' => 'Omake for this book',
            'image' => array(
                'manga' => 'tank/curriculum_plus.jpg',
                'chapter' => 'tank/curriculumplus/ch/182.png',
            ),
            'title' => array(
                'en' => 'Early Bloom Destroyer!! House of 1000 Corpses Counterattack',
                'jp' => '狂い咲きデストロイ!!逆襲のマーダーライド',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Image Gallery-->
<h2 class="subtitle">Colored Images Gallery</h2>

<p><b>"Pink Revenge Festival"</b> have 4 colored pages as an introduction. The colored pages of <b>"Tentacle God's Fist, Last Mystery: Out of Season Bloom Destroyer"</b> was printed in greyscale in this volume.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/curriculumplus/ch/003.jpg">
					<img src="/assets/images/tank/curriculumplus/ch/003.jpg" alt="Pink Revenge Festival page 1" title="Pink Revenge Festival page 1"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/curriculumplus/ch/004.jpg">
					<img src="/assets/images/tank/curriculumplus/ch/004.jpg" alt="Pink Revenge Festival page 2" title="Pink Revenge Festival page 2"></a></div>
						
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/curriculumplus/ch/005.jpg">
					<img src="/assets/images/tank/curriculumplus/ch/005.jpg" alt="Pink Revenge Festival page 3" title="Pink Revenge Festival page 3"></a></div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/curriculumplus/ch/006.jpg">
					<img src="/assets/images/tank/curriculumplus/ch/006.jpg" alt="Pink Revenge Festival page 4" title="Pink Revenge Festival page 4"></a></div>
			</div>
			<div class="clear"></div>
	</div>