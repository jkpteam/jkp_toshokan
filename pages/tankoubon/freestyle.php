<div class="cgcenter">
        <h1 class="tank_title_en">Fainting in Agony Freestyle</h1>
        <h1 class="tank_title_jp">悶絶フリースタイル</h1>
    <div class="tank_cover"><img src="/assets/images/tank/freestyle/covera.jpg" alt="Fainting in Agony Freestyle Cover" style="max-height: 250px">
                            <img src="/assets/images/tank/freestyle/coverb.jpg" alt="Fainting in Agony Freestyle Cover" style="max-height: 250px"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Kuriberon Vol.52 - 2017.02.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb052.jpg',
                'chapter' => 'mangalist/kuriberon/ch/kb052.jpg',
            ),
            'title' => array(
                'en' => 'Empress Caligula',
                'jp' => '女帝カリギュラ',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.56 - 2017.06.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb056.jpg',
                'chapter' => 'mangalist/kuriberon/ch/kb056.jpg',
            ),
            'title' => array(
                'en' => 'My Contortion Girl',
                'jp' => 'コントーション彼女',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.58 - 2017.08.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb058.jpg',
                'chapter' => 'mangalist/kuriberon/ch/kb058.jpg',
            ),
            'title' => array(
                'en' => 'Do it! Monzetsu Buster',
                'jp' => '決めるぜ！　悶絶バスター',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.60 - 2017.10.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb060.jpg',
                'chapter' => 'mangalist/kuriberon/ch/kb060.jpg',
            ),
            'title' => array(
                'en' => 'Oppression Full Purge',
                'jp' => '抑圧フルパージ！',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.68 - 2018.06.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb068.jpg',
                'chapter' => 'mangalist/kuriberon/ch/kb068.jpg',
            ),
            'title' => array(
                'en' => 'Her Extraordinary Lust',
                'jp' => '彼女の異常な欲情',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.62 - 2017.12.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb062.jpg',
                'chapter' => 'mangalist/kuriberon/ch/kb062.jpg',
            ),
            'title' => array(
                'en' => 'Cartoonist Momobukuro\'s Miracle Milk Bag',
                'jp' => '桃袋先生のミラクル乳袋',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.66 - 2018.04.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb066.jpg',
                'chapter' => 'mangalist/kuriberon/ch/kb066.jpg',
            ),
            'title' => array(
                'en' => 'Sequel for Monzetsu Idle Rolad',
                'jp' => '続・悶絶アイドル道',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.70 - 2018.08.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb070.jpg',
                'chapter' => 'mangalist/kuriberon/ch/kb070.jpg',
            ),
            'title' => array(
                'en' => 'Mutilate Fuck at the After School',
                'jp' => '放課後バラバラ事件',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Author Comment -->
<h2 class="subtitle">Afterword</h2>
<div class="atogaki">
    <a class="nagyobb" href="/assets/images/tank/freestyle/atogaki.jpg">
        <img src="/assets/images/tank/freestyle/atogaki.jpg" alt="atogaki" title="atogaki">
    </a>
    <p>Translation needed!!</p>
</div>