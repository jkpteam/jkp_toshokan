
<h1 class="tank_title_en">My lovely Acmate</h1>
        <h1 class="tank_title_jp">愛しきアクメイト</h1>
<div class="cgcenter">        
    <div class="tank_cover"><img src="/assets/images/tank/acmate.jpg" alt="My lovely Acmate Cover" style="max-height: 250px"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Megastore Deep Vol. 07 - 2017.01.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd007.jpg',
                'chapter' => 'tank/acmate/ch/004.png',
            ),
            'title' => array(
                'en' => 'Kemonist Family',
                'jp' => 'ケモニストファミリー',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 05 - 2016.10.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd005.jpg',
                'chapter' => 'tank/acmate/ch/024.png',
            ),
            'title' => array(
                'en' => 'Ushimitsu Bomber',
                'jp' => '丑三つボンバー',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 03 - 2016.06.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd003.jpg',
                'chapter' => 'tank/acmate/ch/044.png',
            ),
            'title' => array(
                'en' => 'Captivating Piercing Princess',
                'jp' => '魅惑の貫き姫',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 02 - 2016.03.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd002.jpg',
                'chapter' => 'tank/acmate/ch/064.png',
            ),
            'title' => array(
                'en' => 'Welcome to Hormone Tea House',
                'jp' => 'ホルモン喫茶へようこそ',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 01 - 2015.12.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd001.jpg',
                'chapter' => 'tank/acmate/ch/084.png',
            ),
            'title' => array(
                'en' => 'Shota Hunter Hikaru',
                'jp' => 'ショタハンター☆ヒカル',
            ),
        ),
        array(
            'release' => 'Megastore Alpha 2016.01 - 2015.12.14',
            'image' => array(
                'manga' => 'mangalist/megastorea/msa2016_01.jpg',
                'chapter' => 'tank/acmate/ch/104.png',
            ),
            'title' => array(
                'en' => 'Mr. Yoshida and the senior female pig',
                'jp' => '吉田君とメス豚先輩',
            ),
        ),
        array(
            'release' => 'Megastore H Vol. 112 - 2013.07',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh112.jpg',
                'chapter' => 'tank/acmate/ch/124.png',
            ),
            'title' => array(
                'en' => 'Desire Matter X',
                'jp' => '欲情の物体X',
            ),
        ),
        array(
            'release' => 'Megastore H Vol. 111 - 2013.05',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh111.jpg',
                'chapter' => 'tank/acmate/ch/144.png',
            ),
            'title' => array(
                'en' => 'Invincible Combination Senpain',
                'jp' => '無敵合体センパイン',
            ),
        ),
        array(
            'release' => 'Megastore H Vol. 110 - 2013.03',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh110.jpg',
                'chapter' => 'tank/acmate/ch/164.png',
            ),
            'title' => array(
                'en' => 'Love Tackle',
                'jp' => '恋のタックル',
            ),
        ),
        array(
            'release' => 'Megastore H Vol. 109 - 2013.01',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh109.jpg',
                'chapter' => 'tank/acmate/ch/184.png',
            ),
            'title' => array(
                'en' => 'Shinano LOUD Ranch',
                'jp' => '特濃ＬＯＵＤ牧場',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
    <p>These stories were released in <a href="/tankoubon/daigaku">Private Slug Holes Academy</a> as well.</p>
        <ul>
            <li>Desire Matter X - 欲情の物体X</li>
            <li>Invincible Combination Senpain - 無敵合体センパイン</li>
            <li>Love Tackle - 恋のタックル</li>
            <li>Shinano LOUD Ranch - 特濃ＬＯＵＤ牧場</li>
        </ul>
</span>

<h2 class="subtitle">Melonbooks purchase special drawing 4P leaflet</h2>

<p>Twitter post: https://twitter.com/jkp55645/status/844779818080124928/photo/1 ... https://twitter.com/jkp55645/status/869045512321748992/photo/1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus iaculis velit ut quam volutpat pulvinar. Aliquam volutpat luctus scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim elit, bibendum eu consectetur sed, viverra quis enim. Aliquam cursus, ante quis venenatis vestibulum, leo leo lacinia erat, quis iaculis mauris ante non tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus a dolor cursus, sagittis risus vel, bibendum justo. Donec massa leo, aliquam in scelerisque ut, consectetur ut velit. Aliquam quis vestibulum sapien, eget bibendum metus.</p>
	<div class="cgwrapper"> 
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/omake/to-leaflet01.jpg">
					<img src="/assets/images/tank/acmate/omake/to-leaflet01.jpg" alt="001" title="001"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/omake/to-leaflet02.jpg">
					<img src="/assets/images/tank/acmate/omake/to-leaflet02.jpg" alt="002" title="002"></a>
				</div>

				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/omake/to-leaflet03.jpg">
					<img src="/assets/images/tank/acmate/omake/to-leaflet03.jpg" alt="003" title="003"></a>
				</div>

				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/omake/to-leaflet04.jpg">
					<img src="/assets/images/tank/acmate/omake/to-leaflet04.jpg" alt="004" title="004"></a>
				</div>
            </div>
        
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/omake/mb-leaflet01.png">
					<img src="/assets/images/tank/acmate/omake/mb-leaflet01.png" alt="001" title="001"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/omake/mb-leaflet02.png">
					<img src="/assets/images/tank/acmate/omake/mb-leaflet02.png" alt="002" title="002"></a>
				</div>

				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/omake/mb-leaflet03.png">
					<img src="/assets/images/tank/acmate/omake/mb-leaflet03.png" alt="003" title="003"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/omake/mb-leaflet04.jpg">
					<img src="/assets/images/tank/acmate/omake/mb-leaflet04.jpg" alt="004" title="004"></a>
				</div>
            </div>
<div class="clear"></div>
</div>

<h2 class="subtitle">Autograph</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus iaculis velit ut quam volutpat pulvinar. Aliquam volutpat luctus scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim elit, bibendum eu consectetur sed, viverra quis enim. Aliquam cursus, ante quis venenatis vestibulum, leo leo lacinia erat, quis iaculis mauris ante non tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus a dolor cursus, sagittis risus vel, bibendum justo. Donec massa leo, aliquam in scelerisque ut, consectetur ut velit. Aliquam quis vestibulum sapien, eget bibendum metus.
sign 1: https://twitter.com/jkp55645/status/869045512321748992
sign 2: https://twitter.com/jkp55645/status/1230076437966639104
sign 3: https://twitter.com/jkp55645/status/854529446442786817
</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/sign/sign1.jpg">
					<img src="/assets/images/tank/acmate/sign/sign1.jpg" alt="sign" title="sign"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/sign/sign1_melonbooks.jpg">
					<img src="/assets/images/tank/acmate/sign/sign1_melonbooks.jpg" alt="sign" title="sign"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/sign/sign1_toranoana.jpg">
					<img src="/assets/images/tank/acmate/sign/sign1_toranoana.jpg" alt="sign" title="sign"></a>
				</div>	
	       </div>
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/sign/sign2.jpg">
					<img src="/assets/images/tank/acmate/sign/sign2.jpg" alt="sign" title="sign"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/sign/sign3.jpg">
					<img src="/assets/images/tank/acmate/sign/sign3.jpg" alt="sign" title="sign"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/sign/sign4.jpg">
					<img src="/assets/images/tank/acmate/sign/sign4.jpg" alt="sign" title="sign"></a>
                </div>	
			<div class="clear"></div>
</div>

<h2 class="subtitle">Guest works</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus iaculis velit ut quam volutpat pulvinar. Aliquam volutpat luctus scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim elit, bibendum eu consectetur sed, viverra quis enim. Aliquam cursus, ante quis venenatis vestibulum, leo leo lacinia erat, quis iaculis mauris ante non tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus a dolor cursus, sagittis risus vel, bibendum justo. Donec massa leo, aliquam in scelerisque ut, consectetur ut velit. Aliquam quis vestibulum sapien, eget bibendum metus.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/205.jpg">
					<img src="/assets/images/tank/acmate/205.jpg" alt="guest" title="guest"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/206.jpg">
					<img src="/assets/images/tank/acmate/206.jpg" alt="guest" title="guest"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/acmate/208.jpg">
					<img src="/assets/images/tank/acmate/208.jpg" alt="guest" title="guest"></a>
				</div>
			<div class="clear"></div>
	</div>
</div>


<!-- Author Comment -->
<h2 class="subtitle">Afterword</h2>
<div class="atogaki">
    <a class="nagyobb" href="/assets/images/tank/acmate/atogaki.jpg">
        <img src="/assets/images/tank/acmate/atogaki.jpg" alt="atogaki" title="atogaki">
    </a>
    <p>Translation needed!!</p>
</div>