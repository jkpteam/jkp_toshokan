<div class="cgcenter">
        <h1 class="tank_title_en">Search and Fainting in Agony Destroy</h1>
        <h1 class="tank_title_jp">サーチ&悶絶デストロイ</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/destroy/cover.jpg"><img src="/assets/images/tank/destroy/cover.jpg" alt="Search and Fainting in Agony Destroy Cover"></a></div>
</div>


<?php
    $contents = array(
        array(
            'release' => 'MegaMilk Vol.08 - 2011.01.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm08b.jpg',
                'chapter' => 'tank/destroy/ch/005.png',
            ),
            'title' => array(
                'en' => 'Exterminator from today!',
                'jp' => '今日からエクスターミネート',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.09 - 2011.02.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm09b.jpg',
                'chapter' => 'tank/destroy/ch/029.png',
            ),
            'title' => array(
                'en' => 'Midday\'s Enigmatic Hero',
                'jp' => '真昼のエニグマヒーロー',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.01 - 2010.06.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm01b.jpg',
                'chapter' => 'tank/destroy/ch/053.png',
            ),
            'title' => array(
                'en' => 'Attack House 303',
                'jp' => 'アタックメゾン３０３',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.02 - 2010.07.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm02b.jpg',
                'chapter' => 'tank/destroy/ch/077.png',
            ),
            'title' => array(
                'en' => 'Pink Fire Potato',
                'jp' => '桃色ファイアーポテト',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.04 - 2010.09.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm04b.jpg',
                'chapter' => 'tank/destroy/ch/101.png',
            ),
            'title' => array(
                'en' => 'Crash on the Beach',
                'jp' => 'クラッシュオンザビーチ',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.03 - 2010.08.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm03b.jpg',
                'chapter' => 'tank/destroy/ch/125.png',
            ),
            'title' => array(
                'en' => 'Red Hot Beach',
                'jp' => 'RED HOT BEACH',
            ),
        ),
        array(
            'release' => '0EX Vol.29 - 2010.04.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex29.jpg',
                'chapter' => 'tank/destroy/ch/149.png',
            ),
            'title' => array(
                'en' => 'Shutter Fucker Sister',
                'jp' => 'シャッターファッカーシスター',
            ),
        ),
        array(
            'release' => '0EX Vol.28 - 2010.03.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex28.jpg',
                'chapter' => 'tank/destroy/ch/173.png',
            ),
            'title' => array(
                'en' => 'Female Pig Princess Cash Cow',
                'jp' => 'ドル箱メス豚プリンセス',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
<p>On the official MegaMilk blog the title of the chapter <b>"Pink Fire Potato" [桃色ファイアーポテト]</b> got <a href="http://megamilk.coremagazine.co.jp/archives/65381158.html">mistyped</a>, they wrote ファイヤー instead of ファイアー.</p>
</span>
	
<!-- Image Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

<p>The title page shows the girl from <b>"Attack House 303"</b> in color as she spreads her ass. The second page is just an inverse of three girls from these chapters.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/destroy/003.jpg">
					<img src="/assets/images/tank/destroy/003.jpg" alt="Title page 1" title="Title page 1"></a><br>Title page 1
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/destroy/004.jpg">
					<img src="/assets/images/tank/destroy/004.jpg" alt="Title page 2" title="Title page 2"></a><br>Title page 2</div>
			</div>
			<div class="clear"></div>
	</div>


<!-- Author Comment -->
<h2 class="subtitle">Afterword</h2>
<div class="atogaki">
    <a class="nagyobb" href="/assets/images/tank/destroy/atogaki.jpg">
        <img src="/assets/images/tank/destroy/atogaki.jpg" alt="atogaki" title="atogaki">
    </a>
    <p>Translation needed!!</p>
</div>