<h1 class='titleen'>Fainting in Agony Caligula Machine</h1>
<h2 class='titlejp'>悶絶カリギュラマシーン</h2>

<div class="cgcenter">
    <div class="tank_cover">
        <a class="nagyobb" href="/assets/images/tank/caligula/cover.jpg">
            <img src="/assets/images/tank/caligula/cover.jpg" alt="Fainting in Agony Caligula Machine Cover">
        </a>
    </div>
</div>


<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No.188 - 2003.08',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-08.jpg',
                'chapter' => 'tank/caligula/ch/005.png',
            ),
            'title' => array(
                'en' => 'Straight Pitch! Bewithchment Operation',
                'jp' => '直球！悩殺オペレーション',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo 2003 ?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/caligula/ch/023.png',
            ),
            'title' => array(
                'en' => 'Steam - Bewitchment Fever',
                'jp' => '湯けむり悩殺フィーバー',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.185 - 2003.05',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-05.jpg',
                'chapter' => 'tank/caligula/ch/039.png',
            ),
            'title' => array(
                'en' => 'Spartacus Games - Directive 076',
                'jp' => 'スパルタ指令076',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.180 - 2002.12',
            'image' => array(
                'manga' => 'mangalist/jumbo/2002-12.jpg',
                'chapter' => 'tank/caligula/ch/057.png',
            ),
            'title' => array(
                'en' => 'My Neighbor\'s Meat',
                'jp' => 'となりの肉',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.189 - 2003.09',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-09.jpg',
                'chapter' => 'tank/caligula/ch/073.png',
            ),
            'title' => array(
                'en' => 'Three Part Milk Explosion Summit Alpha',
                'jp' => '第3次爆乳サミットα',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.184 - 2003.04',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-04.jpg',
                'chapter' => 'tank/caligula/ch/091.png',
            ),
            'title' => array(
                'en' => 'Open Ass Model - Fainting Club Captain',
                'jp' => '肛開モデル悶絶部長',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo 2003 ?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/caligula/ch/107.png',
            ),
            'title' => array(
                'en' => 'Reverse Thrust! Bespectacled Cheater',
                'jp' => '逆噴射インチキメガネ',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.183 - 2003.03',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-03.jpg',
                'chapter' => 'tank/caligula/ch/125.png',
            ),
            'title' => array(
                'en' => 'Breast Karate, Milk Fist',
                'jp' => 'オッパイ空手ミルク拳',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.187 - 2003.07',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-07.jpg',
                'chapter' => 'tank/caligula/ch/141.png',
            ),
            'title' => array(
                'en' => 'Hot Blooded! Fainting in Agony! The Hard Working Captain',
                'jp' => '熱血！悶絶！しごき部長',
            ),
        ),
        array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/caligula.jpg',
                'chapter' => 'tank/caligula/ch/159.png',
            ),
            'title' => array(
                'en' => 'Bonus Comic: Miracle Justice Girl J-Ko',
                'jp' => 'おまけマンガ ミラクル女子鋼製J子',
            ),
        ),
        array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/caligula.jpg',
                'chapter' => 'mangalist/omake/caligula_omake.jpg',
            ),
            'title' => array(
                'en' => 'Cover foldout 4 panel',
                'jp' => 'カバー折り返し4コマ',
            ),
        ),			
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
    <p>These stories were released in different <a href="/anthology">Anthology</a> Tankoubons.</p>
        <ul>
            <li>Straight Pitch! Bewithchment Operation: Creampie Devil Rape CLIMAX</li>
            <li>Three Part Milk Explosion Summit Alpha: Tortured Sex Object Excellent</li>
            <li>Reverse Thrust! Bespectacled Cheater: Girls with Glasses Excellent + JKP's World</li>
            <li>Hot Blooded! Fainting in Agony! The Hard Working Captain: Gang Rape and Promiscuity Excellent + JKP's World</li>
        </ul>
</span>    
    
<h2 class="subtitle">Unique Images Gallery</h2>

	<div class="cgwrapper">
		<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/caligula/vtitle.jpg"><img src="/assets/images/tank/caligula/vtitle.jpg" alt="title" title="Title Image (Three Part Milk Explosion Summit Alpha)"></a><br>Volume Title</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/caligula/innerc.png"><img src="/assets/images/tank/caligula/innerc.png" alt="innerc" title="Inner Cover"></a><br>Inner Cover</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/caligula/contents.png"><img src="/assets/images/tank/caligula/contents.png" alt="contents" title="contents"></a><br>Contents</div>
	</div>
        <div class="clear"></div>
</div>

<!--
<h2 class="subtitle">Foldout Special Bonus Comic>


<table class="manga">
            <tr>
                <td><img src="/assets/images/karigyura/4koma_1-1.jpg" alt="Bonus Comic 1-1" /></td>
                <td><img src="/assets/images/karigyura/4koma_1-2.jpg" alt="Bonus Comic 1-2" /></td>
                <td><img src="/assets/images/karigyura/4koma_1-3.jpg" alt="Bonus Comic 1-3" /></td>
                <td><img src="/assets/images/karigyura/4koma_1-4.jpg" alt="Bonus Comic 1-4" /></td>
            </tr>

            <tr>
                <td align="center"><strong>Guy:</strong> !</td>
                <td><strong>Guy:</strong> Miss, you dropped it!<br><strong>Girl:</strong> What?</td>
                <td><strong>Guy:</strong> Your bra!</td>
                <td><strong>Girl:</strong> What careless am I?<br><strong>Narrator:</strong> No way!</td>
            </tr>
</table>

<table class="manga">
            <tr>
                <td><img src="/assets/images/karigyura/4koma_2-1.jpg" alt="Bonus Comic 2-1" /></td>
                <td><img src="/assets/images/karigyura/4koma_2-2.jpg" alt="Bonus Comic 2-2" /></td>
                <td><img src="/assets/images/karigyura/4koma_2-3.jpg" alt="Bonus Comic 2-3" /></td>
            </tr>

            <tr>
                <td><strong>Old Man:</strong> Which vibrator did you dropped?<br>The Gold one? The Silver one?</td>
                <td><strong>Girl:</strong> I didn't drop any...<br>Hey, who are you?</td>
                <td><strong>Old Man:</strong> Hm, so honest, you deserve the bare penis!</td>
            </tr>
</table>

<h2 class="subtitle">Back Cover Comic>


<table class="manga">
            <tr>
                <td><img src="/assets/images/karigyura/back-2.jpg" alt="Back Comic 1" /></td>
                <td><img src="/assets/images/karigyura/back-1.jpg" alt="Back Comic 2" /></td>
            </tr>

            <tr>
                <td>Woah... the inside is...<br>such a...</td>
                <td>You spreading it too much...<br>Don't look that deep inside...</td>
            </tr>
</table>

-->
