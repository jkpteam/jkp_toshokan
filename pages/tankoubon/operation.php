<div class="cgcenter">
        <h1 class="tank_title_en">Intense!! Fainting in Agony Operation</h1>
        <h1 class="tank_title_jp">悶絶オペレーション</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/operation/cover.jpg"><img src="/assets/images/tank/operation/cover.jpg" alt="Intense!! Fainting in Agony Operation Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No. 202 - 2004.10',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-10.jpg',
                'chapter' => 'tank/operation/ch/005.png',
            ),
            'title' => array(
                'en' => 'Submissive Girl Lesson, Disorderly Punishment',
                'jp' => 'ガチん娘レッスン乱れ責め',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo 2004?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/operation/ch/023.png',
            ),
            'title' => array(
                'en' => 'Sudden Attack! Letter V of Disgrace',
                'jp' => 'ぬきうち！屈辱Vの字斬り',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No. 204 - 2004.12',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-12.jpg',
                'chapter' => 'tank/operation/ch/041.png',
            ),
            'title' => array(
                'en' => 'Hell Spa, Hot Bath of Shame',
                'jp' => '地獄温泉羞恥の湯',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No. 203 - 2004.11',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-11.jpg',
                'chapter' => 'tank/operation/ch/059.png',
            ),
            'title' => array(
                'en' => 'Peach Breasts!! Desire Spirits',
                'jp' => '桃乳！！欲情スピリッツ',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.213 - 2005.09',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/operation/ch/077.png',
            ),
            'title' => array(
                'en' => 'Humiliating Punishment MAX!!',
                'jp' => '惨敗おしおきMAX！！',
            ),
        ),
        array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/operation.jpg',
                'chapter' => 'tank/operation/ch/095.png',
            ),
            'title' => array(
                'en' => 'Tentacle Club Leader Yoshio',
                'jp' => '触手委員長ヨシオ',
            ),
        ),
        array(
            'release' => 'Women Manga-ka Excellent',
            'image' => array(
                'manga' => '/anthology/excellent_womanmangaka.jpg',
                'chapter' => 'tank/operation/ch/097.png',
            ),
            'title' => array(
                'en' => 'Goodbye, Climax Sensei',
                'jp' => 'さよなら絶頂先生',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo 2004?',
            'image' => array(
                'manga' => '/anthology/bestof_reverserapefemale.jpg',
                'chapter' => 'tank/operation/ch/105.png',
            ),
            'title' => array(
                'en' => 'Peach Ass Challenge of Disgrace',
                'jp' => '屈辱の桃尻チャレンジ',
            ),
        ),
		array(
            'release' => 'Comic Jumbo No. 200 - 2004.08',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-08.jpg',
                'chapter' => 'tank/operation/ch/123.png',
            ),
            'title' => array(
                'en' => 'Slave Tied Milky Mistress',
                'jp' => 'ミルク極道下克上縛り',
            ),
        ),
		array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/operation.jpg',
                'chapter' => 'tank/operation/ch/141.png',
            ),
            'title' => array(
                'en' => 'Tentacle Club Leader Yoshio 2',
                'jp' => '触手委員長ヨシオ2',
            ),
        ),	
		array(
            'release' => 'Huge Breast Rape Excellent',
            'image' => array(
                'manga' => '/anthology/excellent_hugebreastrape.jpg',
                'chapter' => 'tank/operation/ch/143.png',
            ),
            'title' => array(
                'en' => 'Afterschool Blowback',
                'jp' => '放課後ブローバック',
            ),
        ),
		array(
            'release' => 'COMIC Jumbo No. 214 - 2005.10',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/operation/ch/151.png',
            ),
            'title' => array(
                'en' => 'Double Doors Exorcist Raper',
                'jp' => '凌辱陰陽観音開き',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Images Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

<p>The colored title image was first printed on the cover of an anthology book titled <strong>Women Manga-ka Excellent</strong> which you can find on the <a href="/anthology">Anthology</a> list. The colored image of <strong>"Afterschool Blowback"</strong> was also printed on the cover for a similar anthology book titled <strong>Huge Breast Rape Excellent</strong>. The cover illustration shows the girl from <strong>"Submissive Girl Lesson, Disorderly Punishment"</strong>.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/operation/contents.png">
					<img src="/assets/images/tank/operation/contents.png" alt="Contents" title="Contents"></a><br>Contents
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/operation/title.jpg">
					<img src="/assets/images/tank/operation/title.jpg" alt="title" title="Title Image (Goodbye, Climax Sensei Color)"></a><br>Title</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/operation/excellent_hugebreastrape_color.jpg">
					<img src="/assets/images/tank/operation/excellent_hugebreastrape_color.jpg" alt="color" title="Afterschool Blowback color"></a><br>Foldout #1</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/operation/cover-illustration.jpg">
					<img src="/assets/images/tank/operation/cover-illustration.jpg" alt="cover illustration" title="Cover illustration"></a><br>Foldout #2</div>
				
			</div>
			<div class="clear"></div>
	</div>