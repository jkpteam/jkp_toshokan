 <div class="cgcenter">
        <h1 class="tank_title_en">Exciting Fainting in Agony Balkan!!</h1>
        <h1 class="tank_title_jp">トキメキ悶絶バルカン!!</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/balkan/cover.jpg"><img src="/assets/images/tank/balkan/cover.jpg" alt="Exciting Fainting in Agony Balkan!! Cover"></a></div>
</div>


<?php
    $contents = array(
        array(
            'release' => 'Megaplus Vol.48 - 2007.09.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp48b.jpg',
                'chapter' => 'tank/balkan/ch/005.png',
            ),
            'title' => array(
                'en' => 'Under Class HERO',
                'jp' => 'アンダークラスヒーロー',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.49 - 2007.10.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp49b.jpg',
                'chapter' => 'tank/balkan/ch/025.png',
            ),
            'title' => array(
                'en' => 'The High-tension Crazy driving!!',
                'jp' => '暴走ハイテンション!!',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.50 - 2007.11.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp50b.jpg',
                'chapter' => 'tank/balkan/ch/045.png',
            ),
            'title' => array(
                'en' => 'Sudden Fire!!',
                'jp' => 'ユウダチファイヤー',
            ),
        ),
        array(
            'release' => '0EX Vol.01 - 2007.12.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex01.jpg',
                'chapter' => 'tank/balkan/ch/065.png',
            ),
            'title' => array(
                'en' => 'Danger Saint 24',
                'jp' => 'デンジャーセイント24',
            ),
        ),
        array(
            'release' => '0EX Vol.02 - 2008.01.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex02.jpg',
                'chapter' => 'tank/balkan/ch/085.png',
            ),
            'title' => array(
                'en' => 'Blossoming Disorder Train!',
                'jp' => '咲き乱れトレイン',
            ),
        ),
        array(
            'release' => '0EX Vol.04 - 2008.03.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex04.jpg',
                'chapter' => 'tank/balkan/ch/115.png',
            ),
            'title' => array(
                'en' => 'Blue Sky Secret',
                'jp' => '青空シークレット',
            ),
        ),
        array(
            'release' => '0EX Vol.05 - 2008.04.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex05.jpg',
                'chapter' => 'tank/balkan/ch/126.png',
            ),
            'title' => array(
                'en' => 'Endless Waltz in G',
                'jp' => 'エンドレスなGでワルツ',
            ),
        ),
        array(
            'release' => '0EX Vol.06 - 2008.05.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex06.jpg',
                'chapter' => 'tank/balkan/ch/145.png',
            ),
            'title' => array(
                'en' => 'Hop Skip Crazy!',
                'jp' => 'ホップ・ステップ・狂気',
            ),
        ),
        array(
            'release' => '0EX Vol.07 - 2008.06.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex07.jpg',
                'chapter' => 'tank/balkan/ch/165.png',
            ),
            'title' => array(
                'en' => 'Excite! Second girl!',
                'jp' => 'エキサイト第二彼女',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.222 - 2006.06',
            'image' => array(
                'manga' => 'mangalist/jumbo/2006-06.jpg',
                'chapter' => 'tank/balkan/ch/185.png',
            ),
            'title' => array(
                'en' => 'Shoot out! My Long lost Spark!',
                'jp' => '撃ち出せ！俺の未練スパーク!!',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
		<b>1. Danger Saint 24</b><br>
<p>In the original magazine printing the title was mistyped. An earlier story's name titled <b>"The High-tension Crazy driving!!"</b> was given to it. This was corrected in this release to the correct title.</p>
<div style="text-align:center;">
<img src="/assets/images/mangalist/ex/ch/dangersaint_1.png" alt="dangersaint" title="dangersaint">
<img src="/assets/images/arrow.png" alt="arrow" title="arrow">
<img src="/assets/images/mangalist/ex/ch/dangersaint_2.png" alt="dangersaint" title="dangersaint">
</div>
</span>

<!-- Image Gallery-->

<h2 class="subtitle">Unique Images Gallery</h2>

<p>The book kick off with a 2 page colored introduction in POV style showing the reader as he / she opens up a girls vagina. CG work is done by a friend of JKP and fellow hentai author <b>Machino Henmaru</b>. In Volume 49 of Comic Megaplus, <b>The High-tension Crazy driving!!</b> originally had a colored introduction page. Unfortunately this page is not present in this release.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kepszoveg"><a class="nagyobb" href="/assets/images/tank/balkan/003.jpg">
					<img src="/assets/images/tank/balkan/003.jpg" alt="Introduction page 1" title="Introduction page 1"></a><br>Introduction page 1
				</div>
				
				<div class="kepszoveg"><a class="nagyobb" href="/assets/images/tank/balkan/004.jpg">
					<img src="/assets/images/tank/balkan/004.jpg" alt="Introduction page 2" title="Introduction page 2"></a><br>Introduction page 2</div>
				
				<div class="kepszoveg"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp49_002.jpg">
					<img src="/assets/images/mangalist/megaplus/ch/mp49_002.jpg" alt="The High-tension Crazy driving!! color page" title="The High-tension Crazy driving!! color page"></a><br>The High-tension<br>Crazy driving!!
				</div>				
			</div>
			<div class="clear"></div>
	</div>