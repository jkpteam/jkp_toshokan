<div class="cgcenter">
        <h1 class="tank_title_en">Full Buxom!! Fainting in Agony Fever Plus</h1>
        <h1 class="tank_title_jp">ムチムチ！！悶絶フィーバープラス</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/feverplus/cover.jpg"><img src="/assets/images/tank/feverplus/cover.jpg" alt="Full Buxom!! Fainting in Agony Fever Plus Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No.208 - 2005.04',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/feverplus/ch/005.png',
            ),
            'title' => array(
                'en' => 'Sleeping Assault Guerrillas',
                'jp' => 'スリーピング突撃ゲリラ',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.209 - 2005.05',
            'image' => array(
                'manga' => 'mangalist/jumbo/2005-05.jpg',
                'chapter' => 'tank/feverplus/ch/023.png',
            ),
            'title' => array(
                'en' => 'Forbidden Planet W.C.',
                'jp' => '禁断惑星W・C',
            ),
        ),
        array(
            'release' => 'Married Woman Rape Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_hitozumaraipe.jpg',
                'chapter' => 'tank/feverplus/ch/041.png',
            ),
            'title' => array(
                'en' => 'Milk Riot - Panty Mask',
                'jp' => '乱乳パンツ仮面',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.209 - 2005.05',
            'image' => array(
                'manga' => 'mangalist/jumbo/2005-05.jpg',
                'chapter' => 'tank/feverplus/ch/049.png',
            ),
            'title' => array(
                'en' => 'Arousal Jetstream',
                'jp' => '挑発ジェットストリーム',
            ),
        ),
        array(
            'release' => 'After School FUCK Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_afterschoolfuck.jpg',
                'chapter' => 'tank/feverplus/ch/067.png',
            ),
            'title' => array(
                'en' => 'Downpour March',
                'jp' => 'どしゃぶり行進曲',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo 2005?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/fever/ch/075.png',
            ),
            'title' => array(
                'en' => 'In Other Words, The Electric Shock Virgin',
                'jp' => 'つまみ出し電撃バージン',
            ),
        ),		
        array(
            'release' => 'Full Buxom!! Fainting in Agony Fever',
            'image' => array(
                'manga' => 'tank/fever.jpg',
                'chapter' => 'tank/feverplus/ch/093.png',
            ),
            'title' => array(
                'en' => 'What if Series',
                'jp' => 'もしもシリーズ',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.207 - 2005.03',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/feverplus/ch/095.png',
            ),
            'title' => array(
                'en' => 'Mania! Pleasure Addict',
                'jp' => '熱中！快感アディクト',
            ),
        ),
        array(
            'release' => 'Nurse Rape Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_nursrape.jpg',
                'chapter' => 'tank/feverplus/ch/113.png',
            ),
            'title' => array(
                'en' => 'Night Shift 4:44',
                'jp' => '夜勤4:44',
            ),
        ),
        array(
            'release' => 'Comic Jumbo 2005?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/feverplus/ch/121.png',
            ),
            'title' => array(
                'en' => 'Counterattack: Shine Spark',
                'jp' => '逆襲シャインスパーク',
            ),
        ),
        array(
            'release' => 'Imouto Moe Excellent',
            'image' => array(
                'manga' => 'anthology/excellent_imoutomoe.jpg',
                'chapter' => 'tank/feverplus/ch/139.png',
            ),
            'title' => array(
                'en' => 'Explosion: Little Sister Simulation',
                'jp' => '爆裂妹シミュレーション',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.212 - 2005.08',
            'image' => array(
                'manga' => 'mangalist/jumbo/2005-08.jpg',
                'chapter' => 'tank/feverplus/ch/147.png',
            ),
            'title' => array(
                'en' => 'Let\'s Secret Coffee Shop',
                'jp' => 'Let\'s秘密喫茶',
            ),
        ),
        array(
            'release' => '0EX Vol.20 - 2009.07.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex20.jpg',
                'chapter' => 'tank/feverplus/ch/165.png',
            ),
            'title' => array(
                'en' => 'Burning JET Peach',
                'jp' => 'バーニングJETピーチ',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
<p><b>"Burning JET Peach" [バーニングJETピーチ]</b> have a 5 page Bonus addition for this release only.</p>
</span>

<!-- Images Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

<p>The colored illustration of <b>"Milk Riot - Panty Mask"</b>, <b>"Downpour March"</b> and <b>"Explosion: Little Sister Simulation"</b> were not inlcuded in this re-release. The title image is the exact reprinted cover illustration of <a href="fever">Full Buxom!! Fainting in Agony Fever</a>. It is interesting that on this image the Toen Comics logo was censored but nothing else changed.</p>

	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/feverplus/contents.png">
					<img src="/assets/images/tank/feverplus/contents.png" alt="Contents" title="Contents"></a><br>Contents
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/feverplus/title.jpg">
					<img src="/assets/images/tank/feverplus/title.jpg" alt="titleplus" title="Title Image"></a><br>Title Page</div>
			</div>
			<div class="clear"></div>
	</div>