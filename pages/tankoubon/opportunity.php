<div class="cgcenter">
        <h1 class="tank_title_en">Fainting in Agony Opportunity</h1>
        <h1 class="tank_title_jp">悶絶オポチュニティ</h1>
    <div class="tank_cover"><img src="/assets/images/tank/opportunity/cover.jpg" alt="Fainting in Agony opportunity Cover" style="max-height: 250px"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Kuriberon Vol.44 - 2016.06.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb044.jpg',
                'chapter' => 'tank/opportunity/ch/002.png',
            ),
            'title' => array(
                'en' => 'X-Ray glasses from planet X',
                'jp' => '遊星からの透け透けメガネX',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.47 - 2016.09.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb047.jpg',
                'chapter' => 'tank/opportunity/ch/028.png',
            ),
            'title' => array(
                'en' => 'The moonlight of Iron Maiden',
                'jp' => '月夜のアイアンメイデン',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.38 - 2015.12.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb038.jpg',
                'chapter' => 'tank/opportunity/ch/052.png',
            ),
            'title' => array(
                'en' => 'Way of the Monzetsu Idol',
                'jp' => '悶絶アイドル道',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.41 - 2016.03.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb041.jpg',
                'chapter' => 'tank/opportunity/ch/076.png',
            ),
            'title' => array(
                'en' => 'Bitch Sniper at the Battlefield',
                'jp' => '戦場の肉穴スナイパー',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.49 - 2016.11.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb049.jpg',
                'chapter' => 'tank/opportunity/ch/100.png',
            ),
            'title' => array(
                'en' => 'Ai-chan\'s madness',
                'jp' => '狂気のアイちゃん',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.51 - 2017.01.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb051.jpg',
                'chapter' => 'tank/opportunity/ch/126.png',
            ),
            'title' => array(
                'en' => 'Super Gesu Bitch Hinata-chan',
                'jp' => 'スーパーゲスビッチひなたちゃん',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.35 - 2015.09.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb035.jpg',
                'chapter' => 'tank/opportunity/ch/150.png',
            ),
            'title' => array(
                'en' => 'Private Penis Gakuen',
                'jp' => '私立おちんちん学園',
            ),
        ),
        array(
            'release' => 'Kuriberon Vol.54 - 2017.04.01',
            'image' => array(
                'manga' => 'mangalist/kuriberon/kb054.jpg',
                'chapter' => 'tank/opportunity/ch/174.png',
            ),
            'title' => array(
                'en' => 'Hot-blood maternity',
                'jp' => '熱血マタニティ',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<h2 class="subtitle">Colored pages</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus iaculis velit ut quam volutpat pulvinar. Aliquam volutpat luctus scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim elit, bibendum eu consectetur sed, viverra quis enim. Aliquam cursus, ante quis venenatis vestibulum, leo leo lacinia erat, quis iaculis mauris ante non tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus a dolor cursus, sagittis risus vel, bibendum justo. Donec massa leo, aliquam in scelerisque ut, consectetur ut velit. Aliquam quis vestibulum sapien, eget bibendum metus.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/title.jpg">
					<img src="/assets/images/tank/opportunity/title.jpg" alt="title" title="title"></a>
				</div>			
			<div class="clear"></div>
	</div>
</div>  

<h2 class="subtitle">Melonbooks purchase special drawing 4P leaflet</h2>

<p>twitter: https://twitter.com/jkp55645/status/866778825157246976 Nobunaga and Autumn leaft gift cards. 4 page leaflet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus iaculis velit ut quam volutpat pulvinar. Aliquam volutpat luctus scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim elit, bibendum eu consectetur sed, viverra quis enim. Aliquam cursus, ante quis venenatis vestibulum, leo leo lacinia erat, quis iaculis mauris ante non tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus a dolor cursus, sagittis risus vel, bibendum justo. Donec massa leo, aliquam in scelerisque ut, consectetur ut velit. Aliquam quis vestibulum sapien, eget bibendum metus.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/omake/01.jpg">
					<img src="/assets/images/tank/opportunity/omake/01.jpg" alt="001" title="001"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/omake/02.jpg">
					<img src="/assets/images/tank/opportunity/omake/02.jpg" alt="002" title="002"></a>
				</div>

				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/omake/03.jpg">
					<img src="/assets/images/tank/opportunity/omake/03.jpg" alt="003" title="003"></a>
				</div>

				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/omake/04.jpg">
					<img src="/assets/images/tank/opportunity/omake/04.jpg" alt="004" title="004"></a>
				</div>				
			<div class="clear"></div>
	</div>
</div>

	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/omake/bonus.jpg">
					<img src="/assets/images/tank/opportunity/omake/bonus.jpg" alt="001" title="001"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/omake/bonus2.jpg">
					<img src="/assets/images/tank/opportunity/omake/bonus2.jpg" alt="002" title="002"></a>
				</div>				
			<div class="clear"></div>
	</div>
</div>
   
<h2 class="subtitle">Draft pages</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus iaculis velit ut quam volutpat pulvinar. Aliquam volutpat luctus scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim elit, bibendum eu consectetur sed, viverra quis enim. Aliquam cursus, ante quis venenatis vestibulum, leo leo lacinia erat, quis iaculis mauris ante non tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus a dolor cursus, sagittis risus vel, bibendum justo. Donec massa leo, aliquam in scelerisque ut, consectetur ut velit. Aliquam quis vestibulum sapien, eget bibendum metus.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/draft01.jpg">
					<img src="/assets/images/tank/opportunity/draft01.jpg" alt="001" title="001"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/draft02.jpg">
					<img src="/assets/images/tank/opportunity/draft02.jpg" alt="002" title="002"></a>
				</div>
                
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/draft03.jpg">
					<img src="/assets/images/tank/opportunity/draft03.jpg" alt="003" title="003"></a>
				</div>

				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/draft04.jpg">
					<img src="/assets/images/tank/opportunity/draft04.jpg" alt="004" title="004"></a>
				</div>			
			<div class="clear"></div>
	</div>
</div>

<h2 class="subtitle">Autograph</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus iaculis velit ut quam volutpat pulvinar. Aliquam volutpat luctus scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim elit, bibendum eu consectetur sed, viverra quis enim. Aliquam cursus, ante quis venenatis vestibulum, leo leo lacinia erat, quis iaculis mauris ante non tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus a dolor cursus, sagittis risus vel, bibendum justo. Donec massa leo, aliquam in scelerisque ut, consectetur ut velit. Aliquam quis vestibulum sapien, eget bibendum metus.
1: https://twitter.com/jkp55645/status/870467134442184704
2: https://twitter.com/jkp55645/status/1029910330623705088
</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/sign.jpg">
					<img src="/assets/images/tank/opportunity/sign.jpg" alt="sign" title="sign"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/opportunity/sign2.jpg">
					<img src="/assets/images/tank/opportunity/sign2.jpg" alt="sign" title="sign"></a>
				</div>
			<div class="clear"></div>
	</div>
</div>