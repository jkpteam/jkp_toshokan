<div class="cgcenter">
        <h1 class="tank_title_en">Super Monzetsu Mega Bitch</h1>
        <h1 class="tank_title_jp">スーパーモンゼツメガビッチ</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/megabitch/cover.jpg"><img src="/assets/images/tank/megabitch/cover.jpg" alt="Super Monzetsu Megabitch Cover"></a></div>
</div>


<?php
    $contents = array(
        array(
            'release' => '0EX Vol.27 - 2010.02.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex27.jpg',
                'chapter' => 'tank/megabitch/ch/005.png',
            ),
            'title' => array(
                'en' => 'Passionate Four-eyed String Bitch',
                'jp' => '情熱のメガネヒモビッチ',
            ),
        ),
        array(
            'release' => '0EX Vol.23 - 2009.10.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex23.jpg',
                'chapter' => 'tank/megabitch/ch/029.png',
            ),
            'title' => array(
                'en' => 'Inhuman Etiquette',
                'jp' => '非道のエチケット',
            ),
        ),
        array(
            'release' => '0EX Vol.25 - 2009.12.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex25.jpg',
                'chapter' => 'tank/megabitch/ch/053.png',
            ),
            'title' => array(
                'en' => 'Phantom Sidekick Homerun',
                'jp' => '幻のサイキックホームラン',
            ),
        ),
        array(
            'release' => '0EX Vol.26 - 2010.01.09',
            'image' => array(
                'manga' => 'mangalist/ex/ex26.jpg',
                'chapter' => 'tank/megabitch/ch/077.png',
            ),
            'title' => array(
                'en' => 'The Counter attack\'s needle balkan!!',
                'jp' => '逆襲のニードルバルカン!!',
            ),
        ),
        array(
            'release' => '0EX Vol.19 - 2009.06.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex19.jpg',
                'chapter' => 'tank/megabitch/ch/101.png',
            ),
            'title' => array(
                'en' => 'Tokyo Drunk Pudding',
                'jp' => 'トーキョードランクプリン',
            ),
        ),
        array(
            'release' => '0EX Vol.18 - 2009.05.09',
            'image' => array(
                'manga' => 'mangalist/ex/ex18.jpg',
                'chapter' => 'tank/megabitch/ch/125.png',
            ),
            'title' => array(
                'en' => 'The soul\'s Blasting Scrum!!',
                'jp' => '魂の爆砕スクラム!!',
            ),
        ),
        array(
            'release' => '0EX Vol.21 - 2009.08.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex21.jpg',
                'chapter' => 'tank/megabitch/ch/149.png',
            ),
            'title' => array(
                'en' => 'Magma Slider',
                'jp' => 'マグマスライダー',
            ),
        ),
        array(
            'release' => '0EX Vol.22 - 2009.09.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex22.jpg',
                'chapter' => 'tank/megabitch/ch/173.png',
            ),
            'title' => array(
                'en' => 'Shaving on Neverending Summer',
                'jp' => '常夏シェービング',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
    
<p><b>1. The soul's Blasting Scrum!!</b><br>
In the original magazine printing the title was misprinted. The word 逆襲 - gyakushuu (english: counterattack) replaced the original word 魂 - tamashii (english: spirit). This was corrected in the tankoubon release.</p>
	
<div style="text-align:center;">
<img src="/assets/images/mangalist/ex/ch/scrum_1.png" alt="scrum" title="scrum">
<img src="/assets/images/arrow.png" alt="arrow" title="arrow">
<img src="/assets/images/mangalist/ex/ch/scrum_2.png" alt="scrum" title="scrum">
</div>    
</span>

<!-- Image Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

<p>The introduction page shows the girl from <b>"Passionate Four-eyed String Bitch"</b> in color as she spreads her vagina. The second page is just a greyscale image of her, and the girl from <b>Magma Slider</b>'s page 11.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/megabitch/003.jpg">
					<img src="/assets/images/tank/megabitch/003.jpg" alt="Introduction page 1" title="Introduction page 1"></a><br>Introduction page 1
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/megabitch/004.png">
					<img src="/assets/images/tank/megabitch/004.png" alt="Introduction page 2" title="Introduction page 2"></a><br>Introduction page 2</div>
			</div>
			<div class="clear"></div>
	</div>