<div class="cgcenter">
        <h1 class="tank_title_en">Mubobi na Siko Ana</h1>
        <h1 class="tank_title_jp">メス穴彼女</h1>
    <div class="tank_cover"><img src="/assets/images/tank/shikoana.jpg" alt="Shikoana" style="max-height: 250px"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'COMIC Megastore Deep Vol. 37 - 2022.02.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd037.jpg',
                'chapter' => 'tank/shikoana/ch/003.jpg',
            ),
            'title' => array(
                'en' => 'Beautiful boss is Drunk Queen',
                'jp' => '美人上司はドランククィーン',
            ),
        ),
        array(
            'release' => 'COMIC Megastore Deep Vol. 36 - 2021.12.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd036.jpg',
                'chapter' => 'tank/shikoana/ch/023.jpg',
            ),
            'title' => array(
                'en' => 'Legend of the Magic Milk',
                'jp' => '魔乳の伝説',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 34 - 2021.08.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd034.jpg',
                'chapter' => 'tank/shikoana/ch/043.jpg',
            ),
            'title' => array(
                'en' => 'Ex-girlfriend Maternity',
                'jp' => '元カノマタニティ',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 31 - 2021.02.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd031.jpg',
                'chapter' => 'tank/shikoana/ch/063.jpg',
            ),
            'title' => array(
                'en' => 'My older meat urinal',
                'jp' => 'オレの年上肉便器',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 29 - 2020.10.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd029.jpg',
                'chapter' => 'tank/shikoana/ch/083.jpg',
            ),
            'title' => array(
                'en' => 'Mysterious! Pizza Aunt',
                'jp' => '怪奇！ ピザおばさん',
            ),
        ),
    );
?>


<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>
<!--
<span class="marker">
<h3>Notes:</h3>
<p>Summer Tentacle and Amazing! Every Holes Girl were republished in the next tankoubon Gucchiri Ana Meito [ぐっちょり穴メイト] on July 2020.</p>
</span>
-->