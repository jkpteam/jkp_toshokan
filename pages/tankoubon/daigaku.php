<div class="cgcenter">
        <h1 class="tank_title_en">Private Slug Holes Academy</h1>
        <h1 class="tank_title_jp">私立ハメ穴学園</h1>
    <div class="tank_cover"><img src="/assets/images/tank/daigaku.jpg" alt="Private Slug Holes Academy Cover"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Megastore H Vol. 112 - 2013.07',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh112.jpg',
                'chapter' => 'tank/daigaku/ch/002.png',
            ),
            'title' => array(
                'en' => 'Desire Matter X',
                'jp' => '欲情の物体X',
            ),
        ),
        array(
            'release' => 'Megastore H Vol. 111 - 2013.05',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh111.jpg',
                'chapter' => 'tank/daigaku/ch/022.png',
            ),
            'title' => array(
                'en' => 'Invincible Combination Senpain',
                'jp' => '無敵合体センパイン',
            ),
        ),
        array(
            'release' => 'Megastore H Vol. 110 - 2013.03',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh110.jpg',
                'chapter' => 'tank/daigaku/ch/042.png',
            ),
            'title' => array(
                'en' => 'Love Tackle',
                'jp' => '恋のタックル',
            ),
        ),
        array(
            'release' => 'Megastore H Vol. 109 - 2013.01',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh109.jpg',
                'chapter' => 'tank/daigaku/ch/062.png',
            ),
            'title' => array(
                'en' => 'Shinano LOUD Ranch',
                'jp' => '特濃ＬＯＵＤ牧場',
            ),
        ),
        array(
            'release' => 'Megastore H Vol. 108 - 2012.11',
            'image' => array(
                'manga' => 'mangalist/megastoreh/msh108.jpg',
                'chapter' => 'tank/daigaku/ch/082.png',
            ),
            'title' => array(
                'en' => 'Spring Napalm Stretch',
                'jp' => '青春ナパームストレッチ！！',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
    <p>1. These stories were released in <a href="/tankoubon/acmate">My lovely Acmate</a> as well.</p>
        <ul>
            <li>Desire Matter X - 欲情の物体X</li>
            <li>Invincible Combination Senpain - 無敵合体センパイン</li>
            <li>Love Tackle - 恋のタックル</li>
            <li>Shinano LOUD Ranch - 特濃ＬＯＵＤ牧場</li>
        </ul>
<p><b>2. Spring Napalm Stretch</b> was released in <a href="/tankoubon/miraclehole">Miracle Hole</a> as well.</p>
</span>