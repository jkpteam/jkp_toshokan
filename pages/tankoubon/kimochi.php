<div class="cgcenter">
        <h1 class="tank_title_en">Every hole feels good</h1>
        <h1 class="tank_title_jp">どの穴でも気持ちいい</h1>
    <div class="tank_cover"><img src="/assets/images/tank/kimochi/cover.jpg" alt="Every hole feels good Cover" style="max-height: 250px"></div>
</div>
<?php
    $contents = array(
        array(
            'release' => 'Magnum Vol.82 - 2016.02.05',
            'image' => array(
                'manga' => 'mangalist/magnum/m082.jpg',
                'chapter' => 'mangalist/magnum/ch/m82.jpg',
            ),
            'title' => array(
                'en' => 'Brutish butterfly',
                'jp' => 'ブルーティシュバタフライ',
            ),
        ),
        array(
            'release' => 'Magnum Vol.78 - 2015.10.02',
            'image' => array(
                'manga' => 'mangalist/magnum/m078.jpg',
                'chapter' => 'mangalist/magnum/ch/m78.jpg',
            ),
            'title' => array(
                'en' => 'Donburi Master Himeka',
                'jp' => 'どんぶりマイスター ひめか',
            ),
        ),
        array(
            'release' => 'Magnum Vol.88 - 2016.08.05',
            'image' => array(
                'manga' => 'mangalist/magnum/m088.jpg',
                'chapter' => 'mangalist/magnum/ch/m88.jpg',
            ),
            'title' => array(
                'en' => 'Hyper double Cooking Star',
                'jp' => 'ハイパーダブルコッキング☆',
            ),
        ),
        array(
            'release' => 'Magnum Vol.99 - 2017.07.07',
            'image' => array(
                'manga' => 'mangalist/magnum/m099.jpg',
                'chapter' => 'mangalist/magnum/ch/m99.jpg',
            ),
            'title' => array(
                'en' => 'Manten acme teacher',
                'jp' => '満点アクメティーチャー',
            ),
        ),
        array(
            'release' => 'Magnum Vol.84 - 2016.04.08',
            'image' => array(
                'manga' => 'mangalist/magnum/m084.jpg',
                'chapter' => 'mangalist/magnum/ch/m84.jpg',
            ),
            'title' => array(
                'en' => 'Drink up! Mass drink',
                'jp' => '飲み干せ！！マッスルドリンク',
            ),
        ),
        array(
            'release' => 'Magnum Vol.92 - 2016.12.02',
            'image' => array(
                'manga' => 'mangalist/magnum/m092.jpg',
                'chapter' => 'mangalist/magnum/ch/m92.jpg',
            ),
            'title' => array(
                'en' => 'My Perosuke',
                'jp' => '私のペロ助',
            ),
        ),
        array(
            'release' => 'Magnum Vol.95 - 2017.03.03',
            'image' => array(
                'manga' => 'mangalist/magnum/m095.jpg',
                'chapter' => 'mangalist/magnum/ch/m95.jpg',
            ),
            'title' => array(
                'en' => 'Business Blackout 24 hours!',
                'jp' => '営業・悶絶24時！',
            ),
        ),
        array(
            'release' => 'Magnum Vol.103 - 2017.11.10',
            'image' => array(
                'manga' => 'mangalist/magnum/m103.jpg',
                'chapter' => 'mangalist/magnum/ch/m103.jpg',
            ),
            'title' => array(
                'en' => 'She is my pet',
                'jp' => '彼女は僕ペット',
            ),
        ),
        array(
            'release' => 'Magnum Vol.75 - 2015.07.03',
            'image' => array(
                'manga' => 'mangalist/magnum/m075.jpg',
                'chapter' => 'mangalist/magnum/ch/m75.jpg',
            ),
            'title' => array(
                'en' => 'Ithacat Riot',
                'jp' => 'イサカ・ライアット',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Image Gallery-->

<h2 class="subtitle">Colored Pages Gallery</h2>

	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/kimochi/002.jpg">
					<img src="/assets/images/tank/kimochi/002.jpg" alt="002" title="002"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/kimochi/contents.jpg">
					<img src="/assets/images/tank/kimochi/contents.jpg" alt="003" title="003"></a>
				</div>
            </div>
			<div class="clear"></div>
</div>

<h2 class="subtitle">Bonus Illustration Card</h2>
<p>If you purchased from Toraoana or Melobooks, this Volume comed with a bonus illustration. <a href="https://twitter.com/jkp55645/status/991221633019924481">Twitter post from JKP.</a></p>
<p>For the buyers of the webshop <a href="http://www.toranoana.jp/mailorder/article/20/0011/86/70/200011867002.html">toranoana.jp</a> a special Illustration Card is included in this volume.<br>
For the buyers of the webshop <a href="https://www.melonbooks.co.jp/detail/detail.php?product_id=122853&adult_view=1">Melonbooks</a> a special Message Card is included in this volume.<br>
sign: https://twitter.com/jkp55645/status/999777936558374913    
sign2: https://twitter.com/jkp55645/status/994023021663866880
    
</p>
	<div class="cgwrapper">
			<div class="cgcenter">				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/kimochi/icard1.jpg">
                    <img src="/assets/images/tank/kimochi/icard1.jpg" alt="Message" title="Illustration card toraoana.jp"></a><br>Bonus Illustration Card</div>
				
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/kimochi/icard1t.jpg">
                    <img src="/assets/images/tank/kimochi/icard1t.jpg" alt="Message" title="Illustration card Melonbooks"></a><br>Bonus Illustration Card</div>
                
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/kimochi/icard2.jpg">
                    <img src="/assets/images/tank/kimochi/icard2.jpg" alt="Message" title="Illustration card toraoana.jp"></a><br>Bonus Illustration Card</div>
				
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/kimochi/icard2t.jpg">
                    <img src="/assets/images/tank/kimochi/icard2t.jpg" alt="Message" title="Illustration card Melonbooks"></a><br>Bonus Illustration Card</div>
			</div>

	</div>
	<div class="clear"></div>




<!-- Author Comment -->
<h2 class="subtitle">Afterword</h2>
<div class="atogaki">
    <a class="nagyobb" href="/assets/images/tank/kimochi/atogaki.jpg">
        <img src="/assets/images/tank/kimochi/atogaki.jpg" alt="atogaki" title="atogaki">
    </a>
    <p>Translation needed!!</p>
</div>