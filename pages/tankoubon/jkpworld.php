<div class="cgcenter">
        <h1 class="tank_title_en">John K. Pe-ta's World</h1>
        <h1 class="tank_title_jp">ジョン・Ｋ・ペー太の世界</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/jkpworld/cover.jpg"><img src="/assets/images/tank/jkpworld/cover.jpg" alt="John K. Pe-ta's World Cover"></a></div>
</div>

<h2 class="subtitle">Contents</h2>

<table cellspacing="0" width="100%" border="0" align="center" style="margin-bottom: 5px">
      <tr color="#5faf80">
        <td class="header">Contents</td>
        </tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%" border="0" style="margin-bottom: 20px";>
  <tr>
	<td><font size="3">Title</font></td>
    <td><div align="center"><font size="3">First Appearance</font></div></td>
    <td><div align="center"><font size="3">Reprint</font></div></td>
  </tr>
    <tr>
    <td><font color="#ff00cc">触手神拳最終奥義 狂い咲きデストロイ<br />Tentacle God's Fist, Last Mystery: Out of Season Bloom Destroyer</font></td>
    <td><font color="#ff00cc"><div align="center">No Magazine Release </div></font></td>
    <td><font color="#ff00cc"><div align="center">Curriculum Plus </div></font></td>
  </tr>
    <tr>
    <td>熱血！悶絶！しごき部長<br />Hot Blooded! Fainting in Agony! The Hard Working Captain</td>
    <td><div align="center">Comic Jumbo 2003.07</div></td>
    <td><div align="center"> Fainting in Agony Caligula Machine</div></td>
  </tr>
    <tr>
    <td>薔薇十字に揉みしだけ！！<br />The Rosicrucian Massage!!</td>
    <td><div align="center">Comic Jumbo 2004.02</div></td>
    <td><div align="center"> Ultra Fainting in Agony Curriculum and Plus</div></td>
  </tr>
    <tr>
    <td>ジェット・ケンタウロス<br />Jet Kentauros</td>
    <td><div align="center">N/A</div></td>
    <td><div align="center">Not in Monzetsu Series</div></td>
  </tr>
    <tr>
    <td>地獄温泉羞恥の湯<br />Hell Spa, Hot Bath of Shame</td>
    <td><div align="center">N/A</div></td>
    <td><div align="center"> Geki!! Fainting in Agony Operation</div></td>
  </tr>
    <tr>
    <td>逆噴射インチキメガネ<br />Reverse Thrust! Bespectacled Cheater</td>
    <td><div align="center">N/A</div></td>
    <td><div align="center"> Fainting in Agony Caligula Machine</div></td>
  </tr>
    <tr>
    <td>死霊のすなぎも<br />Gizzard of the Departed</td>
    <td><div align="center">N/A</div></td>
    <td><div align="center">Not in Monzetsu Series (Guro)</div></td>
    </tr>
    <tr>
    <td>キノコでポン<br />Mushroom Plop</td>
    <td><div align="center">N/A</div></td>
    <td><div align="center">Not in Monzetsu Series (Guro)</div></td>
  </tr>
    <tr>
    <td>カオス同棲<br />Chaotic Cohabitation</td>
    <td><div align="center">N/A</div></td>
    <td><div align="center">Not in Monzetsu Series (Guro)</div></td>
  </tr>
    <tr>
    <td>駅前ブローバック<br />Station Front Blowback</td>
    <td><div align="center">N/A</div></td>
    <td><div align="center">Not in Monzetsu Series</div></td>
  </tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%" border="0" style="margin-bottom: 20px";>
  <tr>
	<td><font size="3">Bonus Content Title</font></td>
    <td><div align="center"><font size="3">Pages</font></div></td>
    <td><div align="center"><font size="3">Comments</font></div></td>
  </tr>
    <tr>
    <td>ペー太の自作解説<br />
    Pe-ta's Self Commentary</td>
    <td><div align="center">19, 57, 70, 143, 182</div></td>
    <td><div align="center">Pe-ta talks about his works  </div></td>
  </tr>
    <tr>
    <td>スペシャル雑談対談ジョンK・ペー太　町野変丸<br />
      Special Talk between John K. Pe-ta and Machino Henmaru</td>
    <td><div align="center">20, 58, 144, 183, 189</div></td>
    <td><div align="center">John K. Pe-ta  Interview </div></td>
  </tr>
    <tr>
    <td>ジョン・K・ペー太評論「拡張する身体」をめぐって　伊藤剛<br />
    Criticism  of John K. Pe-ta:  In regard to &quot;Body Expansion" by Itou Gou </td>
    <td><div align="center">107-110</div></td>
    <td><div align="center">Text by Itou Gou</div></td>
  </tr>
    <tr>
    <td>メッセージイラスト[ハロルド作石]<br />
    Illustrated Message from Harold Sakuishi</td>
    <td><div align="center">184</div></td>
    <td><div align="center">Illustation by Harold Sakuishi</div></td>
  </tr>
    <tr>
    <td>悶絶断面ゆみこちゃん[町野変丸]<br />
    Fainting in Agony X-ray style Yumiko-chan (Machino Henmaru)</td>
    <td><div align="center">185-188</div></td>
    <td><div align="center">Machino Henmaru's short comic</div></td>
  </tr>
    <tr>
    <td>疑問・質問に答えるコーナー<br />Question: Question Reply Corner</td>
    <td><div align="center">190-193</div></td>
    <td><div align="center">Ask Pe-ta corner </div></td>
  </tr>
    <tr>
    <td>ペー太のお仕事年鑑<br />
    Pe-ta's Works by Year</td>
    <td><div align="center">194-196</div></td>
    <td><div align="center">Pe-ta's works from 1998-2005 </div></td>
    </tr>
</table>

<!-- Images Gallery
<h2 class="subtitle">Unique Images Gallery</h2>
<table cellpadding="3" cellspacing="0" width="100%" border="0" style="margin-bottom: 20px";>
    <tr>
    <td><center><table width="200" border="0">
  <tr>
    <td>Contents</td>
    <td>Inside Cover</td>
  </tr>
  <tr>
    <td><img src="/assets/images/jkpworld/peta6-contents.jpg" alt="Contents" border="0" /></td>
    <td><img src="/assets/images/jkpworld/peta6-cover.jpg" alt="Cover" border="0" /></td>
        </tr>
</table></center>
</td>
  </tr>
   <tr>
    <td><center><table width="600" border="0">
  <tr>
    <td>Downpour March</td>
    <td>Wake Up! Vise Calisthenics</td>
    <td>Explosion Cover 1</td>
    <td>Explosion Cover 2</td>
  </tr>
  <tr>
    <td><a target="_blank" href="/assets/images/colour/downpour_march-big.jpg"><img src="/assets/images/colour/downpour_march-s.jpg" alt="Downpour March Colour" border="0" /></a></td>
    <td><a target="_blank" href="/assets/images/colour/wake_up-big.jpg"><img src="/assets/images/colour/wake_up-s.jpg" alt="Wake Up! Vise Calisthenics Colour" border="0" /></a></td>
    <td><a target="_blank" href="/assets/images/colour/explosion_cover_1-big.jpg"><img src="/assets/images/colour/explosion_cover_1-s.jpg" alt="Explosion Cover 1" border="0" /></a></td>
    <td><a target="_blank" href="/assets/images/colour/explosion_cover_2-big.jpg"><img src="/assets/images/colour/explosion_cover_2-s.jpg" alt="Explosion Cover 2" border="0" /></a></td>
  </tr>
        </table></center>
    <tr>
     <td align="justify">Interesting to see that the pictures on the cover have nothing to do with the chapters found in this volume, because those were not included. The first image was the cover illustration for <strong>"After School FUCK Excellent"</strong> which you can find in the <a href="/anthology">Anthology books</a> listing on this site. The second image was also featured on the cover for a similar volume called <strong>"Hentai Virginity Loss Excellent"</strong>. The last two images origin are yet unknown but they were featured on the foldout cover of <a href="/tankoubon/explosion">FAINTING in agony!! Explosion.</a> The coloured page of <strong>"Hell Spa, Hot Bath of Shame"</strong> was not an official work. It was coloured by a fan.</td>
  </tr>
</td>
  </tr>
</table>

Coloured Images Gallery
<h2 class="subtitle">Colored Images Gallery</h2>

<table cellpadding="3" cellspacing="0" width="100%" border="0" style="margin-bottom: 10px";>
<tr>
    <td><center>
        <table width="800" border="0">
<tr>
    <td align="center" colspan="4">Tentacle God's Fist, Last Mystery: Out of Season Bloom Destroyer</td>
    <td>Hell Spa, Hot Bath of Shame</td>
    </tr>
<tr>
      <td><img src="/assets/images/jkpworld/tentaclegodc-1.jpg" alt="Tentacle God p1" border="0" /></td>
      <td><img src="/assets/images/jkpworld/tentaclegodc-2.jpg" alt="Tentacle God p2" border="0" /></td>
      <td><img src="/assets/images/jkpworld/tentaclegodc-3.jpg" alt="Tentacle God p3" border="0" /></td>
      <td><img src="/assets/images/jkpworld/tentaclegodc-4.jpg" alt="Tentacle God p4" border="0" /></td>
      <td><a target="_blank" href="/assets/images/colour/hell_spa-fancolor-big.jpg"><img src="/assets/images/colour/hell_spa-fancolor-sm.jpg" alt="Hell Spa, Hot Bath of Shame Fan Colour" border="0" /></a></td>
</tr>
        </table>
        </center>
     </td>

</tr>

</table>
<table>
    <tr>
     <td align="justify">The chapter <strong>"Tentacle God's Fist, Last Mystery: Out of Season Bloom Destroyer"</strong> was drawn for this release and had 4 coloured pages. Later this chapter was re-released in the book <a href="/tankoubon/curriculumplus">Super Fainting in Agony Curriculum Plus</a> but not in colour! The coloured page of <strong>"Hell Spa, Hot Bath of Shame"</strong> is not an official work. It was coloured by a fan.

</td>
        </tr>
</table>
-->