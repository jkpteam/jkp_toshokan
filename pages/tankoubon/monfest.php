<div class="cgcenter">
        <h1 class="tank_title_en">Monfest XI</h1>
        <h1 class="tank_title_jp">モンフェスXI</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/monfesu/cover.jpg"><img src="/assets/images/tank/monfesu/cover.jpg" alt="Monfest XI Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => '0EX Vol.10 - 2008.09.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex10.jpg',
                'chapter' => 'tank/monfesu/ch/005.png',
            ),
            'title' => array(
                'en' => 'Summer Riot \'09',
                'jp' => 'サマーライアット\'09',
            ),
        ),
        array(
            'release' => '0EX Vol.09 - 2008.08.09',
            'image' => array(
                'manga' => 'mangalist/ex/ex09.jpg',
                'chapter' => 'tank/monfesu/ch/029.png',
            ),
            'title' => array(
                'en' => 'Scorching Extreme Batting',
                'jp' => '灼熱のエクストリーム打法',
            ),
        ),
        array(
            'release' => '0EX Vol.12 - 2008.11.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex12.jpg',
                'chapter' => 'tank/monfesu/ch/054.png',
            ),
            'title' => array(
                'en' => 'Urgent Awakening, Switch ON!!',
                'jp' => '緊急覚醒スイッチON!!',
            ),
        ),
        array(
            'release' => '0EX Vol.17 - 2009.04.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex17.jpg',
                'chapter' => 'tank/monfesu/ch/071.png',
            ),
            'title' => array(
                'en' => 'Rivalry Burning',
                'jp' => 'カタキバーニング',
            ),
        ),
        array(
            'release' => '0EX Vol.11 - 2008.10.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex11.jpg',
                'chapter' => 'tank/monfesu/ch/095.png',
            ),
            'title' => array(
                'en' => 'Hyper Ox\'s Hour of Fear',
                'jp' => 'ハイパー丑の刻フィアー',
            ),
        ),
        array(
            'release' => '0EX Vol.15 - 2009.02.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex15.jpg',
                'chapter' => 'tank/monfesu/ch/119.png',
            ),
            'title' => array(
                'en' => 'Ramage Ghost',
                'jp' => 'ランペイジゴースト',
            ),
        ),
        array(
            'release' => '0EX Vol.16 - 2009.03.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex16.jpg',
                'chapter' => 'tank/monfesu/ch/144.png',
            ),
            'title' => array(
                'en' => 'Ruinous Catastrophe',
                'jp' => '落ちぶれてカタストロフ',
            ),
        ),
        array(
            'release' => '0EX Vol.13 - 2008.12.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex13.jpg',
                'chapter' => 'tank/monfesu/ch/168.png',
            ),
            'title' => array(
                'en' => 'Whimsy Leak Custard Heaven',
                'jp' => 'ダダ漏れカスタードヘブン',
            ),
        ),
        array(
            'release' => '0EX Vol.14 - 2009.01.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex14.jpg',
                'chapter' => 'tank/monfesu/ch/185.png',
            ),
            'title' => array(
                'en' => 'Wildly Blossoming Black Sarena',
                'jp' => '狂い咲きブラックサレナ',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>


<span class="marker">
<h3>Notes:</h3>
    
	<p><b>1. Summer Riot'08</b><br>
In the original magazine printing the title was named <b>Summer Riot'08</b> but this was later changed in the tankoubon release <a href="/tankoubon/monfest">Monfest XI</a> to the title Summer Riot'09.</p>
	
<div style="text-align:center;">
<img src="/assets/images/mangalist/ex/ch/summerriot08.png" alt="summerriot08" title="summerriot08">
<img src="/assets/images/arrow.png" alt="arrow" title="arrow">
<img src="/assets/images/mangalist/ex/ch/summerriot09.png" alt="summerriot09" title="summerriot09">
</div>
	
<p><b>2. Ruinous Catastrophe</b><br>
On the <a href="http://www.coremagazine.co.jp/zeroex/zeroex16.html">official website</a> there was a typo, they have typed the word "ochifurete" (落ちぶれて) with a kanji (落) instead of the kana version what was on the title page.</p>
	
<div style="text-align:center;">
<img src="/assets/images/mangalist/ex/ch/ruinousc.png" alt="ruinousc" title="ruinousc">
</div>    
</span>

<!-- Image Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

<p>The introduction page shows the girl from <b>"Hyper Ox's Hour of Fear"</b> in color as she opens up her butthole and vagina. The second page is just a greyscale image of her, and the girl from <b>"Rampage Ghost"</b>.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/monfesu/003.jpg">
					<img src="/assets/images/tank/monfesu/003.jpg" alt="Introduction page 1" title="Introduction page 1"></a><br>Introduction page 1
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/monfesu/004.png">
					<img src="/assets/images/tank/monfesu/004.png" alt="Introduction page 2" title="Introduction page 2"></a><br>Introduction page 2</div>
			</div>
			<div class="clear"></div>
	</div>