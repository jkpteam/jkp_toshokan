<div class="cgcenter">
        <h1 class="tank_title_en">Quavery Fainting In Agony License</h1>
        <h1 class="tank_title_jp">プルプル 悶絶ライセンス</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/license/cover.jpg"><img src="/assets/images/tank/license/cover.jpg" alt="Quavery Fainting In Agony License Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Megaplus Vol.36 - 2006.09.09',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp36b.jpg',
                'chapter' => 'tank/license/ch/002.jpg',
            ),
            'title' => array(
                'en' => 'Love Rock Fucking Live!!',
                'jp' => 'LoveRockファッキンライブ!!',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.37 - 2006.10.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp37b.jpg',
                'chapter' => 'tank/license/ch/022.png',
            ),
            'title' => array(
                'en' => 'Transcendence!! After School Revolution',
                'jp' => '超絶!! 放課後レボリューション',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.38 - 2006.11.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp38b.jpg',
                'chapter' => 'tank/license/ch/040.png',
            ),
            'title' => array(
                'en' => 'All Night Long',
                'jp' => 'オールナイトロング',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.39 - 2006.12.09',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp39b.jpg',
                'chapter' => 'tank/license/ch/060.png',
            ),
            'title' => array(
                'en' => 'Artistic Sacrifice',
                'jp' => 'アートないけにえ',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.40 - 2007.01.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp40b.jpg',
                'chapter' => 'tank/license/ch/080.png',
            ),
            'title' => array(
                'en' => 'Super Brute Park!!',
                'jp' => 'スーパー鬼畜パーク!!',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.28 - 2006.01.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp28b.jpg',
                'chapter' => 'tank/license/ch/100.png',
            ),
            'title' => array(
                'en' => 'Exploding Hot Spring Pink Fire!!',
                'jp' => '爆裂温泉桃色ファイヤー!!',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.30 - 2006.03.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp30b.jpg',
                'chapter' => 'tank/license/ch/116.png',
            ),
            'title' => array(
                'en' => 'Passion Jet Front',
                'jp' => '欲情ジェット戦線!!',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.32 - 2006.05.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp32b.jpg',
                'chapter' => 'tank/license/ch/134.png',
            ),
            'title' => array(
                'en' => 'Monzetsu Golden Crusher!',
                'jp' => '悶絶ゴールデンクラッシャー',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.35 - 2006.08.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp35b.jpg',
                'chapter' => 'tank/license/ch/154.png',
            ),
            'title' => array(
                'en' => 'Light-Blue Delusion Butterfly',
                'jp' => '水色暴走バタフライ',
            ),
        ),
        array(
            'release' => 'Megaplus Vol.34 - 2006.07.10',
            'image' => array(
                'manga' => 'mangalist/megaplus/mp34b.jpg',
                'chapter' => 'tank/license/ch/174.png',
            ),
            'title' => array(
                'en' => 'Certain Kill Peach Butt Burning!!',
                'jp' => '必殺 桃尻バーニング!!',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
<p>Certain Kill Peach Butt Burning!! [必殺 桃尻バーニング!!] have a 2 page Bonus addition for this release only.</p>
        <div class="cgwrapper">
            <div class="cgcenter">
                <div class="kep"><img src="/assets/images/tank/license/ch/194.png"></div>
                <div class="kep"><img src="/assets/images/tank/license/ch/195.png"></div>
            </div>
            <div class="clear"></div>
        </div>
</span>


<!-- Image Gallery-->

<h2 class="subtitle">Colored Pages Gallery</h2>

<p>This volume features two manga chapters with colored illustrations. <b>"Love Rock Fucking Live!!"</b> have one colored title page and three manga pages. The second is <b>"Certain Kill Peach Butt Burning!!"</b> which had a <a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp34_002.jpg">colored title page</a> in <i><b>Comic Megaplus Vol.34</b></i> but this illustration is only featured in this volume's cover foldout.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/license/ch/002.jpg">
					<img src="/assets/images/tank/license/ch/002.jpg" alt="Love Rock Fucking Live!!" title="Love Rock Fucking Live!!"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/license/ch/003.jpg">
					<img src="/assets/images/tank/license/ch/003.jpg" alt="Love Rock Fucking Live!!" title="Love Rock Fucking Live!!"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/license/ch/004.jpg">
					<img src="/assets/images/tank/license/ch/004.jpg" alt="Love Rock Fucking Live!!" title="Love Rock Fucking Live!!"></a>
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/license/ch/005.jpg">
					<img src="/assets/images/tank/license/ch/005.jpg" alt="Love Rock Fucking Live!!" title="Love Rock Fucking Live!!"></a>
				</div>
	</div>
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/mangalist/megaplus/ch/mp34_002.jpg">
					<img src="/assets/images/mangalist/megaplus/ch/mp34_002.jpg" alt="Certain Kill Peach Butt Burning!!" title="Certain Kill Peach Butt Burning!!"></a>
				</div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/license/mp034_002.jpg">
					<img src="/assets/images/tank/license/mp034_002.jpg" alt="Certain Kill Peach Butt Burning!!" title="Certain Kill Peach Butt Burning!!"></a>
				</div>
	</div>
			<div class="clear"></div>
</div>

<!-- Author Comment -->
<h2 class="subtitle">Afterword</h2>
<div class="atogaki">
    <a class="nagyobb" href="/assets/images/tank/license/ch/Atogaki.png">
        <img src="/assets/images/tank/license/ch/Atogaki.png" alt="atogaki" title="atogaki">
    </a>
    <p>Translation needed!!</p>
</div>