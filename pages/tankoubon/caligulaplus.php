<div class="cgcenter">
        <h1 class="tank_title_en">Fainting in Agony Caligula Machine Plus</h1>
        <h1 class="tank_title_jp">悶絶カリギュラマシーンプラス</h1>
    <div class="tank_cover"><img src="/assets/images/tank/caligulaplus/cover.jpg" alt="Fainting in Agony Caligula Machine Plus Cover"></div>
</div>


<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No.188 - 2003.08',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-08.jpg',
                'chapter' => 'tank/caligula/ch/005.png',
            ),
            'title' => array(
                'en' => 'Straight Pitch! Bewithchment Operation',
                'jp' => '直球！悩殺オペレーション',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo 2003 ?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/caligula/ch/023.png',
            ),
            'title' => array(
                'en' => 'Steam - Bewitchment Fever',
                'jp' => '湯けむり悩殺フィーバー',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.185 - 2003.05',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-05.jpg',
                'chapter' => 'tank/caligula/ch/039.png',
            ),
            'title' => array(
                'en' => 'Spartacus Games - Directive 076',
                'jp' => 'スパルタ指令076',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.180 - 2002.12',
            'image' => array(
                'manga' => 'mangalist/jumbo/2002-12.jpg',
                'chapter' => 'tank/caligula/ch/057.png',
            ),
            'title' => array(
                'en' => 'My Neighbor\'s Meat',
                'jp' => 'となりの肉',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.189 - 2003.09',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-09.jpg',
                'chapter' => 'tank/caligula/ch/073.png',
            ),
            'title' => array(
                'en' => 'Three Part Milk Explosion Summit Alpha',
                'jp' => '第3次爆乳サミットα',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.184 - 2003.04',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-04.jpg',
                'chapter' => 'tank/caligula/ch/091.png',
            ),
            'title' => array(
                'en' => 'Open Ass Model - Fainting Club Captain',
                'jp' => '肛開モデル悶絶部長',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo 2003 ?',
            'image' => array(
                'manga' => 'mangalist/jumbo/nopic.png',
                'chapter' => 'tank/caligula/ch/107.png',
            ),
            'title' => array(
                'en' => 'Reverse Thrust! Bespectacled Cheater',
                'jp' => '逆噴射インチキメガネ',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.183 - 2003.03',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-03.jpg',
                'chapter' => 'tank/caligula/ch/125.png',
            ),
            'title' => array(
                'en' => 'Breast Karate, Milk Fist',
                'jp' => 'オッパイ空手ミルク拳',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.187 - 2003.07',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-07.jpg',
                'chapter' => 'tank/caligula/ch/141.png',
            ),
            'title' => array(
                'en' => 'Hot Blooded! Fainting in Agony! The Hard Working Captain',
                'jp' => '熱血！悶絶！しごき部長',
            ),
        ),
        array(
            'release' => 'Fainting in Agony Caligula Machine',
            'image' => array(
                'manga' => 'tank/caligula.jpg',
                'chapter' => 'tank/caligula/ch/159.png',
            ),
            'title' => array(
                'en' => 'Bonus Comic: Miracle Justice Girl J-Ko',
                'jp' => 'おまけマンガ ミラクル女子鋼製J子',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.18 - 2011.11.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm18b.jpg',
                'chapter' => 'mangalist/megamilk/ch/mmv18.jpg',
            ),
            'title' => array(
                'en' => 'Lecture on Sex for Dohtei!!!',
                'jp' => 'レクチャーエブン',
            ),
        ),
        array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/caligula_plus.jpg',
                'chapter' => 'tank/caligulaplus/ch/femaleninja.jpg',
            ),
            'title' => array(
                'en' => 'Female Ninja Fainting in Agony',
                'jp' => 'くの一悶絶帖',
            ),
        ),	
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
    <p>1. These stories were released in different <a href="/anthology">Anthology</a> Tankoubons.</p>
        <ul>
            <li>Straight Pitch! Bewithchment Operation: Creampie Devil Rape CLIMAX</li>
            <li>Three Part Milk Explosion Summit Alpha: Tortured Sex Object Excellent</li>
            <li>Reverse Thrust! Bespectacled Cheater: Girls with Glasses Excellent + JKP's World</li>
            <li>Hot Blooded! Fainting in Agony! The Hard Working Captain: Gang Rape and Promiscuity Excellent + JKP's World</li>
        </ul>
    
    <p>2. The story titled <b>"Three Part Milk Explosion Summit Alpha" [第3次爆発乳サミットα]</b> got mistranslated an additional kanji (hatsu - 発) was added to the word (bakunyuu - 爆乳), the original title is something like "Three Part Eneromous Tits Summit Alpha".</p>
    
    <p>3. On the official website, <b>"Open Ass Model - Fainting Club Captain" [肛開モデル悶絶部長]</b> was mistyped, they have used the (mon - 門) kanji instead of the (hiraki - 開) kanji.</p>
    
    <p>4. On the official website, the bonus story <b>"Miracle Justice Girl J-Ko" [ミラクル女子鋼製J子]</b> was mistyped, they have used the word (kōtetsu - 鋼鉄) instead of (kōsei - 鋼製).</p>
</span>    
    

<!-- Images Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

	<div class="cgwrapper">
			<div class="cgcenter">

				<div class="kep">
					<img src="/assets/images/tank/caligulaplus/color.jpg" alt="color" title="Female Ninja Fainting in Agony"><br>Color</div>
			</div>
			<div class="clear"></div>
	</div>