<div class="cgcenter">
        <h1 class="tank_title_en">Gucchori Ana Meito</h1>
        <h1 class="tank_title_jp">ぐっちょり穴メイト</h1>
    <div class="tank_cover"><img src="/assets/images/tank/anameito.jpg" alt="Gucchori Ana Meito" style="max-height: 250px"></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'Megastore Deep Vol. 09 - 2017.05.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd009.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd009.jpg',
            ),
            'title' => array(
                'en' => 'Onee Trap',
                'jp' => 'お姉トラップ',
            ),
        ),  
        array(
            'release' => 'Megastore Deep Vol.11 - 2017.09.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd011.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd011.jpg',
            ),
            'title' => array(
                'en' => 'Summer Tentacle',
                'jp' => 'サマーテンタクル',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol.23 - 2019.09.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd023.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd023.jpg',
            ),
            'title' => array(
                'en' => 'Henshin switch',
                'jp' => '変身スイッチ',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 15 - 2018.05.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd015.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd015.jpg',
            ),
            'title' => array(
                'en' => 'She is a black hunter',
                'jp' => '彼女は黒いハンター',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 25 - 2020.02.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd025.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd025.jpg',
            ),
            'title' => array(
                'en' => 'You, become a female with me',
                'jp' => '君よ俺で雌になれ',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol. 13 - 2018.01.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd013.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd013.jpg',
            ),
            'title' => array(
                'en' => 'Flesh Maman Life',
                'jp' => '肉欲ママンライフ',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol.17 - 2018.09.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd017.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd017.jpg',
            ),
            'title' => array(
                'en' => 'Amazing ! Every holes girl',
                'jp' => '怪奇！全穴女<br>',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol.27 - 2020.06.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd027.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd027.jpg',
            ),
            'title' => array(
                'en' => 'Yokubari Eggs',
                'jp' => 'よくばりエッグス',
            ),
        ),
        array(
            'release' => 'Megastore Deep Vol.19 - 2019.01.24',
            'image' => array(
                'manga' => 'mangalist/megastoredeep/msd019.jpg',
                'chapter' => 'mangalist/megastoredeep/ch/msd019.jpg',
            ),
            'title' => array(
                'en' => 'Camp Fever',
                'jp' => 'キャンプフィーバー',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<span class="marker">
<h3>Notes:</h3>
<p>The first two story (Summer Tentacle and Amazing! Every Holes Girl) were already published in the previous tankoubon メス穴彼女 on January 2019.</p>
<p>The last story "Henshin switch" (変身スイッチ) was republished in COMIC Hotmilk 2020. Vol. 07.</p>
<p>#1,2,6 Onee Trap, Summer Tentacle, Flesh Maman Life was already released in Mesu Ana Kanojo.</p>
</span>

<h2 class="subtitle">Bonus Illustration Card</h2>
<p>If you purchased from Toraoana or Melobooks, this Volume comed with a bonus leaflet and a 3 page comic. <a href="https://twitter.com/jkp55645/status/1270943658255118337">Twitter post</a> of Melonbooks bonus from JKP and <a href="https://twitter.com/jkp55645/status/1270284089120047106">another one</a>.</p>
<p>For the buyers of the webshop <a href="https://ec.toranoana.jp/tora_r/ec/item/200012111746/">Toranoana</a> a special <a href="https://twitter.com/tora_eigyou_a/status/1283340132129505283">illustration card</a> is included in this volume.</p>
	<div class="cgwrapper">
			<div class="cgcenter">				
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/anameito/omake/to-leaflet01_sample.jpg">
                    <img src="/assets/images/tank/anameito/omake/to-leaflet01_sample.jpg" alt="Message" title="Toranoana"></a><br></div>
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/anameito/omake/to-leaflet01_raw.jpg">
                    <img src="/assets/images/tank/anameito/omake/to-leaflet01_raw.jpg" alt="Message" title="Toranoana"></a><br></div>
			</div>

	</div>
	<div class="clear"></div>
<p>For the buyers of the webshop <a href="https://www.melonbooks.co.jp/detail/detail.php?product_id=685366">Melonbooks</a> a special 4 page thank you gift is included in this volume for a limited time.<br></p>
	<div class="cgwrapper">
			<div class="cgcenter">				
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/anameito/omake/001.png">
                    <img src="/assets/images/tank/anameito/omake/001.png" alt="Message" title="Illustration card Melonbooks"></a></div>	
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/anameito/omake/002.png">
                    <img src="/assets/images/tank/anameito/omake/002.png" alt="Message" title="Illustration card Melonbooks"></a></div>	
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/anameito/omake/003.png">
                    <img src="/assets/images/tank/anameito/omake/003.png" alt="Message" title="Illustration card Melonbooks"></a></div>	
                <div class="kep"><a class="nagyobb" href="/assets/images/tank/anameito/omake/004.png">
                    <img src="/assets/images/tank/anameito/omake/004.png" alt="Message" title="Illustration card Melonbooks"></a></div>	
			</div>

	</div>
	<div class="clear"></div>


<h2 class="subtitle">Sign</h2>


	<div class="cgwrapper">
			<div class="cgcenter">
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/anameito/sign.jpg">
					<img src="/assets/images/tank/anameito/sign.jpg" alt="Sign" title="Sign"></a></div>
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/anameito/sign2.jpg">
					<img src="/assets/images/tank/anameito/sign2.jpg" alt="Sign" title="Sign"></a></div>
        </div>
			<div class="clear"></div>
</div>