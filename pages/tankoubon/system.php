<div class="cgcenter">
        <h1 class="tank_title_en">Monzetsu System!</h1>
        <h1 class="tank_title_jp">もんぜつ系!</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/system/cover.jpg"><img src="/assets/images/tank/system/cover.jpg" alt="Monzetsu System! Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'MegaMilk Vol.14 - 2011.07.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm14b.jpg',
                'chapter' => 'tank/system/ch/005.png',
            ),
            'title' => array(
                'en' => 'Brutal 23:00',
                'jp' => 'ブルータル23時',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.10 - 2011.03.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm10b.jpg',
                'chapter' => 'tank/system/ch/029.png',
            ),
            'title' => array(
                'en' => 'Twisted at Dawn',
                'jp' => '暁にねじれて',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.11 - 2011.04.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm11b.jpg',
                'chapter' => 'tank/system/ch/053.png',
            ),
            'title' => array(
                'en' => 'New Yuri Hell',
                'jp' => '新百合地獄',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.09 - 2011.02.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm09b.jpg',
                'chapter' => 'tank/system/ch/077.png',
            ),
            'title' => array(
                'en' => 'Give Me Pig',
                'jp' => 'ギブミーピッグ',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.12 - 2011.05.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm12b.jpg',
                'chapter' => 'tank/system/ch/101.png',
            ),
            'title' => array(
                'en' => 'Hyper Contortion',
                'jp' => 'ハイパーコントーション',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.13 - 2011.06.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm13b.jpg',
                'chapter' => 'tank/system/ch/125.png',
            ),
            'title' => array(
                'en' => 'Graduation Forbidden Literature',
                'jp' => '卒業禁書',
            ),
        ),
        array(
            'release' => '0EX Vol.24 - 2009.11.10',
            'image' => array(
                'manga' => 'mangalist/ex/ex29.jpg',
                'chapter' => 'tank/system/ch/149.png',
            ),
            'title' => array(
                'en' => 'Complex Only One!',
                'jp' => 'コンプレックスオンリーワン！',
            ),
        ),
        array(
            'release' => 'MegaMilk Vol.06 - 2010.11.10',
            'image' => array(
                'manga' => 'mangalist/megamilk/mm06b.jpg',
                'chapter' => 'tank/system/ch/173.png',
            ),
            'title' => array(
                'en' => 'Super Stomach Down',
                'jp' => 'スーパーストマックダウン',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Image Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/system/003.jpg">
					<img src="/assets/images/tank/system/003.jpg" alt="Contents" title="Contents"></a><br>Contents
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/system/004.jpg">
					<img src="/assets/images/tank/system/004.jpg" alt="Title page" title="Title page"></a><br>Title page</div>
			</div>
			<div class="clear"></div>
	</div>


<!-- Author Comment -->
<h2 class="subtitle">Afterword</h2>
<div class="atogaki">
    <a class="nagyobb" href="/assets/images/tank/system/atogaki.png">
        <img src="/assets/images/tank/system/atogaki.png" alt="atogaki" title="atogaki">
    </a>
    <p>Translation needed!!</p>
</div>