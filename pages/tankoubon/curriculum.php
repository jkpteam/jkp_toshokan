<div class="cgcenter">
        <h1 class="tank_title_en">Super Fainting in Agony Curriculum</h1>
        <h1 class="tank_title_jp">超悶絶カリキュラム</h1>
    <div class="tank_cover"><a class="nagyobb" href="/assets/images/tank/curriculum/cover.jpg"><img src="/assets/images/tank/curriculum/cover.jpg" alt="Super Fainting in Agony Curriculum Cover"></a></div>
</div>

<?php
    $contents = array(
        array(
            'release' => 'COMIC Jumbo No.198 - 2004.06',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-06.jpg',
                'chapter' => 'tank/curriculum/ch/006.png',
            ),
            'title' => array(
                'en' => 'Pink Revenge Festival',
                'jp' => '桃色リベンジ祭り',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.195 - 2004.03',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-03.jpg',
                'chapter' => 'tank/curriculum/ch/023.png',
            ),
            'title' => array(
                'en' => 'Forbidden Juice Mix',
                'jp' => '禁断のミックスジュース',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.190 - 2003.10',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-10.jpg',
                'chapter' => 'tank/curriculum/ch/041.png',
            ),
            'title' => array(
                'en' => 'Sister\'s Pink Bath Hell',
                'jp' => '妹地獄ピンク風呂',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.197 - 2004.05',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-05.jpg',
                'chapter' => 'tank/curriculum/ch/059.png',
            ),
            'title' => array(
                'en' => 'Hard After - School Doll',
                'jp' => '放課後コケシ固め',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.193 - 2004.01',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-01.jpg',
                'chapter' => 'tank/curriculum/ch/077.png',
            ),
            'title' => array(
                'en' => 'Direct hit!! Fainting in Agony by the Hellish Sword',
                'jp' => '直撃！！悶絶地獄剣',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.192 - 2003.12',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-12.jpg',
                'chapter' => 'tank/curriculum/ch/095.png',
            ),
            'title' => array(
                'en' => 'Frenzy! Dead Drunk March',
                'jp' => '狂乱！泥酔行進曲',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.194 - 2004.02',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-02.jpg',
                'chapter' => 'tank/curriculum/ch/113.png',
            ),
            'title' => array(
                'en' => 'The Rosicrucian Massage!!',
                'jp' => '薔薇十字に揉みしだけ！！',
            ),
        ),
        array(
            'release' => 'COMIC Jumbo No.196 - 2004.04',
            'image' => array(
                'manga' => 'mangalist/jumbo/2004-04.jpg',
                'chapter' => 'tank/curriculum/ch/131.png',
            ),
            'title' => array(
                'en' => 'Cherry Blossoms Scatter',
                'jp' => 'サクラチルチル',
            ),
        ),
		array(
            'release' => 'COMIC Jumbo No.191 - 2003.11',
            'image' => array(
                'manga' => 'mangalist/jumbo/2003-11.gif',
                'chapter' => 'tank/curriculum/ch/149.png',
            ),
            'title' => array(
                'en' => 'Restraint, Anal Tyranny, Paparazzi',
                'jp' => '拘束.肛虐.パパラッチ',
            ),
        ),
		array(
            'release' => 'Bonus for this Release',
            'image' => array(
                'manga' => 'tank/curriculum.jpg',
                'chapter' => 'tank/curriculum/ch/167.png',
            ),
            'title' => array(
                'en' => 'Bonus Pages',
                'jp' => 'おまけページ',
            ),
        ),
    );
?>

<h2 class="subtitle">Contents</h2>

    <?php renderTankoubonList($contents) ?>

<!-- Image Gallery-->
<h2 class="subtitle">Unique Images Gallery</h2>

<p>The cover and title page shows the girl from the first chapter <strong>"Pink Revenge Festival"</strong>. Altough this chapter originally had 4 colored pages when first printed in Comic Jumbo magazine, these pages were not printed in color this time. The second release <a  href="curriculumplus">Super Fainting in Agony Curriculum Plus</a> corrected this.</p>
	<div class="cgwrapper">
			<div class="cgcenter">
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/curriculum/contents.png">
					<img src="/assets/images/tank/curriculum/contents.png" alt="Contents" title="Contents"></a><br>Contents
				</div>
				
				<div class="kep"><a class="nagyobb" href="/assets/images/tank/curriculum/title.jpg">
					<img src="/assets/images/tank/curriculum/title.jpg" alt="title" title="title"></a><br>Title Page</div>

			</div>
			<div class="clear"></div>
	</div>