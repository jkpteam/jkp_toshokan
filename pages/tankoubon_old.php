<p>Commercial hentai comics are released in monthly magazines such as <b>COMIC Megaplus</b> and then later compiled into seperate volumes. These volumes referred as Tankōbon (単行本) amongst western Manga fans but for the Japanese it simply means "comic book". On this page you can see a list of all JKP's tankōbons. You can click on the pictures to see detailed information of the particular volumes.</p>

<a href="javascript:toggleDiv('Touen')" style="text-decoration: none"><h2 class="subtitle">Published by Toen Shobo></a>
<p>From 2003 till 2007 JKP's comics were released in <a href="/magazine/jumbo">Comic Jumbo</a>. Its publisher <b>Toen Shobo</b> (桃園書房) went bankrupt in mid-2007.</p>

<div id="Touen" style="display: none;">

<a href="/tankoubon/caligula" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/caligula.jpg">

</div>
<div class="descript">Fainting in Agony Caligula Machine</div>

<div class="info">
Volume Information</h3>
<p>
<span>Japanese Title:</span>悶絶カリギュラマシーン<br>
<span>Released:</span>2003/09/25<br>
<span>Retail:</span>¥900<br>
<span>Publisher:</span>Toen Shobou<br>
<span>Pages:</span>164<br>
<span>Scanlated:</span>SCANLATED by TCup<br>
</p></div>
</div></a>

<a href="/tankoubon/curriculum" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/curriculum.jpg">
</div>

<div class="info">
<div class="descript">Super Fainting in Agony Curriculum</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>超悶絶カリキュラム<br>
<span>Released:</span>2004/07/15<br>
<span>Retail:</span>¥900<br>
<span>Publisher:</span>Toen Shobou<br>
<span>Pages:</span>172<br>
<span>Scanlated:</span>SCANLATED by TCup<br>
</p></div>
</div></a>

<a href="/tankoubon/operation" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/operation.jpg">
</div>

<div class="info">
<div class="descript">Intense!! Fainting in Agony Operation</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>激！！悶絶オペレーション<br>
<span>Released:</span>2005/03/15<br>
<span>Retail:</span>¥900<br>
<span>Publisher:</span>Toen Shobou<br>
<span>Pages:</span>172<br>
<span>Scanlated:</span>SCANLATED by TCup<br>
</p></div>
</div></a>

<a href="/tankoubon/fever" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/fever.jpg">
</div>

<div class="info">
<div class="descript">Full Buxom!! Fainting in Agony Fever</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>ムチムチ！！悶絶フィーバー<br>
<span>Released:</span>2005/11/15<br>
<span>Retail:</span>¥900<br>
<span>Publisher:</span>Toen Shobou<br>
<span>Pages:</span>167<br>
<span>Scanlated:</span>SCANLATED by TCup<br>
</p></div>
</div></a>

<a href="/tankoubon/jkpworld" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/jkpworld.jpg">
</div>

<div class="info">
<div class="descript">John K. Pe-Ta's World</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>ジョン・Ｋ・ペー太の世界<br>
<span>Released:</span>2005/12/25<br>
<span>Retail:</span>¥1200<br>
<span>Publisher:</span>Toen Shobou<br>
<span>Pages:</span>198<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>

<a href="/tankoubon/explosion" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/explosion.jpg">
</div>

<div class="info">
<div class="descript">FAINTING in agony!! Explosion</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>MON絶!!エクスプロージョン<br>
<span>Released:</span>2006/08/25<br>
<span>Retail:</span>¥900<br>
<span>Publisher:</span>Toen Shobou<br>
<span>Pages:</span>198<br>
<span>Scanlated:</span>SCANLATED by SaHa<br>
</p></div>
</div></a>

</div>

<a href="javascript:toggleDiv('Coremag')" style="text-decoration: none"><h2 class="subtitle">Published by Core Magazine></a>
<p>Since Toen Shobo is out of buisness JKP's comics released by <a href="http://www.coremagazine.co.jp/comic/comic/jyon_k_peta/">Core Magazine</a> in the Hot Milk Comics line.</p>

<div id="Coremag" style="display: none;">

<a href="/tankoubon/license" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/license.jpg">
</div>

<div class="info">
<div class="descript">Quavery Fainting In Agony License</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>プルプル悶絶ライセンス<br>
<span>Released:</span>2007/02/28<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 234<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>198<br>
<span>Scanlated:</span>SCANLATED by DoujinMoe
</p></div>
</div></a>

<a href="/tankoubon/screamer" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/screamer.jpg">
</div>

<div class="info">
<div class="descript">Roar!! Fainting in Agony Screamer</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>轟け！！悶絶スクリーマー<br>
<span>Released:</span>2007/10/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 246<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>201<br>
<span>Scanlated:</span>SCANLATED by SaHa<br>
</p></div>
</div></a>

<a href="/tankoubon/curriculumplus" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/curriculum_plus.jpg">
</div>

<div class="info">
<div class="descript">Super Fainting in Agony Curriculum Plus</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>超悶絶カリキュラムプラス<br>
<span>Released:</span>2008/03/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 261<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>201<br>
<span>Scanlated:</span>SCANLATED by SaHa<br>
</p></div>
</div></a>

<a href="/tankoubon/balkan" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/balkan.jpg">
</div>

<div class="info">
<div class="descript">Exciting Fainting in Agony Balkan!!</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>トキメキ悶絶バルカン!!<br>
<span>Released:</span>2008/07/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 270<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>203<br>
<span>Scanlated:</span>SCANLATED by Yuribou<br>
</p></div>
</div></a>


<a href="/tankoubon/operationplus" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/operation_plus.jpg">
</div>

<div class="info">
<div class="descript">Intense!! Fainting in Agony Operation Plus</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>激！！悶絶オペレーションプラス<br>
<span>Released:</span>2009/01/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 281<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>203<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>


<a href="/tankoubon/monfest" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/mofesu.jpg">
</div>

<div class="info">
<div class="descript">Monfest XI</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>モンフェスXI(イレブン)<br>
<span>Released:</span>2009/07/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 297<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>206<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>

<a href="/tankoubon/feverplus" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/fever_plus.jpg">
</div>

<div class="info">
<div class="descript">Full Buxom!! Fainting in Agony Fever Plus</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>ムチムチ悶絶!!フィーバープラス<br>
<span>Released:</span>2009/12/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 311<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>196<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>


<a href="/tankoubon/megabitch" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/mega_bitch.jpg">
</div>

<div class="info">
<div class="descript">Super Fainting in Agony Megabitch</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>スーパーモンゼツメガビッチ<br>
<span>Released:</span>2010/04/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 319<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>198<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>


<a href="/tankoubon/explosionplus" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/explosion_plus.jpg">
</div>

<div class="info">
<div class="descript">FAINTING in agony!! Explosion Plus</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>MON絶!!エクスプロージョンプラス<br>
<span>Released:</span>2010/11/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 333<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>196<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>


<a href="/tankoubon/destroy" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/mon_destroy.jpg">
</div>

<div class="info">
<div class="descript">Search and Fainting in Agony Destroy</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>サーチ&悶絶デストロイ<br>
<span>Released:</span>2011/03/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 342<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>198<br>
<span>Scanlated:</span>-
</p></div>
</div></a>


<a href="/tankoubon/system" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/system.jpg">
</div>

<div class="info">
<div class="descript">Monzetsu System!</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>もんぜつ系!<br>
<span>Released:</span>2011/09/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 355<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>196<br>
<span>Scanlated:</span>SCANLATED by SaHa<br>
</p></div>
</div></a>


<a href="/tankoubon/caligulaplus" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/caligula_plus.jpg">
</div>

<div class="info">
<div class="descript">Fainting in Agony Caligula Machine Plus</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>悶絶カリギュラマシーンプラス<br>
<span>Released:</span>2011/12/10<br>
<span>Retail:</span>¥1050<br>
<span>Code:</span>Hot Milk Comics 365<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>194<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>

<a href="/tankoubon/miraclehole" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/kiseki_no_ana.jpg">
</div>

<div class="info">
<div class="descript">Miracle Hole</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>奇跡の穴<br>
<span>Released:</span>2012/11/30<br>
<span>Retail:</span>¥1155<br>
<span>Code:</span>Hot Milk Comics 387<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>240<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>

</div>

<a href="javascript:toggleDiv('Got')" style="text-decoration: none"><h2 class="subtitle">Published by GOT></a>

<div id="Got" style="display: none;">

<a href="/tankoubon/maison" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/maison.jpg">
</div>

<div class="info">
<div class="descript">Waku Waku Monzetsu Maison</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>わくわく悶絶めぞん<br>
<span>Released:</span>2015/04/25<br>
<span>Retail:</span>¥1080<br>
<span>Publisher:</span>GOT<br>
<span>Pages:</span>197<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>

<a href="/tankoubon/daigaku" style="text-decoration: none"><div class="tanko-stack tanko">

<div class="cover">
    <img class="img3" src="/assets/images/tank/daigaku.jpg">
</div>

<div class="info">
<div class="descript">Private Slug Holes Academy</div>
Volume Information</h3>
<p>
<span>Japanese Title:</span>私立ハメ穴学園<br>
<span>Released:</span>2016/06/08<br>
<span>Retail:</span>¥1080<br>
<span>Publisher:</span>Core Magazine<br>
<span>Pages:</span>197<br>
<span>Scanlated:</span>-<br>
</p></div>
</div></a>

</div>