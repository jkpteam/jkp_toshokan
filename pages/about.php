<div style='float:right'>
    <img src="/assets/images/about/jkpsignt.png"></div>
<div class="about">
<h2>About the Site</h2>
    <p>This website is just a fan project and there are absolutely no money involved in it. The creator of it has absolutely no connection with JKP or the publishers. It's purpose is to celebrate a well-known and popular adult manga artist and to present a complete list of his works.</p>
</div>

<div class="about">
<h2>Disclaimer</h2>
<ul>
  <li>This website is for adults only!</li>
  <li>All copyrights is reserved for their respective owners!</li>
  <li>Minimum viewing width is 640 pixels wide! On small screened mobiles it may not display correctly.</li>
    <li>This is a completely free and non-profit website and there are no cookies or any harmful code in it. Only HTML5, CSS3, JQUERY and PHP are used.</li>
  <li>You can use any information on this site as you please, but you cannot duplicate the design nor the layout anywhere!</li>
</ul>

<div class="about">
<h2>About JKP</h2>
    <div class="aboutpic"><img src="/assets/images/about/jkp.jpg"></div>
<p>John K. Pe-ta (ジョン・K・ペー太) or JKP for short is the pen name of an adult manga author. He is well known of his cross-section style as well of his extreme content and his clean artwork. He likes horror movies and a fan of John Carpenter's works, this is where his pen name comes from. He likes punk and metal music and love guns.</p>
    <p><a href="https://twitter.com/jkp55645"><img src="/assets/images/tw2.png" alt="JKP Twitter account" title="JKP Twitter account" style="width:50px;"></a> JKP's Official Twitter account</p>
</div>
