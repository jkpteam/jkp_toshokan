<?php
  require_once 'data.php';
  require_once 'templates.php';
   header("Content-type: text/html;charset=utf-8");

  $page = (
    count($_REQUEST) == 0
    ? 'news'
    : (
      array_key_exists('page', $_REQUEST)
      && $_REQUEST['page'] != ''
      ? rtrim($_REQUEST['page'], "/")
      : '404'
    )
  );

  function printmenujson() {
    global $page;
    global $menu;
    global $submenus;

    echo "window.currentPage = " . json_encode($page) . ";";
    echo "window.tabs = " . json_encode($menu) . ";";
    echo "window.navMenus = " . json_encode($submenus) . ";";
  }

  function printmenu()
  {
    echo "  <div id='mainmenu'>Welcome!</div>\n";
  }

  function printmenujs()
  {
    global $menu;

    print "\n    \$( function() {";
    print "\n      scrollToTop();";
    print "\n      new zlightbox('.nagyobb');";
    print "\n    });";
  }

  function printsubmenu()
  {
    global $page;
    global $submenus;

    foreach ($submenus as $subpage => $menu)
    {
			//$_pos = strpos($page,'_');
			//$subpage = $_pos !== false ? substr($page,0,$_pos) : $page;

			echo "  <div class='submenu' id='sub$subpage' style='display: none;'>\n";
			foreach ($menu as $id => $item)
			{
				$selclass = '';
				if ($page == $subpage."/".$id || strpos($page, $subpage."/".$id."/") === 0)
					$selclass = 'selected';

				$lnk = "/".$subpage."/";
				if ($item['link'] == '')
					$lnk .= $id;
				else
					$lnk = $item['link'];
				echo "    <a href='$lnk' class='submenuitem $selclass' id='menu$id'>".htmlspecialchars($item['name'])."</a>\n"; //<a href='/$lnk' --> it wasn't working right
			}
			echo "  </div>\n";
    }
  }

  function getFilename($page = 'news') {
    return $_SERVER["DOCUMENT_ROOT"]."/pages/".$page.".php";
  }

  function printcontent()
  {
    global $page;
    echo "<div id='page'>";
    $filename = getFilename($page);
    if (file_exists($filename)) {
      require_once $filename;
    } else {
      require_once getFilename('404');
    }
    echo "<div class='clear'></div></div>";
  }

?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>John K. Pe-Ta's Library - ジョン・Ｋ・ペー太の図書館</title>
  <link rel=stylesheet href="/assets/css/style.css">
  <link rel=stylesheet href="/assets/css/zlightbox-min.css">
  <script src="/assets/js/jquery-2.0.3.min.js"></script>
  <script src="/assets/js/zlightbox-min.js"></script>
  <script src="/assets/js/jkp.js"></script>
  <script src="/assets/js/navigation.js"></script>
  <script src="/assets/js/toggle.js"></script>
  <script><?php printmenujs(); ?></script>
  <script><?php printmenujson(); ?></script>
</head>
<body>
  <span id="topanchor"></span>
  <div id="contents"><a href="/news"><div id="toplnk"></div></a>
 <!--  Removed, wasn't working rith <?php printmenu(); ?> -->
    <div id="main">
      <div id="navigation"></div>
      <script>renderNavigation()</script>
      <?php printcontent(); ?>
      <a href="#topanchor" style= "text-decoration:none;"><div id="bottom">▲ &nbsp; &nbsp; ジョン・K・ペー太の図書館 - John K. Pe-Ta's Library 2008-2023 &nbsp; &nbsp; ▲<br>All Rights Belong to Their Respective Owners</div></a>
      <?php printsubmenu(); ?>
    </div>
  </div>
</body>
</html>