var mi = [];

var menuenter = function(menuid)
{
  menushow(menuid);
}

var menuleave = function(menuid)
{
  menuhide(menuid);
}

var menushow = function(menuid)
{
  if (mi[menuid] === true)
    return;

  var sub = $(menuid);
  sub.stop().show();
	var subitems = sub.children('.submenu > .submenuitem');
	for (var j = subitems.length - 1; j >= 0; --j)
	{
		var si = subitems.eq(j);
		//si[0].currentTop = si.position().top;
		si.stop().show(200);
	}

  mi[menuid] = true;
}

var menuhide = function(menuid)
{
  if (mi[menuid] === undefined)
    return;

  mi[menuid] = false;
  starthidetimer(menuid);
}

var starthidetimer = function(menuid)
{
  if (mi[menuid] === undefined)
    return;

  window.setTimeout(timedhide, 400, menuid);
}

var timedhide = function(menuid)
{
  if (mi[menuid] === undefined || mi[menuid] === true)
    return;

  var sub = $(menuid);
	var subitems = sub.children('.submenu > .submenuitem');
	for (var j = subitems.length - 1; j >= 0; --j)
	{
		var si = subitems.eq(j);
		//si[0].currentTop = si.position().top;
		si.hide(200);
	}

  //sub.hide();
  delete mi[menuid];
}

var scrollToTop = function()
{
  return $('[href="#topanchor"]').on("click", function(e) {
      e.preventDefault();
      e.stopPropagation();

      var href = $(this).attr("href");
      $('html, body').animate({
          scrollTop: $(href).offset().top
      }, 500);
  })
}