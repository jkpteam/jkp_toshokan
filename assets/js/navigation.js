let openedMenu

const noOp = () => {}
const pipe = (first, ...rest) => rest.reduce((pipeline, next) => next(pipeline), first())

const setOpenedMenu = id => () => openedMenu = id
const resetOpenedMenu = () => () => openedMenu = null

const closeMenu = id => id ? () => document.getElementById(id).style.display = 'none' : noOp
const openMenu = id => () => document.getElementById(id).style.display = 'block'

const toggleMenu = id => {
    openedMenu === id
    ? pipe(closeMenu(id), resetOpenedMenu())
    : pipe(closeMenu(openedMenu), openMenu(id), setOpenedMenu(id))
}

const getMenuItemLink = (linkRoot, path = '') => `/${linkRoot}/${path}`

const goToUrl = (name, url) => () => (
    history.go(
        history.pushState(history.state, name, url)
    )
)

const hasMenuLinks = menuItem => Object.keys(menuItem).length > 0

const Tab = ({ link, name, page }) => {
    const Tab = document.createElement('button')
    const { navMenus } = window

    Tab.className = 'tablinks'
    Tab.onclick = () => (
        hasMenuLinks(navMenus[page])
        ? toggleMenu(page)
        : pipe(closeMenu(openedMenu), goToUrl(name, getMenuItemLink(page)))
    )

    const text = document.createTextNode(name)

    Tab.appendChild(text)

    return Tab
}

const TabContainer = () => {
    const TabContainer = document.createElement('div')

    TabContainer.className = 'tab'

    const { tabs } = window

    Object.keys(tabs)
    .map(key => ({
        ...tabs[key],
        page: key,
    }))
    .map(Tab)
    .forEach(element => TabContainer.appendChild(element))

    return TabContainer
}

const MenuItemLink = ({ link, name }) => {
    const MenuItemLink = document.createElement('a')
    const text = document.createTextNode(name)

    MenuItemLink.className = 'MenuItemLink'
    MenuItemLink.href = link

    MenuItemLink.appendChild(text)

    return MenuItemLink
}

const MenuItem = childElement => {
    const MenuItem = document.createElement('li')

    MenuItem.className = 'MenuItem'

    MenuItem.appendChild(childElement)

    return MenuItem
}

const MenuItems = ({ linkRoot, menuItems }) => {
    const MenuItems = document.createElement('ul')

    MenuItems.className = 'MenuItems'

    Object.keys(menuItems)
    .map(key => ({
        ...menuItems[key],
        link: getMenuItemLink(linkRoot, key),
    }))
    .map(MenuItemLink)
    .map(MenuItem)
    .forEach(element => MenuItems.appendChild(element))

    return MenuItems
}

const Menu = ({ menuItems, page }) => {
    const Menu = document.createElement('div')

    Menu.className = 'Menu'
    Menu.id = page
    Menu.style.display = 'none'

    hasMenuLinks(menuItems)
    && (
        Menu.appendChild(
            MenuItems({
                linkRoot: page,
                menuItems,
            })
        )
    )

    return Menu
}

const MenuContainer = () => {
    const MenuContainer = document.createElement('div')

    MenuContainer.className = 'MenuContainer'

    const { navMenus } = window

    Object.keys(navMenus)
    .filter(key => key !== 'author')
    .map(key => ({
        menuItems: navMenus[key],
        page: key,
    }))
    .map(Menu)
    .forEach(element => MenuContainer.appendChild(element))

    return MenuContainer
}

const renderToDom = elements => {
    const navigation = document.getElementById('navigation')

    elements
    .forEach(element => navigation.appendChild(element))
}

const renderNavigation = () => (
    renderToDom([
        TabContainer(),
        MenuContainer(),
    ])
)
