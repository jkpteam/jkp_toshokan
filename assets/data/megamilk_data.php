<?php
    $contents = array(
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv01.png',
                'jp' => 'アタックメゾン３０３',
                'eng' => 'Attack House 303',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.01',
                'date' => '2010/06/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm01b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm01b.jpg',
                'alt' => 'Megamilk Volume 1 Cover',
                'img_title' => 'Megamilk Volume 1 Cover',
                'issue' => 'Volume 1',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc01.jpg',
                'jptext' => 'スパＩＶで初めてネット対戦をやりました。マイキャラは元じいちゃんです。',
            ),
        ),  
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv02.png',
                'jp' => '桃色ファイアーポテト',
                'eng' => 'Pink Fire Potato',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.02',
                'date' => '2010/07/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm02b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm02b.jpg',
                'alt' => 'Megamilk Volume 2 Cover',
                'img_title' => 'Megamilk Volume 2 Cover',
                'issue' => 'Volume 2',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc02.jpg',
                'jptext' => 'グンドウとレイの食事シーンでした。破の特典フィルム。',
            ),
        ),  
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv03.png',
                'jp' => 'RED HOT BEACH',
                'eng' => 'Red Hot Beach',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.03',
                'date' => '2010/08/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm03b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm03b.jpg',
                'alt' => 'Megamilk Volume 3 Cover',
                'img_title' => 'Megamilk Volume 3 Cover',
                'issue' => 'Volume 3',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc03.jpg',
                'jptext' => '電子タバコの「タバコ味」に納得できません、メンソールはそれっぽいのにぃ・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv04.png',
                'jp' => 'クラッシュオンザビーチ',
                'eng' => 'Crash on the Beach',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.04',
                'date' => '2010/09/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm04b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm04b.jpg',
                'alt' => 'Megamilk Volume 4 Cover',
                'img_title' => 'Megamilk Volume 4 Cover',
                'issue' => 'Volume 4',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc04.jpg',
                'jptext' => '飲みすぎました。色んな先生方や編集さん達に生意気な事を言ってしまったかも・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv05.png',
                'jp' => '真昼のエニグマヒーロー',
                'eng' => 'Midday\'s Enigmatic Hero',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.05',
                'date' => '2010/10/09',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm05b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm05b.jpg',
                'alt' => 'Megamilk Volume 5 Cover',
                'img_title' => 'Megamilk Volume 5 Cover',
                'issue' => 'Volume 5',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc05.jpg',
                'jptext' => '鉄騎の新作が出る!ｗｋｔｋが止まらない。',
            ),
        ),
        array(
		    'tankobon' => 'Monzetsu System!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv06.png',
                'jp' => 'スーパーストマックダウン',
                'eng' => 'Super Stomach Down',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.06',
                'date' => '2010/11/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm06b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm06b.jpg',
                'alt' => 'Megamilk Volume 6 Cover',
                'img_title' => 'Megamilk Volume 6 Cover',
                'issue' => 'Volume 6',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc06.jpg',
                'jptext' => '昔の単行本が色々プラスされて発売されてます。ネタエロマンガとして是非!!',
            ),
        ),
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv08.png',
                'jp' => '今日からエクスターミネート',
                'eng' => 'Exterminator from today!',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.08',
                'date' => '2011/01/08',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm08b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm08b.jpg',
                'alt' => 'Megamilk Volume 8 Cover',
                'img_title' => 'Megamilk Volume 8 Cover',
                'issue' => 'Volume 8',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc08.jpg',
                'jptext' => '年末に色々な人に迷惑をかけてしまいました。心からおわびします・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Monzetsu System!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv09.png',
                'jp' => 'ギブミーピッグ',
                'eng' => 'Give Me Pig',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.09',
                'date' => '2011/02/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm09b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm09b.jpg',
                'alt' => 'Megamilk Volume 9 Cover',
                'img_title' => 'Megamilk Volume 9 Cover',
                'issue' => 'Volume 9',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc09.png',
                'jptext' => 'ボトムズ、００、グレン、ギアス・・・PSPを買わねば！！けど テレビ出力でやる予定ですスパロボ。',
            ),
        ),
        array(
		    'tankobon' => 'Monzetsu System!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv10.png',
                'jp' => '暁にねじれて',
                'eng' => 'Twisted at Dawn',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.10',
                'date' => '2011/03/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm10b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm10b.jpg',
                'alt' => 'Megamilk Volume 10 Cover',
                'img_title' => 'Megamilk Volume 10 Cover',
                'issue' => 'Volume 10',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc10.png',
                'jptext' => '新しい単行本が発売になるます。内容はいつも通りですがヨロシクです。',
            ),
        ),
        array(
		    'tankobon' => 'Monzetsu System!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv11.png',
                'jp' => '新百合地獄',
                'eng' => 'New Yuri Hell',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.11',
                'date' => '2011/04/09',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm11b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm11b.jpg',
                'alt' => 'Megamilk Volume 11 Cover',
                'img_title' => 'Megamilk Volume 11 Cover',
                'issue' => 'Volume 11',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc11.png',
                'jptext' => '仕事場のロボット軍団がほぼ壊滅・・・復旧には時間がかかりそうです。',
            ),
		),
        array(
		    'tankobon' => 'Monzetsu System!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv12.png',
                'jp' => 'ハイパーコントーション',
                'eng' => 'Hyper Contortion',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.12',
                'date' => '2011/05/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm12b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm12b.jpg',
                'alt' => 'Megamilk Volume 12 Cover',
                'img_title' => 'Megamilk Volume 12 Cover',
                'issue' => 'Volume 12',
                'artworkby' => 'Amaori Tatsuki',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc12.png',
                'jptext' => 'ゼロにケンカ売るカミナ、これだからスパロボは面白い。',
            ),
		),
        array(
		    'tankobon' => 'Monzetsu System!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv13.png',
                'jp' => '卒業禁書',
                'eng' => 'Graduation Forbidden Literature',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.13',
                'date' => '2011/06/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm13b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm13b.jpg',
                'alt' => 'Megamilk Volume 13 Cover',
                'img_title' => 'Megamilk Volume 13 Cover',
                'issue' => 'Volume 13',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc13.png',
                'jptext' => '長年使っていた携帯灰皿が壊れました。禁煙しろと言うことでしょうか。',
            ),
		),
        array(
		    'tankobon' => 'Monzetsu System!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv14.png',
                'jp' => 'ブルータル23時',
                'eng' => 'Brutal 23:00',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.14',
                'date' => '2011/07/08',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm14b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm14b.jpg',
                'alt' => 'Megamilk Volume 14 Cover',
                'img_title' => 'Megamilk Volume 14 Cover',
                'issue' => 'Volume 14',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc14.png',
                'jptext' => '１８年ぶりにプラモを作りました。最近の出来はすごいですね',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv15.png',
                'jp' => '常夏デスフィッシュ',
                'eng' => 'Neverending Summer Deathfish',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.15',
                'date' => '2011/08/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm15b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm15b.jpg',
                'alt' => 'Megamilk Volume 15 Cover',
                'img_title' => 'Megamilk Volume 15 Cover',
                'issue' => 'Volume 15',
                'artworkby' => 'Kotoyoshi Yumisuke',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc15.png',
                'jptext' => 'なんか色々限界です。。。痩れました、スイマセン。',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv16.png',
                'jp' => 'ラッキーサマーレッスン',
                'eng' => 'Lucky Summer Lesson',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.16',
                'date' => '2011/09/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm16b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm16b.jpg',
                'alt' => 'Megamilk Volume 16 Cover',
                'img_title' => 'Megamilk Volume 16 Cover',
                'issue' => 'Volume 16',
                'artworkby' => 'Inuhoshi',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc16.png',
                'jptext' => 'やってはならない過ちを二回もやってしまいました。',
            ),
		),
        array(
		    'tankobon' => 'Fainting in Agony Caligula Machine Plus',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv18.jpg',
                'jp' => 'レクチャーエブン',
                'eng' => 'Lecture on Sex for Dohtei!!!',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.18',
                'date' => '2011/11/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm18b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm18b.jpg',
                'alt' => 'Megamilk Volume 18 Cover',
                'img_title' => 'Megamilk Volume 18 Cover',
                'issue' => 'Volume 18',
                'artworkby' => 'Hijiri Tsukasa',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc18.png',
                'jptext' => 'とりあえず毎週ガンダムAGEを観てます。',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv20.png',
                'jp' => 'スカーな彼女',
                'eng' => 'The Girl with the Scar',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.20',
                'date' => '2012/01/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm20b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm20b.jpg',
                'alt' => 'Megamilk Volume 20 Cover',
                'img_title' => 'Megamilk Volume 20 Cover',
                'issue' => 'Volume 20',
                'artworkby' => 'Ashisyun (芦俊)',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc20.png',
                'jptext' => '今年の目標は空腹ガマンしない事です。',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv21.png',
                'jp' => '第一回ミスデストロイ',
                'eng' => 'First time Miss Destroy',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.21',
                'date' => '2012/02/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm21b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm21b.jpg',
                'alt' => 'Megamilk Volume 21 Cover',
                'img_title' => 'Megamilk Volume 21 Cover',
                'issue' => 'Volume 21',
                'artworkby' => 'Nanase Meruchi',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc21.png',
                'jptext' => '機械オンチの自分にはデジタル化 は苦難の道です。',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv22.png',
                'jp' => 'L・S・D！ ラッキー先生ダイナマイト',
                'eng' => 'LSD! Lucky Sensei Dynamite',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.22',
                'date' => '2012/03/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm22b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm22b.jpg',
                'alt' => 'Megamilk Volume 22 Cover',
                'img_title' => 'Megamilk Volume 22 Cover',
                'issue' => 'Volume 22',
                'artworkby' => 'Mozuya Murasaki',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc22.png',
                'jptext' => '先日トイレで気絶しました。アxx顔ばかり描いたバツでしょうかxx CHECK NEEDED!!',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv23.png',
                'jp' => 'はみ出す二人',
                'eng' => 'Two people just hanging out',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.23',
                'date' => '2012/04/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm23b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm23b.jpg',
                'alt' => 'Megamilk Volume 23 Cover',
                'img_title' => 'Megamilk Volume 23 Cover',
                'issue' => 'Volume 23',
                'artworkby' => 'Ashi Shun',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc23.png',
                'jptext' => 'スパロボZ再世篇がたのしみでしょうがないです。',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv24.png',
                'jp' => '水色チョコレート',
                'eng' => 'Light blue Chocolate',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.24',
                'date' => '2012/05/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm24b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm24b.jpg',
                'alt' => 'Megamilk Volume 24 Cover',
                'img_title' => 'Megamilk Volume 24 Cover',
                'issue' => 'Volume 24',
                'artworkby' => 'CARNELIAN',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc24.png',
                'jptext' => 'スパロボをコソコソ進めています、今の所エースはカレンです。',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv25.png',
                'jp' => 'サマーソルト30L',
                'eng' => 'Summer Assault 30 liters',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.25',
                'date' => '2012/06/08',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm25b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm25b.jpg',
                'alt' => 'Megamilk Volume 25 Cover',
                'img_title' => 'Megamilk Volume 25 Cover',
                'issue' => 'Volume 25',
                'artworkby' => 'Hayashiya Shizuru (林家志弦)',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc25.png',
                'jptext' => '新しいパッチが来たので、またスカイリムを旅してます。',
            ),
		),
        array(
		    'tankobon' => 'Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megamilk/ch/mmv26.png',
                'jp' => '桃色Bath Death',
                'eng' => 'Pink Bath Death',
            ),
            'magazine' => array(
                'title' => 'Megamilk Vol.26',
                'date' => '2012/07/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megamilk/mm26b.jpg"',
                'smallimage' => 'mangalist/megamilk/mm26b.jpg',
                'alt' => 'Megamilk Volume 26 Cover',
                'img_title' => 'Megamilk Volume 26 Cover',
                'issue' => 'Volume 26',
                'artworkby' => 'Shirota Dai',
            ),
            'comment' => array(
                'cimg' => 'megamilk/comment/mmc26.png',
                'jptext' => '海外レビューは散々な重鉄騎ですが突撃したいと思います。',
            ),
		),
    );
?>