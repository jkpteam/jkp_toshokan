<?php
    $contents = array(
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2018_09.jpg',
                'jp' => 'スーパーホール伊加瀬さん',
                'eng' => 'Super Hall Mr. Ise',
            ),
            'magazine' => array(
                'title' => 'Masyo 2018.09',
                'date' => '2018/07/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2018_09b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2018_09.jpg',
                'alt' => 'COMIC Masyo Volume 2018/09 Cover',
                'img_title' => 'COMIC Masyo Volume 2018/09 Cover',
                'issue' => '2018/09',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2018_09.png',
                'jptext' => 'エロマンガしか描かけません が楽しんで もらえらば 光栄です',
            ),
        ),  
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2019_02.jpg',
                'jp' => '色仕掛けの完熟オレンジ',
                'eng' => 'The Ripe Orange Venus Fly Trap',
            ),
            'magazine' => array(
                'title' => 'Masyo 2019.02',
                'date' => '2019/01/02',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2019_02b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2019_02.jpg',
                'alt' => 'COMIC Masyo Volume 2019/02 Cover',
                'img_title' => 'COMIC Masyo Volume 2019/02 Cover',
                'issue' => '2019/02',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2019_02.png',
                'jptext' => '皆様よいお年を、そして来年もよろしくお願いします。',
            ),
        ),  
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2019_04.jpg',
                'jp' => '被験体A子',
                'eng' => 'Child subject A',
            ),
            'magazine' => array(
                'title' => 'Masyo 2019.04',
                'date' => '2019/02/23',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2019_04b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2019_04.jpg',
                'alt' => 'COMIC Masyo Volume 2019/04 Cover',
                'img_title' => 'COMIC Masyo Volume 2019/04 Cover',
                'issue' => '2019/04',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2019_04.png',
                'jptext' => 'マショウさんでは本年度初掲載になります、今年もよろしくお願いします。',
            ),
        ),
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2019_09.jpg',
                'jp' => '常夏ジェットストリーム',
                'eng' => 'Endless Summer JetStream',
            ),
            'magazine' => array(
                'title' => 'Masyo 2019.09',
                'date' => '2019/07/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2019_09b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2019_09.jpg',
                'alt' => 'COMIC Masyo Volume 2019/09 Cover',
                'img_title' => 'COMIC Masyo Volume 2019/09 Cover',
                'issue' => '2019/09',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2019_09.png',
                'jptext' => '今回は夏らしいマンガが描けたと思います。',
            ),
        ),
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2019_12.jpg',
                'jp' => '新米くノ一忍崎さん',
                'eng' => 'Mr. Shinobu Kunoichi Shinozaki',
            ),
            'magazine' => array(
                'title' => 'Masyo 2019.12',
                'date' => '2019/10/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2019_12b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2019_12.jpg',
                'alt' => 'COMIC Masyo Volume 2019/12 Cover',
                'img_title' => 'COMIC Masyo Volume 2019/12 Cover',
                'issue' => '2019/12',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2019_12.png',
                'jptext' => '今回のは初めてちゃんとマンガで描いたくノ一物です。',
            ),
        ),
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2020_04.jpg',
                'jp' => 'おもらしヤンキー鬼原さん',
                'eng' => 'Peeing Yankee Onihara',
            ),
            'magazine' => array(
                'title' => 'Masyo 2020.04',
                'date' => '2020/02/22',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2020_04b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2020_04.jpg',
                'alt' => 'COMIC Masyo Volume 2020/04 Cover',
                'img_title' => 'COMIC Masyo Volume 2020/04 Cover',
                'issue' => '2020/04',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2020_04.png',
                'jptext' => '最近オカルト系のサイトで怖い話ばかり見てたのでこのようなストーリになりました。',
            ),
        ),
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2020_07.jpg',
                'jp' => '歴戦の女戦士と僕の旅',
                'eng' => 'A veteran female warrior and my journey',
            ),
            'magazine' => array(
                'title' => 'Masyo 2020.07',
                'date' => '2020/05/23',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2020_07.jpg',
                'smallimage' => 'mangalist/masyo/masyo2020_07.jpg',
                'alt' => 'COMIC Masyo Volume 2020/07 Cover',
                'img_title' => 'COMIC Masyo Volume 2020/07 Cover',
                'issue' => '2020/07',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2020_07.png',
                'jptext' => '初めてのファンタジー物なので新鮮な気持ちで描くことができました。',
            ),
        ),
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2020_10.jpg',
                'jp' => '歴戦の女戦士と僕の旅 ２話',
                'eng' => 'A veteran female warrior and my journey Part #2',
            ),
            'magazine' => array(
                'title' => 'Masyo 2020.10',
                'date' => '2020/08/23',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2020_10b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2020_10.jpg',
                'alt' => 'COMIC Masyo Volume 2020/10 Cover',
                'img_title' => 'COMIC Masyo Volume 2020/10 Cover',
                'issue' => '2020/10',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2020_10.png',
                'jptext' => '今回は普段は描かないファンタジー物のまさかの続きです、やり応えのある仕事でした。',
            ),
        ),
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2021_01.jpg',
                'jp' => '学園フェイタリティ',
                'eng' => 'Fatality Academy',
            ),
            'magazine' => array(
                'title' => 'Masyo 2021.01',
                'date' => '2020/12/02',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2021_01b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2021_01.jpg',
                'alt' => 'COMIC Masyo Volume 2021/01 Cover',
                'img_title' => 'COMIC Masyo Volume 2021/01 Cover',
                'issue' => '2021/01',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2021_01.png',
                'jptext' => '定期的に訪れるキャットファイト描きたい病の発作が今回来てしまいました。',
            ),
        ),
        array(
		    'tankobon' => 'Meat Hole Full Package',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2021_03.jpg',
                'jp' => '肉穴フルパッケージ',
                'eng' => 'Meat Hole Full Package',
            ),
            'magazine' => array(
                'title' => 'Masyo 2021.03',
                'date' => '2021/01/22',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2021_03b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2021_03.jpg',
                'alt' => 'COMIC Masyo Volume 2021/03 Cover',
                'img_title' => 'COMIC Masyo Volume 2021/03 Cover',
                'issue' => '2021/03',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2021_03.png',
                'jptext' => '三和出版さんからの初の単行本「肉穴フルパッケージ」いよいよ発売です。',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2021_05.jpg',
                'jp' => '歴戦の女戦士と僕の旅 3話',
                'eng' => 'A veteran female warrior and my journey Part #3',
            ),
            'magazine' => array(
                'title' => 'Masyo 2021.05',
                'date' => '2021/03/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2021_05b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2021_05.jpg',
                'alt' => 'COMIC Masyo Volume 2021/05 Cover',
                'img_title' => 'COMIC Masyo Volume 2021/05 Cover',
                'issue' => '2021/05',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2021_05.png',
                'jptext' => '今回はファンタジー世界らしいファンタジーなエロマンガになったとおもいます。',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2021_08.jpg',
                'jp' => 'コスプレヤンキー鬼原さん',
                'eng' => 'Cosplay Yankee Onihara-san',
            ),
            'magazine' => array(
                'title' => 'Masyo 2021.08',
                'date' => '2021/07/02',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2021_08b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2021_08.jpg',
                'alt' => 'COMIC Masyo Volume 2021/08 Cover',
                'img_title' => 'COMIC Masyo Volume 2021/08 Cover',
                'issue' => '2021/08',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2021_08.png',
                'jptext' => '今回のヒロインは以前登場させたキャラですが初めて見る方にも違和感無いよう作りました。',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2021_11.jpg',
                'jp' => '田舎でハメよう',
                'eng' => 'Let\'s fuck in the country',
            ),
            'magazine' => array(
                'title' => 'Masyo 2021.11',
                'date' => '2021/10/02',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2021_11b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2021_11.jpg',
                'alt' => 'COMIC Masyo Volume 2021/11 Cover',
                'img_title' => 'COMIC Masyo Volume 2021/11 Cover',
                'issue' => '2021/11',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2021_11.png',
                'jptext' => '今回は夏らしいエロ漫画が描けたと思います',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2022_02.jpg',
                'jp' => 'もっと田舎でハメよう',
                'eng' => 'Let\'s fuck in the country more',
            ),
            'magazine' => array(
                'title' => 'Masyo 2022.02',
                'date' => '2022/01/02',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2022_02b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2022_02.jpg',
                'alt' => 'COMIC Masyo Volume 2022/02 Cover',
                'img_title' => 'COMIC Masyo Volume 2022/02 Cover',
                'issue' => '2022/02',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2022_02.png',
                'jptext' => '真冬なのに夏が舞台のマンガですが楽しんでもらえれば幸いです。',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2022_05.jpg',
                'jp' => '歴戦の女戦士と僕の旅 4話',
                'eng' => 'A veteran female warrior and my journey Part #4',
            ),
            'magazine' => array(
                'title' => 'Masyo 2022.05',
                'date' => '2022/03/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2022_05b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2022_05.jpg',
                'alt' => 'COMIC Masyo Volume 2022/05 Cover',
                'img_title' => 'COMIC Masyo Volume 2022/05 Cover',
                'issue' => '2022/05',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2022_05.png',
                'jptext' => '普段あまり描かない体型の女子は作業が新鮮でした。',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2022_09.jpg',
                'jp' => '女教師ビリビリ伝説',
                'eng' => 'Legend of the Female Teacher',
            ),
            'magazine' => array(
                'title' => 'Masyo 2022.09',
                'date' => '2022/06/22',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2022_09b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2022_09.jpg',
                'alt' => 'COMIC Masyo Volume 2022/09 Cover',
                'img_title' => 'COMIC Masyo Volume 2022/09 Cover',
                'issue' => '2022/09',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2022_09.png',
                'jptext' => '真夏にピッタリなエロ漫画が描けたと思います',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2022_12.jpg',
                'jp' => '歴戦の女戦士と僕の旅 5話',
                'eng' => 'A veteran female warrior and my journey Part #5',
            ),
            'magazine' => array(
                'title' => 'Masyo 2022.12',
                'date' => '2022/10/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2022_12b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2022_12.jpg',
                'alt' => 'COMIC Masyo Volume 2022/12 Cover',
                'img_title' => 'COMIC Masyo Volume 2022/12 Cover',
                'issue' => '2022/12',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2022_12.png',
                'jptext' => '生まれ変わったらロボットのパイロットか触手になりたい、 と昔思っていたことがありました。',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/masyo/ch/masyo2023_03.jpg',
                'jp' => '怖がりヤンキー鬼原さん',
                'eng' => 'Fearful Yankee Kihara-san',
            ),
            'magazine' => array(
                'title' => 'Masyo 2023.03',
                'date' => '2023/03/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/masyo/masyo2023_03b.jpg',
                'smallimage' => 'mangalist/masyo/masyo2023_03.jpg',
                'alt' => 'COMIC Masyo Volume 2023/03 Cover',
                'img_title' => 'COMIC Masyo Volume 2023/03 Cover',
                'issue' => '2023/03',
                'artworkby' => 'Urushihara Satoshi',
            ),
            'comment' => array(
                'cimg' => 'masyo/comment/masyo2023_03.png',
                'jptext' => '今年もるド変態なエロ漫画を描きたいと思います。',
            ),
        ),
    );
?>