<?php
    $contents = array(
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv28.png',
                'jp' => '爆裂温泉桃色ファイヤー!!',
                'eng' => 'Exploding Hot Spring Pink Fire!!',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.28',
                'date' => '2006/01/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp28b.jpg',
                'smallimage' => 'mangalist/megaplus/mp28b.jpg',
                'alt' => 'Megaplus Volume 28 Cover',
                'img_title' => 'Megaplus Volume 28 Cover',
                'issue' => 'Volume 28',
                'artworkby' => 'Hiyo Hiyo',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc28.jpg',
                'jptext' => '寒いんで温泉モノを描きました。けど、熱い風呂は苦手です。',
            ),
        ),  
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv30.png',
                'jp' => '欲情ジェット戦線!!',
                'eng' => 'Passion Jet Front',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.30',
                'date' => '2006/03/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp30b.jpg',
                'smallimage' => 'mangalist/megaplus/mp30b.jpg',
                'alt' => 'Megaplus Volume 30 Cover',
                'img_title' => 'Megaplus Volume 30 Cover',
                'issue' => 'Volume 30',
                'artworkby' => 'Hiyo Hiyo',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc30.jpg',
                'jptext' => 'スノボに行ってきました、スポーツするのは久しぶりでした。',
            ),
        ),  
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv32.png',
                'jp' => '悶絶ゴールデンクラッシャー',
                'eng' => 'Monzetsu Golden Crusher!',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.32',
                'date' => '2006/05/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp32b.jpg',
                'smallimage' => 'mangalist/megaplus/mp32b.jpg',
                'alt' => 'Megaplus Volume 32 Cover',
                'img_title' => 'Megaplus Volume 32 Cover',
                'issue' => 'Volume 32',
                'artworkby' => 'Hiyo Hiyo',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc32.jpg',
                'jptext' => 'ＡＣＥ２に夢中です。愛機はエステバリスのヒカル機です。',
            ),
        ),
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv34.png',
                'jp' => '必殺 桃尻バーニング!!',
                'eng' => 'Certain Kill Peach Butt Burning!!',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.34',
                'date' => '2006/07/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp34b.jpg',
                'smallimage' => 'mangalist/megaplus/mp34b.jpg',
                'alt' => 'Megaplus Volume 34 Cover',
                'img_title' => 'Megaplus Volume 34 Cover',
                'issue' => 'Volume 34',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc34.jpg',
                'jptext' => 'スプリンターセルを３作たて続けに購入、面白すぎる。',
            ),
        ),
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv35.png',
                'jp' => '水色暴走バタフライ',
                'eng' => 'Light-Blue Delusion Butterfly',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.35',
                'date' => '2006/08/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp35b.jpg',
                'smallimage' => 'mangalist/megaplus/mp35b.jpg',
                'alt' => 'Megaplus Volume 35 Cover',
                'img_title' => 'Megaplus Volume 35 Cover',
                'issue' => 'Volume 35',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc35.jpg',
                'jptext' => '映画「サイレント・ヒル」を観ました。1作目（ＰＳ版）の再現度がハンバ(パ?)じゃない!良質のホラー映画をひさしぶりに見れました。',
            ),
        ),
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv36.png',
                'jp' => 'LoveRockファッキンライブ!!',
                'eng' => 'Love Rock Fucking Live!!',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.36',
                'date' => '2006/09/09',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp36b.jpg',
                'smallimage' => 'mangalist/megaplus/mp36b.jpg',
                'alt' => 'Megaplus Volume 36 Cover',
                'img_title' => 'Megaplus Volume 36 Cover',
                'issue' => 'Volume 36',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc36.jpg',
                'jptext' => 'ゴキブリが死ぬほど苦手です。カラー原稿はもっと苦手です。',
            ),
        ),
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv37.png',
                'jp' => '超絶!! 放課後レボリューション',
                'eng' => 'Transcendence!! After School Revolution',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.37',
                'date' => '2006/10/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp37b.jpg',
                'smallimage' => 'mangalist/megaplus/mp37b.jpg',
                'alt' => 'Megaplus Volume 37 Cover',
                'img_title' => 'Megaplus Volume 37 Cover',
                'issue' => 'Volume 37',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc37.jpg',
                'jptext' => 'カジュアリティーズのニューアルバムが最高!!テンションとヤル気が上がります。',
            ),
        ),
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv38.png',
                'jp' => 'オールナイトロング',
                'eng' => 'All Night Long	',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.38',
                'date' => '2006/11/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp38b.jpg',
                'smallimage' => 'mangalist/megaplus/mp38b.jpg',
                'alt' => 'Megaplus Volume 38 Cover',
                'img_title' => 'Megaplus Volume 38 Cover',
                'issue' => 'Volume 38',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc38.jpg',
                'jptext' => 'ＰＳ３高いな～～、けどガンダムはやりたい・・・迷うなァ・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv39.png',
                'jp' => 'アートないけにえ',
                'eng' => 'Artistic Sacrifice',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.39',
                'date' => '2006/12/09',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp39b.jpg',
                'smallimage' => 'mangalist/megaplus/mp39b.jpg',
                'alt' => 'Megaplus Volume 39 Cover',
                'img_title' => 'Megaplus Volume 39 Cover',
                'issue' => 'Volume 39',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc39.jpg',
                'jptext' => '町野変丸先生と飲んだ、楽しい夜でした。',
            ),
        ),
        array(
		    'tankobon' => 'Quavery Fainting In Agony License',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv40.png',
                'jp' => 'スーパー鬼畜パーク!!',
                'eng' => 'Super Brute Park!!',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.40',
                'date' => '2007/01/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp40b.jpg',
                'smallimage' => 'mangalist/megaplus/mp40b.jpg',
                'alt' => 'Megaplus Volume 40 Cover',
                'img_title' => 'Megaplus Volume 40 Cover',
                'issue' => 'Volume 40',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc40.jpg',
                'jptext' => '新年そうそうキモい漫画でスイマセン☆',
            ),
        ),
        array(
		    'tankobon' => 'Roar!! Fainting in Agony Screamer',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv41.png',
                'jp' => '雪崩式サンダーロード',
                'eng' => 'Avalanche Thunder Road',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.41',
                'date' => '2007/02/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp41b.jpg',
                'smallimage' => 'mangalist/megaplus/mp41b.jpg',
                'alt' => 'Megaplus Volume 41 Cover',
                'img_title' => 'Megaplus Volume 41 Cover',
                'issue' => 'Volume 41',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc41.jpg',
                'jptext' => '今月末に単行本が出ます。女の子のいろんな穴から色々なモノが出る一冊なんです。',
            ),
        ),
        array(
		    'tankobon' => 'Roar!! Fainting in Agony Screamer',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv42.png',
                'jp' => '欲求オーバードーズ',
                'eng' => 'Lust Overdose',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.42',
                'date' => '2007/03/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp42b.jpg',
                'smallimage' => 'mangalist/megaplus/mp42b.jpg',
                'alt' => 'Megaplus Volume 42 Cover',
                'img_title' => 'Megaplus Volume 42 Cover',
                'issue' => 'Volume 42',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc42.jpg',
                'jptext' => 'グランドセフトオートの「ＳＡ」をクリア、スーツに日本刀で街を闊歩しました。',
            ),
        ),
        array(
		    'tankobon' => 'Roar!! Fainting in Agony Screamer',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv43.png',
                'jp' => '十戒のマッシブ!!',
                'eng' => 'Massive Ten Commandments!!',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.43',
                'date' => '2007/04/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp43b.jpg',
                'smallimage' => 'mangalist/megaplus/mp43b.jpg',
                'alt' => 'Megaplus Volume 43 Cover',
                'img_title' => 'Megaplus Volume 43 Cover',
                'issue' => 'Volume 43',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc43.jpg',
                'jptext' => 'サイン会をやりました。来てくれたみなさんホントにありがとうございました。',
            ),
        ),
        array(
		    'tankobon' => 'Roar!! Fainting in Agony Screamer',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv44.png',
                'jp' => '夕暮れスナッピングタートル',
                'eng' => 'Twilight Snapping Turtle',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.44',
                'date' => '2007/05/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp44b.jpg',
                'smallimage' => 'mangalist/megaplus/mp44b.jpg',
                'alt' => 'Megaplus Volume 44 Cover',
                'img_title' => 'Megaplus Volume 44 Cover',
                'issue' => 'Volume 44',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc44.jpg',
                'jptext' => '世間の波に反してＸＢＯＸ３６０購入、グロゲーム多し♡',
            ),
        ),
        array(
		    'tankobon' => 'Roar!! Fainting in Agony Screamer',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv45.png',
                'jp' => 'スニーキング爆裂ミッション！',
                'eng' => 'Sneaking Bakuretsu Misson',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.45',
                'date' => '2007/06/08',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp45b.jpg',
                'smallimage' => 'mangalist/megaplus/mp45b.jpg',
                'alt' => 'Megaplus Volume 45 Cover',
                'img_title' => 'Megaplus Volume 45 Cover',
                'issue' => 'Volume 45',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc45.jpg',
                'jptext' => '購入したての３６０を修理に出す事()に。噂には聞いてたけど、こんなに早く近()くとは・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Roar!! Fainting in Agony Screamer',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv46.png',
                'jp' => 'ファイアースターター',
                'eng' => 'Fire Starter',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.46',
                'date' => '2007/07/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp46b.jpg',
                'smallimage' => 'mangalist/megaplus/mp46b.jpg',
                'alt' => 'Megaplus Volume 46 Cover',
                'img_title' => 'Megaplus Volume 46 Cover',
                'issue' => 'Volume 46',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc46.jpg',
                'jptext' => '尊敬する町野変丸先生の単行本に描きおろしマンガを描かせてもらいました。',
            ),
        ),
        array(
		    'tankobon' => 'Roar!! Fainting in Agony Screamer',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv47.png',
                'jp' => 'サマーライアット07\'',
                'eng' => 'Summer Riot \'07',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.47',
                'date' => '2007/08/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp47b.jpg',
                'smallimage' => 'mangalist/megaplus/mp47b.jpg',
                'alt' => 'Megaplus Volume 47 Cover',
                'img_title' => 'Megaplus Volume 47 Cover',
                'issue' => 'Volume 47',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc47.jpg',
                'jptext' => 'この号が出る頃にはオブリビオンの世界で冒険中のハズです。',
            ),
        ),
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv48.png',
                'jp' => 'アンダークラスヒーロー',
                'eng' => 'Under Class HERO',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.48',
                'date' => '2007/09/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp48b.jpg',
                'smallimage' => 'mangalist/megaplus/mp48b.jpg',
                'alt' => 'Megaplus Volume 48 Cover',
                'img_title' => 'Megaplus Volume 48 Cover',
                'issue' => 'Volume 48',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc48.jpg',
                'jptext' => 'トランスフォーマーに呪怨にエヴァ、今年の夏は豊作だ―。',
            ),
        ),
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv49.png',
                'jp' => '暴走ハイテンション!!',
                'eng' => 'The High-tension Crazy driving!!',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.49',
                'date' => '2007/10/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp49b.jpg',
                'smallimage' => 'mangalist/megaplus/mp49b.jpg',
                'alt' => 'Megaplus Volume 49 Cover',
                'img_title' => 'Megaplus Volume 49 Cover',
                'issue' => 'Volume 49',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc49.jpg',
                'jptext' => '新劇場版のラミエルの変形はホントにビビった、もう一回観に行こうかな・・・・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megaplus/ch/mpv50.png',
                'jp' => 'ユウダチファイヤー',
                'eng' => 'Sudden Fire!!',
            ),
            'magazine' => array(
                'title' => 'Megaplus Vol.50',
                'date' => '2007/11/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megaplus/mp50b.jpg',
                'smallimage' => 'mangalist/megaplus/mp50b.jpg',
                'alt' => 'Megaplus Volume 50 Cover',
                'img_title' => 'Megaplus Volume 50 Cover',
                'issue' => 'Volume 50',
                'artworkby' => 'ISAO',
            ),
            'comment' => array(
                'cimg' => 'megaplus/comment/cmpc50.jpg',
                'jptext' => '単行本が発売中です、プラス以外のマンガも載ってます。',
            ),
        ),
    );
?>