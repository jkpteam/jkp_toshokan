
<?php
    $contents = array(
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastorea/ch/msa2016_01c.jpg',
                'jp' => '変身スイッチ',
                'eng' => 'Henshin Switch',
            ),
            'magazine' => array(
                'title' => 'Hotmilk 2020 Vol.07',
                'date' => '2020.06.02',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/hotmilk/hotmilk202007.jpg',
                'smallimage' => 'mangalist/hotmilk/hotmilk202007.jpg',
                'alt' => 'Hotmilk Volume 2020/07 Cover',
                'img_title' => 'Hotmilk Volume 2020/07 Cover',
                'issue' => '2020.07',
                'artworkby' => 'Bota Mochito',
            ),
            ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/hotmilk/ch/hotmilk202211.jpg',
                'jp' => 'デュラハンな彼女',
                'eng' => 'Dullahan Girlfriend',
            ),
            'magazine' => array(
                'title' => 'Hotmilk 2022 Vol.11',
                'date' => '2022.10.05',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/hotmilk/hotmilk202211.jpg',
                'smallimage' => 'mangalist/hotmilk/hotmilk202211.jpg',
                'alt' => 'Hotmilk Volume 2022/11 Cover',
                'img_title' => 'Hotmilk Volume 2022/11 Cover',
                'issue' => '2022.11',
                'artworkby' => 'Kei Mizuryu',
            ),
            ),
    );
?>