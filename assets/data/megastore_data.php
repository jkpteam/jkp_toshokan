<?php
    $contents = array(
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastore/ch/megastore002.jpg',
                'jp' => '秘湯・ママ友温泉',
                'eng' => 'Secret Hot Spring - Mother Girlfriends Hot Spring',
            ),
            'magazine' => array(
                'title' => 'Megastore Vol. 002',
                'date' => '2022/12/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastore/megastore002b.jpg',
                'smallimage' => 'mangalist/megastore/megastore002.jpg',
                'alt' => 'COMIC Megastore Vol. 002 Cover',
                'img_title' => 'COMIC Megastore Vol. 002 Cover',
                'issue' => 'Vol. 002',
                'artworkby' => 'ebecus omom',
            ),
            'comment' => array(
                'cimg' => 'megastore/comment/megastore002.png',
                'jptext' => '',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastore/ch/megastore004.png',
                'jp' => 'おねえさんといっしょ',
                'eng' => 'Together with Sister',
            ),
            'magazine' => array(
                'title' => 'Megastore Vol. 004',
                'date' => '2023/04/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastore/megastore004b.png',
                'smallimage' => 'mangalist/megastore/megastore004.jpg',
                'alt' => 'COMIC Megastore Vol. 004 Cover',
                'img_title' => 'COMIC Megastore Vol. 004 Cover',
                'issue' => 'Vol. 004',
                'artworkby' => 'Kizuki Aruchu',
            ),
            'comment' => array(
                'cimg' => 'megastore/comment/megastore004.png',
                'jptext' => '',
            ),
        ), 
    );
?>