<?php
    $contents = array(
        array(
		    'tankobon' => 'Waku Waku Monzetsu Maison',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/wwos01.jpg',
                'jp' => 'わくわく one-sans 第一話',
                'eng' => 'Waku Waku one-sans Episode 1',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.58',
                'date' => '2014/02/07',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m058b.jpg',
                'smallimage' => 'mangalist/magnum/m058.jpg',
                'alt' => 'Kuriberon Cover Vol.58',
                'img_title' => 'Kuriberon Cover Vol.58',
                'issue' => 'Volume 58',
                'artworkby' => 'Millefeuille',
            ),
        ),
        array(
		    'tankobon' => 'Waku Waku Monzetsu Maison',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/wwos02.jpg',
                'jp' => 'わくわく one-sans 第二話',
                'eng' => 'Waku Waku one-sans Episode 2',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.60',
                'date' => '2014/04/04',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m060b.jpg',
                'smallimage' => 'mangalist/magnum/m060.jpg',
                'alt' => 'Kuriberon Cover Vol.60',
                'img_title' => 'Kuriberon Cover Vol.60',
                'issue' => 'Volume 60',
                'artworkby' => 'Yokaze Japan',
            ),
		),
        array(
		    'tankobon' => 'Waku Waku Monzetsu Maison',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/wwos03.jpg',
                'jp' => 'わくわく one-sans 第三話',
                'eng' => 'Waku Waku one-sans Episode 3',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.62',
                'date' => '2014/06/06',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m062b.jpg',
                'smallimage' => 'mangalist/magnum/m062.jpg',
                'alt' => 'Kuriberon Cover Vol.62',
                'img_title' => 'Kuriberon Cover Vol.62',
                'issue' => 'Volume 62',
                'artworkby' => 'Gustav',
            ),
		),
        array(
		    'tankobon' => 'Waku Waku Monzetsu Maison',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/wwos04.jpg',
                'jp' => 'わくわく one-sans 第四話',
                'eng' => 'Waku Waku one-sans Episode 4',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.64',
                'date' => '2014/08/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m064b.jpg',
                'smallimage' => 'mangalist/magnum/m064.jpg',
                'alt' => 'Kuriberon Cover Vol.64',
                'img_title' => 'Kuriberon Cover Vol.64',
                'issue' => 'Volume 64',
                'artworkby' => 'Hien',
            ),
		),
        array(
		    'tankobon' => 'Waku Waku Monzetsu Maison',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/wwos05.jpg',
                'jp' => 'わくわく one-sans 第五話',
                'eng' => 'Waku Waku one-sans Episode 5',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.66',
                'date' => '2014/08/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m066b.jpg',
                'smallimage' => 'mangalist/magnum/m066.jpg',
                'alt' => 'Kuriberon Cover Vol.66',
                'img_title' => 'Kuriberon Cover Vol.66',
                'issue' => 'Volume 66',
                'artworkby' => 'Yokoyama Michiru',
            ),
		),
        array(
		    'tankobon' => 'Waku Waku Monzetsu Maison',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/wwos06.jpg',
                'jp' => 'わくわく one-sans 第六話',
                'eng' => 'Waku Waku one-sans Episode 6',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.68',
                'date' => '2014/12/05',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m068b.jpg',
                'smallimage' => 'mangalist/magnum/m068.jpg',
                'alt' => 'Kuriberon Cover Vol.68',
                'img_title' => 'Kuriberon Cover Vol.68',
                'issue' => 'Volume 68',
                'artworkby' => 'Youkai Ankake',
            ),
		),
        array(
		    'tankobon' => 'Waku Waku Monzetsu Maison',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/wwos07.jpg',
                'jp' => 'わくわく one-sans 第七話',
                'eng' => 'Waku Waku one-sans Episode 7',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.70',
                'date' => '2015/02/06',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m070b.jpg',
                'smallimage' => 'mangalist/magnum/m070.jpg',
                'alt' => 'Kuriberon Cover Vol.70',
                'img_title' => 'Kuriberon Cover Vol.70',
                'issue' => 'Volume 70',
                'artworkby' => 'Moriyama Izumi',
            ),
		),
        array(
		    'tankobon' => 'Waku Waku Monzetsu Maison',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/wwos08.jpg',
                'jp' => 'わくわくone-sans 最終話',
                'eng' => 'Waku Waku one-sans Last Episode',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.72',
                'date' => '2015/04/03',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m072b.jpg',
                'smallimage' => 'mangalist/magnum/m072.jpg',
                'alt' => 'Kuriberon Cover Vol.72',
                'img_title' => 'Kuriberon Cover Vol.72',
                'issue' => 'Volume 72',
                'artworkby' => 'Warabi Yuuzou',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m75.jpg',
                'jp' => 'イサカ・ライアット',
                'eng' => 'Ithaca Riot',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.75',
                'date' => '2015/07/03',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m075b.jpg',
                'smallimage' => 'mangalist/magnum/m075.jpg',
                'alt' => 'Kuriberon Cover Vol.75',
                'img_title' => 'Kuriberon Cover Vol.75',
                'issue' => 'Volume 75',
                'artworkby' => 'Moriyama Izumi',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m78.jpg',
                'jp' => 'どんぶりマイスター ひめか',
                'eng' => 'Donburi Master Himeka',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.78',
                'date' => '2015/10/02',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m078b.jpg',
                'smallimage' => 'mangalist/magnum/m078.jpg',
                'alt' => 'Kuriberon Cover Vol.78',
                'img_title' => 'Kuriberon Cover Vol.78',
                'issue' => 'Volume 78',
                'artworkby' => 'Bai Asuka',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m82.jpg',
                'jp' => 'ブルーティシュバタフライ',
                'eng' => 'Brutish Butterfly',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.82',
                'date' => '2016/02/05',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m082b.jpg',
                'smallimage' => 'mangalist/magnum/m082.jpg',
                'alt' => 'Kuriberon Cover Vol.82',
                'img_title' => 'Kuriberon Cover Vol.82',
                'issue' => 'Volume 82',
                'artworkby' => 'Kawazuko Chouji',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m84.jpg',
                'jp' => '飲み干せ！！マッスルドリンク',
                'eng' => 'Drink up! ! Mass drink',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.84',
                'date' => '2016/04/08',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m084b.jpg',
                'smallimage' => 'mangalist/magnum/m084.jpg',
                'alt' => 'Kuriberon Cover Vol.84',
                'img_title' => 'Kuriberon Cover Vol.84',
                'issue' => 'Volume 84',
                'artworkby' => 'Piero',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m88.jpg',
                'jp' => 'ハイパーダブルコッキング☆',
                'eng' => 'Hyper Double King Star',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.88',
                'date' => '2016/08/05',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m088b.jpg',
                'smallimage' => 'mangalist/magnum/m088.jpg',
                'alt' => 'Kuriberon Cover Vol.88',
                'img_title' => 'Kuriberon Cover Vol.88',
                'issue' => 'Volume 88',
                'artworkby' => 'Uekan',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m92.jpg',
                'jp' => '私のペロ助',
                'eng' => 'My Perosuke',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.92',
                'date' => '2016/12/02',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m092b.jpg',
                'smallimage' => 'mangalist/magnum/m092.jpg',
                'alt' => 'Kuriberon Cover Vol.92',
                'img_title' => 'Kuriberon Cover Vol.92',
                'issue' => 'Volume 92',
                'artworkby' => 'Kekemotsu',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m95.jpg',
                'jp' => '営業・悶絶24時！',
                'eng' => 'Business Blackout 24 hours!',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.95',
                'date' => '2017/03/03',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m095b.jpg',
                'smallimage' => 'mangalist/magnum/m095.jpg',
                'alt' => 'Kuriberon Cover Vol.95',
                'img_title' => 'Kuriberon Cover Vol.95',
                'issue' => 'Volume 95',
                'artworkby' => 'Q',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m99.jpg',
                'jp' => '満点アクメティーチャー',
                'eng' => 'Manten Acme Teacher',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.99',
                'date' => '2017/07/07',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m099b.jpg',
                'smallimage' => 'mangalist/magnum/m099.jpg',
                'alt' => 'Kuriberon Cover Vol.99',
                'img_title' => 'Kuriberon Cover Vol.99',
                'issue' => 'Volume 99',
                'artworkby' => 'Tenchisouha',
            ),
		),
        array(
		    'tankobon' => 'Every hole feels good',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/magnum/ch/m103.jpg',
                'jp' => '彼女は僕ペット',
                'eng' => 'She is my Pet',
            ),
            'magazine' => array(
                'title' => 'Magnum Vol.103',
                'date' => '2017/11/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/magnum/m103b.jpg',
                'smallimage' => 'mangalist/magnum/m103.jpg',
                'alt' => 'Kuriberon Cover Vol.103',
                'img_title' => 'Kuriberon Cover Vol.103',
                'issue' => 'Volume 103',
                'artworkby' => 'Rokko',
            ),
		),
    );
?>