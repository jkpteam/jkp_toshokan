<?php
    $contents = array(
        array(
		    'tankobon' => 'Private Slug Holes Academy<br>Miracle Hole',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoreh/ch/msh108.png',
                'jp' => '青春ナパームストレッチ！！',
                'eng' => 'Spring Napalm Stretch',
            ),
            'magazine' => array(
                'title' => 'Megastore H Vol. 108',
                'date' => '2012.11',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoreh/msh108.jpg',
                'smallimage' => 'mangalist/megastoreh/msh108.jpg',
                'alt' => 'Megastore H Volume 108 Cover',
                'img_title' => 'Megastore H Volume 108 Cover',
                'issue' => 'Volume 108',
                'artworkby' => 'Toe Gojuu',
            ),
            'comment' => array(
                'cimg' => 'megastoreh/comment/msh108c.png',
                'jptext' => '多少時期はずれの内容になってしまいました。スイマセン・・・・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Private Slug Holes Academy<br>My lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoreh/ch/msh109.png',
                'jp' => '特濃ＬＯＵＤ牧場',
                'eng' => 'Shinano LOUD Ranch',
            ),
            'magazine' => array(
                'title' => 'Megastore H Vol. 109',
                'date' => '2013.01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoreh/msh109.jpg',
                'smallimage' => 'mangalist/megastoreh/msh109.jpg',
                'alt' => 'Megastore H Volume 109 Cover',
                'img_title' => 'Megastore H Volume 109 Cover',
                'issue' => 'Volume 109',
                'artworkby' => 'Toe Gojuu',
            ),
            'comment' => array(
                'cimg' => 'megastoreh/comment/msh109c.png',
                'jptext' => '単行本が発売されてます。たくさんの歪んだ妄想と愛がつまってます。',
            ),
        ),
        array(
		    'tankobon' => 'Private Slug Holes Academy<br>My lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoreh/ch/msh110.png',
                'jp' => '恋のタックル',
                'eng' => 'Love Tackle',
            ),
            'magazine' => array(
                'title' => 'Megastore H Vol. 110',
                'date' => '2013.03',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoreh/msh110.jpg',
                'smallimage' => 'mangalist/megastoreh/msh110.jpg',
                'alt' => 'Megastore H Volume 110 Cover',
                'img_title' => 'Megastore H Volume 110 Cover',
                'issue' => 'Volume 110',
                'artworkby' => 'Toe Gojuu',
            ),
            'comment' => array(
                'cimg' => 'megastoreh/comment/msh110c.png',
                'jptext' => '最近フィギュアを買わなくなりました。歳なのかなぁ・・・・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Private Slug Holes Academy<br>My lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoreh/ch/msh111.png',
                'jp' => '無敵合体センパイン',
                'eng' => 'Invincible Combination Senpain',
            ),
            'magazine' => array(
                'title' => 'Megastore H Vol. 111',
                'date' => '2013.05',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoreh/msh111.jpg',
                'smallimage' => 'mangalist/megastoreh/msh111.jpg',
                'alt' => 'Megastore H Volume 111 Cover',
                'img_title' => 'Megastore H Volume 111 Cover',
                'issue' => 'Volume 111',
                'artworkby' => 'Toe Gojuu',
            ),
            'comment' => array(
                'cimg' => 'megastoreh/comment/msh111c.png',
                'jptext' => '多くの作家さん達を目の前にして、メルアドの一つも交換できなかった小心者の自分が情けない・・・・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Private Slug Holes Academy<br>My lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoreh/ch/msh112.png',
                'jp' => '欲情の物体X',
                'eng' => 'Desire Matter X',
            ),
            'magazine' => array(
                'title' => 'Megastore H Vol. 112',
                'date' => '2013.07',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoreh/msh112.jpg',
                'smallimage' => 'mangalist/megastoreh/msh112.jpg',
                'alt' => 'Megastore H Volume 112 Cover',
                'img_title' => 'Megastore H Volume 112 Cover',
                'issue' => 'Volume 112',
                'artworkby' => 'Toe Gojuu',
            ),
            'comment' => array(
                'cimg' => 'megastoreh/comment/msh112c.png',
                'jptext' => '暖かくなってきたので、黒くて素早い奴におぴえる日々が来てしまいました。',
            ),
        ),
    );
?>