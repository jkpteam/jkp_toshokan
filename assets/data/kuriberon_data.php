<?php
    $contents = array(
        array(
		    'tankobon' => 'Fainting in Agony Opportunity',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb035.jpg',
                'jp' => '私立おちんちん学園',
                'eng' => 'Private Penis Gakuen',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.35',
                'date' => '2015/09/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb035.jpg',
                'smallimage' => 'mangalist/kuriberon/kb035.jpg',
                'alt' => 'Kuriberon Cover Vol.35',
                'img_title' => 'Kuriberon Cover Vol.35',
                'issue' => 'Volume 35',
                'artworkby' => 'Kouzuki Rio',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Opportunity',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb038.jpg',
                'jp' => '悶絶アイドル道',
                'eng' => 'Way of the Monzetsu Idol',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.38',
                'date' => '2015/12/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb038.jpg',
                'smallimage' => 'mangalist/kuriberon/kb038.jpg',
                'alt' => 'Kuriberon Cover Vol.38',
                'img_title' => 'Kuriberon Cover Vol.38',
                'issue' => 'Volume 38',
                'artworkby' => 'Machino Henmaru',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Opportunity',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb041.jpg',
                'jp' => '戦場の肉穴スナイパー',
                'eng' => 'Bitch Sniper at the Battlefield',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.41',
                'date' => '2016/03/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb041b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb041.jpg',
                'alt' => 'Kuriberon Cover Vol.41',
                'img_title' => 'Kuriberon Cover Vol.41',
                'issue' => 'Volume 41',
                'artworkby' => 'Hasuburo',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Opportunity',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb044.jpg',
                'jp' => '遊星からの透け透けメガネX',
                'eng' => 'X-Ray glasses from planet X',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.44',
                'date' => '2016/06/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb044b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb044.jpg',
                'alt' => 'Kuriberon Cover Vol.44',
                'img_title' => 'Kuriberon Cover Vol.44',
                'issue' => 'Volume 44',
                'artworkby' => 'Tsumagomi Izumo',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Opportunity',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb047.jpg',
                'jp' => '月夜のアイアンメイデン',
                'eng' => 'The moonlight of Iron Maiden',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.47',
                'date' => '2016/09/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb047b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb047.jpg',
                'alt' => 'Kuriberon Cover Vol.47',
                'img_title' => 'Kuriberon Cover Vol.47',
                'issue' => 'Volume 47',
                'artworkby' => 'Sanjou Tomomi',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Opportunity',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb049.jpg',
                'jp' => '狂気のアイちゃん',
                'eng' => 'Ai-cha\'s madness',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.49',
                'date' => '2016/11/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb049b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb049.jpg',
                'alt' => 'Kuriberon Cover Vol.49',
                'img_title' => 'Kuriberon Cover Vol.49',
                'issue' => 'Volume 49',
                'artworkby' => 'Sumino Yuuji',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Opportunity',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb051.jpg',
                'jp' => 'スーパーゲスビッチひなたちゃん',
                'eng' => 'Super Gesu Bitch Hinata-chan',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.51',
                'date' => '2017/01/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb051.jpg',
                'smallimage' => 'mangalist/kuriberon/kb051.jpg',
                'alt' => 'Kuriberon Cover Vol.51',
                'img_title' => 'Kuriberon Cover Vol.51',
                'issue' => 'Volume 51',
                'artworkby' => 'Zummy',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Freestyle',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb052.jpg',
                'jp' => '女帝カリギュラ',
                'eng' => 'Empress Caligula',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.52',
                'date' => '2017/02/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb052.jpg',
                'smallimage' => 'mangalist/kuriberon/kb052.jpg',
                'alt' => 'Kuriberon Cover Vol.52',
                'img_title' => 'Kuriberon Cover Vol.52',
                'issue' => 'Volume 52',
                'artworkby' => 'Sanjou Tomomi',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Opportunity',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb054.jpg',
                'jp' => '熱血マタニティ',
                'eng' => 'Hot-blood maternity',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.54',
                'date' => '2017/04/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb054b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb054.jpg',
                'alt' => 'Kuriberon Cover Vol.54',
                'img_title' => 'Kuriberon Cover Vol.54',
                'issue' => 'Volume 54',
                'artworkby' => 'Fujimoto Ikura',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Freestyle',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb056.jpg',
                'jp' => 'コントーション彼女',
                'eng' => 'My Contortion Girl',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.56',
                'date' => '2017/06/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb056b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb056.jpg',
                'alt' => 'Kuriberon Cover Vol.56',
                'img_title' => 'Kuriberon Cover Vol.56',
                'issue' => 'Volume 56',
                'artworkby' => 'Tsumagomi Izumo',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Freestyle',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb058.jpg',
                'jp' => '決めるぜ！　悶絶バスター',
                'eng' => 'Do it! Monzetsu Buster',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.58',
                'date' => '2017/08/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb058b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb058.jpg',
                'alt' => 'Kuriberon Cover Vol.58',
                'img_title' => 'Kuriberon Cover Vol.58',
                'issue' => 'Volume 58',
                'artworkby' => 'Sumino Yuuji',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Freestyle',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb060.jpg',
                'jp' => '抑圧フルパージ！',
                'eng' => 'Oppression Full Purge',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.60',
                'date' => '2017/10/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb060b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb060.jpg',
                'alt' => 'Kuriberon Cover Vol.60',
                'img_title' => 'Kuriberon Cover Vol.60',
                'issue' => 'Volume 60',
                'artworkby' => 'Sanjou Tomomi',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Freestyle',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb062.jpg',
                'jp' => '桃袋先生のミラクル乳袋',
                'eng' => 'Cartoonist Momobukuro\'s Miracle Milk Bag',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.62',
                'date' => '2017/12/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb062b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb062.jpg',
                'alt' => 'Kuriberon Cover Vol.62',
                'img_title' => 'Kuriberon Cover Vol.62',
                'issue' => 'Volume 62',
                'artworkby' => 'JUNK Kameyoko',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Freestyle',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb066.jpg',
                'jp' => '続・悶絶アイドル道',
                'eng' => 'Sequel for Monzetsu Idle Rolad',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.66',
                'date' => '2018/04/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb066b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb066.jpg',
                'alt' => 'Kuriberon Cover Vol.66',
                'img_title' => 'Kuriberon Cover Vol.66',
                'issue' => 'Volume 66',
                'artworkby' => 'JUNK Kameyoko',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Freestyle',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb068.jpg',
                'jp' => '彼女の異常な欲情',
                'eng' => 'Her abnormal lust',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.68',
                'date' => '2018/06/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb068b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb068.jpg',
                'alt' => 'Kuriberon Cover Vol.68',
                'img_title' => 'Kuriberon Cover Vol.68',
                'issue' => 'Volume 68',
                'artworkby' => 'JUNK Kameyoko',
            ),
        ),
        array(
		    'tankobon' => 'Fainting in Agony Freestyle',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb070.jpg',
                'jp' => '放課後バラバラ事件',
                'eng' => 'Mutilate Fuck at the After School',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.70',
                'date' => '2018/08/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb070b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb070.jpg',
                'alt' => 'Kuriberon Cover Vol.70',
                'img_title' => 'Kuriberon Cover Vol.70',
                'issue' => 'Volume 70',
                'artworkby' => 'JUNK Kameyoko',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb074.jpg',
                'jp' => 'A･F　アイツと再び',
                'eng' => 'A.F with me again',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.74',
                'date' => '2018/12/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb074b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb074.jpg',
                'alt' => 'Kuriberon Cover Vol.74',
                'img_title' => 'Kuriberon Cover Vol.74',
                'issue' => 'Volume 74',
                'artworkby' => 'JUNK Kameyoko',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb076.jpg',
                'jp' => '白い激情',
                'eng' => 'White Passion',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.76',
                'date' => '2019/02/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb076b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb076.jpg',
                'alt' => 'Kuriberon Cover Vol.76',
                'img_title' => 'Kuriberon Cover Vol.76',
                'issue' => 'Volume 76',
                'artworkby' => 'JUNK Kameyoko',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb078.jpg',
                'jp' => 'ミクロ決死隊',
                'eng' => 'Micro Decisive Corps',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.78',
                'date' => '2019/04/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb078b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb078.jpg',
                'alt' => 'Kuriberon Cover Vol.78',
                'img_title' => 'Kuriberon Cover Vol.78',
                'issue' => 'Volume 78',
                'artworkby' => 'JUNK Kameyoko',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb080.jpg',
                'jp' => '三つの心がひとつになれば',
                'eng' => 'The Trinity',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.80',
                'date' => '2019/06/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb080b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb080.jpg',
                'alt' => 'Kuriberon Cover Vol.80',
                'img_title' => 'Kuriberon Cover Vol.80',
                'issue' => 'Volume 80',
                'artworkby' => 'JUNK Kameyoko',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb082.jpg',
                'jp' => 'サマースメル',
                'eng' => 'Summer Smell',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.82',
                'date' => '2019/08/01',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb082b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb082.jpg',
                'alt' => 'Kuriberon Cover Vol.82',
                'img_title' => 'Kuriberon Cover Vol.82',
                'issue' => 'Volume 82',
                'artworkby' => 'Saigado',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb088.jpg',
                'jp' => '黒く呼べ',
                'eng' => 'Call me black Dirty Summon',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.88',
                'date' => '2020/02/07',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb088b.jpg',
                'smallimage' => 'mangalist/kuriberon/kb088.jpg',
                'alt' => 'Kuriberon Cover Vol.88',
                'img_title' => 'Kuriberon Cover Vol.88',
                'issue' => 'Volume 88',
                'artworkby' => 'Saigado',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/kuriberon/ch/kb091.jpg',
                'jp' => 'ミートな彼女',
                'eng' => 'Meat Girlfriend',
            ),
            'magazine' => array(
                'title' => 'Kuriberon Vol.91',
                'date' => '2020/05/09',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/kuriberon/kb091.jpg',
                'smallimage' => 'mangalist/kuriberon/kb091.jpg',
                'alt' => 'Kuriberon Cover Vol.91',
                'img_title' => 'Kuriberon Cover Vol.91',
                'issue' => 'Volume 91',
                'artworkby' => 'Saigado',
            ),
        ),
    );
?>