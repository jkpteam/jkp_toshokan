<?php
    $contents = array(
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastorea/ch/msa2016_01c.jpg',
                'jp' => '吉田君とメス豚先輩',
                'eng' => 'Mr. Yoshida and the senior female pig',
            ),
            'magazine' => array(
                'title' => 'Megastore Alpha 2016.01',
                'date' => '2015/12/14',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastorea/msa2016_01.jpg"',
                'smallimage' => 'mangalist/megastorea/msa2016_01.jpg',
                'alt' => 'Megastore Alpha Volume 2016/01 Cover',
                'img_title' => 'Megastore Alpha Volume 2016/01 Cover',
                'issue' => '2016/01',
                'artworkby' => 'Sasaoka Gungu',
            ),
            'comment' => array(
                'cimg' => 'megastorea/comment/msa2016_01.png',
                'jptext' => '心機一転を試みましたが、やっぱり珍妙な作品になってしまいました。',
            ),
        ),  
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastorea/msa2016_01.jpg',
                'jp' => '愛しきアクメイト 宣伝告知',
                'eng' => 'My lovely Acmate Advertisement',
            ),
            'magazine' => array(
                'title' => 'Megastore Alpha 2017.05',
                'date' => '2017/04/14',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastorea/msa2017_05.jpg',
                'smallimage' => 'mangalist/megastorea/msa2017_05.jpg',
                'alt' => 'Megastore Alpha Volume 2017/06 Cover',
                'img_title' => 'Megastore Alpha Volume 2017/06 Cover',
                'issue' => '2017/05',
                'artworkby' => 'Akino Sora',
            ),
            'comment' => array(
                'cimg' => '',
                'jptext' => '久しぶりに単行本を出させいただくことになりました、興味のある方は是非！',
            ),
        ),  
    );
?>