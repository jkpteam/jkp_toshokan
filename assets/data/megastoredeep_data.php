<?php
    $contents = array(
        array(
		    'tankobon' => 'My Lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd001.jpg',
                'jp' => 'ショタハンター☆ヒカル',
                'eng' => 'Shota Hunter Hikaru',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 01',
                'date' => '2015/12/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd001b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd001.jpg',
                'alt' => 'Megastore Deep Volume 01 Cover',
                'img_title' => 'Megastore Deep Volume 01 Cover',
                'issue' => 'Volume 01',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),  
        array(
		    'tankobon' => 'My Lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd002.jpg',
                'jp' => 'ホルモン喫茶へようこそ',
                'eng' => 'Welcome to Hormone Tea House',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 02',
                'date' => '2016/03/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd002b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd002.jpg',
                'alt' => 'Megastore Deep Volume 02 Cover',
                'img_title' => 'Megastore Deep Volume 02 Cover',
                'issue' => 'Volume 02',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),  
        array(
		    'tankobon' => 'My Lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd003.jpg',
                'jp' => '魅惑の貫き姫',
                'eng' => 'Captivating Piercing Princess',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 03',
                'date' => '2016/06/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd003b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd003.jpg',
                'alt' => 'Megastore Deep Volume 03 Cover',
                'img_title' => 'Megastore Deep Volume 03 Cover',
                'issue' => 'Volume 03',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),
        array(
		    'tankobon' => 'My Lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd005.jpg',
                'jp' => '丑三つボンバー',
                'eng' => 'Ushimitsu Bomber',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 05',
                'date' => '2016/10/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd005b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd005.jpg',
                'alt' => 'Megastore Deep Volume 05 Cover',
                'img_title' => 'Megastore Deep Volume 05 Cover',
                'issue' => 'Volume 05',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),
        array(
		    'tankobon' => 'My Lovely Acmate',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd007.jpg',
                'jp' => 'ケモニストファミリー',
                'eng' => 'Kemonist Family',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 07',
                'date' => '2017/01/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd007b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd007.jpg',
                'alt' => 'Megastore Deep Volume 07 Cover',
                'img_title' => 'Megastore Deep Volume 07 Cover',
                'issue' => 'Volume 07',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),
        array(
		    'tankobon' => 'Mesu ana Kanojo, Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd009.jpg',
                'jp' => 'お姉トラップ',
                'eng' => 'Onee Trap',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 09',
                'date' => '2017/05/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd009b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd009.jpg',
                'alt' => 'Megastore Deep Volume 09 Cover',
                'img_title' => 'Megastore Deep Volume 09 Cover',
                'issue' => 'Volume 09',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),  
        array(
		    'tankobon' => 'Mesu ana Kanojo, Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd011.jpg',
                'jp' => 'サマーテンタクル',
                'eng' => 'Summer Tentacle',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 11',
                'date' => '2017/09/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd011b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd011.jpg',
                'alt' => 'Megastore Deep Volume 11 Cover',
                'img_title' => 'Megastore Deep Volume 11 Cover',
                'issue' => 'Volume 11',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),  
        array(
		    'tankobon' => 'Mesu ana Kanojo, Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd013.jpg',
                'jp' => '肉欲ママンライフ',
                'eng' => 'Flesh Maman Life',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 13',
                'date' => '2018/01/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd013b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd013.jpg',
                'alt' => 'Megastore Deep Volume 13 Cover',
                'img_title' => 'Megastore Deep Volume 13 Cover',
                'issue' => 'Volume 13',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),  
        array(
		    'tankobon' => 'Mesu ana Kanojo, Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd015.jpg',
                'jp' => '彼女は黒いハンター',
                'eng' => 'She is a black hunter	',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 15',
                'date' => '2018/05/2',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd015b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd015.jpg',
                'alt' => 'Megastore Deep Volume 15 Cover',
                'img_title' => 'Megastore Deep Volume 15 Cover',
                'issue' => 'Volume 15',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),  
        array(
		    'tankobon' => 'Mesu ana Kanojo, Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd017.jpg',
                'jp' => '怪奇！全穴女',
                'eng' => 'Amazing ! Every holes girl',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 17',
                'date' => '2018/09/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd017b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd017.jpg',
                'alt' => 'Megastore Deep Volume 17 Cover',
                'img_title' => 'Megastore Deep Volume 17 Cover',
                'issue' => 'Volume 17',
                'artworkby' => 'Yumisuke Kotoyoshi',
            ),
        ),  
        array(
		    'tankobon' => 'Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd019.jpg',
                'jp' => 'キャンプフィーバー',
                'eng' => 'Camp Fever',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 19',
                'date' => '2019/01/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd019b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd019.jpg',
                'alt' => 'Megastore Deep Volume 91 Cover',
                'img_title' => 'Megastore Deep Volume 19 Cover',
                'issue' => 'Volume 19',
                'artworkby' => 'Gustav',
            ),
        ),  
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd021.jpg',
                'jp' => 'ミセス・スーパーマーケット',
                'eng' => 'Mrs. Supermarket',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 21',
                'date' => '2019/05/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd021b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd021.jpg',
                'alt' => 'Megastore Deep Volume 21 Cover',
                'img_title' => 'Megastore Deep Volume 21 Cover',
                'issue' => 'Volume 21',
                'artworkby' => 'Zucchini',
            ),
        ),  
        array(
		    'tankobon' => 'Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd023.jpg',
                'jp' => '変身スイッチ',
                'eng' => 'Henshin switch',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 23',
                'date' => '2019/09/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd023b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd023.jpg',
                'alt' => 'Megastore Deep Volume 23 Cover',
                'img_title' => 'Megastore Deep Volume 23 Cover',
                'issue' => 'Volume 23',
                'artworkby' => 'Sinozuka Atsuto',
            ),
        ),  
        array(
		    'tankobon' => 'Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd025.jpg',
                'jp' => '君よ俺で雌になれ',
                'eng' => 'You, become a female with me',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 25',
                'date' => '2020/02/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd025b.png',
                'smallimage' => 'mangalist/megastoredeep/msd025.jpg',
                'alt' => 'Megastore Deep Volume 25 Cover',
                'img_title' => 'Megastore Deep Volume 25 Cover',
                'issue' => 'Volume 25',
                'artworkby' => 'GGぃ',
            ),
        ),  
        array(
		    'tankobon' => 'Gucchiri Ana Meito',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd027.jpg',
                'jp' => 'よくばりエッグス',
                'eng' => 'Yokubari Eggs',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 27',
                'date' => '2020/06/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd027b.png',
                'smallimage' => 'mangalist/megastoredeep/msd027.jpg',
                'alt' => 'Megastore Deep Volume 27 Cover',
                'img_title' => 'Megastore Deep Volume 27 Cover',
                'issue' => 'Volume 27',
                'artworkby' => 'Menokomone',
            ),
        ),  
        array(
		    'tankobon' => 'Mubobi na Shiko Ana',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd029.jpg',
                'jp' => '怪奇！ ピザおばさん',
                'eng' => 'Mysterious! Aunt Pizza',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 29',
                'date' => '2020/10/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd029b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd029.jpg',
                'alt' => 'Megastore Deep Volume 29 Cover',
                'img_title' => 'Megastore Deep Volume 29 Cover',
                'issue' => 'Volume 29',
                'artworkby' => 'Menokomone',
            ),
        ),
        array(
		    'tankobon' => 'Mubobi na Shiko Ana',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd031.jpg',
                'jp' => 'オレの年上肉便器',
                'eng' => 'My older meat urinal',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 31',
                'date' => '2021/02/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd031b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd031.jpg',
                'alt' => 'Megastore Deep Volume 31 Cover',
                'img_title' => 'Megastore Deep Volume 31 Cover',
                'issue' => 'Volume 31',
                'artworkby' => 'Koyanagi Royal',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd033.jpg',
                'jp' => '兄貴の嫁さんは入れたがり',
                'eng' => 'My brother\'s wife wants in',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 33',
                'date' => '2021/06/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd033b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd033.jpg',
                'alt' => 'Megastore Deep Volume 33 Cover',
                'img_title' => 'Megastore Deep Volume 33 Cover',
                'issue' => 'Volume 33',
                'artworkby' => 'Ashisyun',
            ),
        ),
        array(
		    'tankobon' => 'Mubobi na Shiko Ana',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd034.jpg',
                'jp' => '元カノマタニティ',
                'eng' => 'Ex-girlfriend Maternity',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 34',
                'date' => '2021/08/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd034b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd034.jpg',
                'alt' => 'Megastore Deep Volume 34 Cover',
                'img_title' => 'Megastore Deep Volume 34 Cover',
                'issue' => 'Volume 34',
                'artworkby' => 'Funabori Nariaki',
            ),
        ),
        array(
		    'tankobon' => 'Mubobi na Shiko Ana',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd036.jpg',
                'jp' => '魔乳の伝説',
                'eng' => 'Legend of the Magic Milk',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 36',
                'date' => '2021/12/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd036b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd036.jpg',
                'alt' => 'Megastore Deep Volume 36 Cover',
                'img_title' => 'Megastore Deep Volume 36 Cover',
                'issue' => 'Volume 36',
                'artworkby' => '腿之助兵衛',
            ),
        ),
        array(
		    'tankobon' => 'Mubobi na Shiko Ana',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd037.jpg',
                'jp' => '美人上司はドランククィーン',
                'eng' => 'Beautiful boss is Drunk Queen',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 37',
                'date' => '2022/02/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd037b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd037.jpg',
                'alt' => 'Megastore Deep Volume 37 Cover',
                'img_title' => 'Megastore Deep Volume 37 Cover',
                'issue' => 'Volume 37',
                'artworkby' => 'からあげチャン',
            ),
        ),
        array(
		    'tankobon' => '-',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/megastoredeep/ch/msd039.jpg',
                'jp' => '夢のキャンパスライフ',
                'eng' => 'Campus Life Dream',
            ),
            'magazine' => array(
                'title' => 'Megastore Deep Vol. 39',
                'date' => '2022/06/24',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/megastoredeep/msd039b.jpg',
                'smallimage' => 'mangalist/megastoredeep/msd039.jpg',
                'alt' => 'Megastore Deep Volume 39 Cover',
                'img_title' => 'Megastore Deep Volume 39 Cover',
                'issue' => 'Volume 39',
                'artworkby' => 'Suzunonerena',
            ),
        ),
    );
?>