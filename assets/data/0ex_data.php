<?php
    $contents = array(
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex01.jpg',
                'jp' => 'デンジャーセイント24',
                'eng' => 'Danger Saint 24',
            ),
            'magazine' => array(
                'title' => '0EX Vol.01',
                'date' => '2007/12/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex01.jpg',
                'smallimage' => 'mangalist/ex/ex01.jpg',
                'alt' => '0EX Volume 1 Cover',
                'img_title' => '0EX Volume 1 Cover',
                'issue' => 'Volume 1',
                'artworkby' => 'NIXinamo：LENS',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc01.jpg',
                'jptext' => '流行り神２とボトモズのためにひさしぶりにPS２を機動させました。',
            ),
        ),  
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex02.jpg',
                'jp' => '咲き乱れトレイン',
                'eng' => 'Blossoming Disorder Train!',
            ),
            'magazine' => array(
                'title' => '0EX Vol.02',
                'date' => '2008/01/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex02.jpg',
                'smallimage' => 'mangalist/ex/ex02.jpg',
                'alt' => '0EX Volume 2 Cover',
                'img_title' => '0EX Volume 2 Cover',
                'issue' => 'Volume 2',
                'artworkby' => 'NIXinamo：LENS',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc02.jpg',
                'jptext' => '新年そうそう、こんなマンガですいません。',
            ),
        ),  
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex04.jpg',
                'jp' => '青空シークレット',
                'eng' => 'Blue Sky Secret',
            ),
            'magazine' => array(
                'title' => '0EX Vol.04',
                'date' => '2008/03/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex04.jpg',
                'smallimage' => 'mangalist/ex/ex04.jpg',
                'alt' => '0EX Volume 4 Cover',
                'img_title' => '0EX Volume 4 Cover',
                'issue' => 'Volume 4',
                'artworkby' => 'NIXinamo：LENS',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc04.jpg',
                'jptext' => '昔の単行本がリニューアルされて発売中です、描き下ろすも載ってます。',
            ),
        ),
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex05.jpg',
                'jp' => 'エンドレスなGでワルツ',
                'eng' => 'Endless Waltz in G',
            ),
            'magazine' => array(
                'title' => '0EX Vol.05',
                'date' => '2008/04/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex05.jpg',
                'smallimage' => 'mangalist/ex/ex05.jpg',
                'alt' => '0EX Volume 5 Cover',
                'img_title' => '0EX Volume 5 Cover',
                'issue' => 'Volume 5',
                'artworkby' => 'NIXinamo：LENS',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc05.jpg',
                'jptext' => 'ホステル２のDVDを買った、　あいかわらず不謹慎すぎて笑えました。',
            ),
        ),
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex06.jpg',
                'jp' => 'ホップ・ステップ・狂気',
                'eng' => 'Hop Skip Crazy!',
            ),
            'magazine' => array(
                'title' => '0EX Vol.06',
                'date' => '2008/05/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex06.jpg',
                'smallimage' => 'mangalist/ex/ex06.jpg',
                'alt' => '0EX Volume 6 Cover',
                'img_title' => '0EX Volume 6 Cover',
                'issue' => 'Volume 6',
                'artworkby' => 'NIXinamo：LENS',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc06.jpg',
                'jptext' => 'ボトムズのフィギュアがスコタコだけで50体越えるを始末に・・・。',
            ),
        ),
        array(
		    'tankobon' => 'Exciting Fainting in Agony Balkan!!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex07.jpg',
                'jp' => 'エキサイト第二彼女',
                'eng' => 'Excite! Second girl!',
            ),
            'magazine' => array(
                'title' => '0EX Vol.07',
                'date' => '2008/06/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex07.jpg',
                'smallimage' => 'mangalist/ex/ex07.jpg',
                'alt' => '0EX Volume 7 Cover',
                'img_title' => '0EX Volume 7 Cover',
                'issue' => 'Volume 7',
                'artworkby' => 'NIXinamo：LENS',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc07.jpg',
                'jptext' => '劇エヴァDVDのおまけフィルムはリツコとマヤが。　当たり・・・かな？',
            ),
        ),
        array(
		    'tankobon' => 'Intense!! Fainting in Agony Operation Plus',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex08.jpg',
                'jp' => 'ビッチ娘と爵位のマグナム',
                'eng' => 'The Bitch and the Magnum',
            ),
            'magazine' => array(
                'title' => '0EX Vol.08',
                'date' => '2008/07/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex08.jpg',
                'smallimage' => 'mangalist/ex/ex08.jpg',
                'alt' => '0EX Volume 8 Cover',
                'img_title' => '0EX Volume 8 Cover',
                'issue' => 'Volume 8',
                'artworkby' => 'NIXinamo：LENS',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc08.jpg',
                'jptext' => '最新単行本が発売中です。　カオスな感じに仕上がってるのでヨロシクです。',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex09.jpg',
                'jp' => '灼熱のエクストリーム打法',
                'eng' => 'Scorching Extreme Batting',
            ),
            'magazine' => array(
                'title' => '0EX Vol.09',
                'date' => '2008/08/09',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex09.jpg',
                'smallimage' => 'mangalist/ex/ex09.jpg',
                'alt' => '0EX Volume 9 Cover',
                'img_title' => '0EX Volume 9 Cover',
                'issue' => 'Volume 9',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc09.jpg',
                'jptext' => 'ようやく「２８週後・・・」を観ました。　やっぱり、　走るゾンビ？はいいですね。',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex10.jpg',
                'jp' => 'サマーライアット\'09',
                'eng' => 'Summer Riot\'09',
            ),
            'magazine' => array(
                'title' => '0EX Vol.10',
                'date' => '2008/09/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex10.jpg',
                'smallimage' => 'mangalist/ex/ex10.jpg',
                'alt' => '0EX Volume 10 Cover',
                'img_title' => '0EX Volume 10 Cover',
                'issue' => 'Volume 10',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc10.jpg',
                'jptext' => '長年酷使してきた鉄騎のコントローラが壊れました、お疲れ様と言ってあげたいです。',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex11.jpg',
                'jp' => 'ハイパー丑の刻フィアー',
                'eng' => 'Hyper Ox\'s Hour of Fear',
            ),
            'magazine' => array(
                'title' => '0EX Vol.11',
                'date' => '2008/10/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex11.jpg',
                'smallimage' => 'mangalist/ex/ex11.jpg',
                'alt' => '0EX Volume 11 Cover',
                'img_title' => '0EX Volume 11 Cover',
                'issue' => 'Volume 11',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc11.jpg',
                'jptext' => 'ざまとの１/60VF/１-A,　この完成度はスゴすぐる・・・ハァハァ。',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex12.jpg',
                'jp' => '緊急覚醒スイッチON!!',
                'eng' => 'Urgent Awakening, Switch ON!!',
            ),
            'magazine' => array(
                'title' => '0EX Vol.12',
                'date' => '2008/11/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex11.jpg',
                'smallimage' => 'mangalist/ex/ex11.jpg',
                'alt' => '0EX Volume 12 Cover',
                'img_title' => '0EX Volume 12 Cover',
                'issue' => 'Volume 12',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc12.jpg',
                'jptext' => '毎週ダブルオーが楽しみでしょうがないです。',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex13.jpg',
                'jp' => 'ダダ漏れカスタードヘブン',
                'eng' => 'Whimsy Leak Custard Heaven',
            ),
            'magazine' => array(
                'title' => '0EX Vol.13',
                'date' => '2008/12/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex13.jpg',
                'smallimage' => 'mangalist/ex/ex13.jpg',
                'alt' => '0EX Volume 13 Cover',
                'img_title' => '0EX Volume 13 Cover',
                'issue' => 'Volume 13',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc13.jpg',
                'jptext' => 'GTAIVをプレイしてるとやたらジャンクフードが食べたくなるのは自分だけぢゃないハズ・・・。',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex14.jpg',
                'jp' => '狂い咲きブラックサレナ',
                'eng' => 'Wildly Blossoming Black Sarena',
            ),
            'magazine' => array(
                'title' => '0EX Vol.14',
                'date' => '2009/01/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex14.jpg',
                'smallimage' => 'mangalist/ex/ex14.jpg',
                'alt' => '0EX Volume 14 Cover',
                'img_title' => '0EX Volume 14 Cover',
                'issue' => 'Volume 14',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc14.jpg',
                'jptext' => '変なエロマンガしか描けませんが今年もヨロシクお願いします。',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex15.jpg',
                'jp' => 'ランペイジゴースト',
                'eng' => 'Ramage Ghost',
            ),
            'magazine' => array(
                'title' => '0EX Vol.15',
                'date' => '2009/02/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex15.jpg',
                'smallimage' => 'mangalist/ex/ex15.jpg',
                'alt' => '0EX Volume 15 Cover',
                'img_title' => '0EX Volume 15 Cover',
                'issue' => 'Volume 15',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc15.jpg',
                'jptext' => '１年半ぶりに髪を切りました。メタルからパンクすに戻りました。',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex16.jpg',
                'jp' => 'おちぶれてカタストロフ',
                'eng' => 'Ruinous Catastrophe',
            ),
            'magazine' => array(
                'title' => '0EX Vol.16',
                'date' => '2009/03/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex16.jpg',
                'smallimage' => 'mangalist/ex/ex16.jpg',
                'alt' => '0EX Volume 16 Cover',
                'img_title' => '0EX Volume 16 Cover',
                'issue' => 'Volume 16',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc16.jpg',
                'jptext' => 'シュガーミルク先生と飲みました、色んな事で刺激を受けました。また是非！！',
            ),
        ),
        array(
		    'tankobon' => 'MonfestXI',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex17.jpg',
                'jp' => 'カタキバーニング',
                'eng' => 'Rivalry Burning',
            ),
            'magazine' => array(
                'title' => '0EX Vol.17',
                'date' => '2009/04/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex17.jpg',
                'smallimage' => 'mangalist/ex/ex17.jpg',
                'alt' => '0EX Volume 17 Cover',
                'img_title' => '0EX Volume 17 Cover',
                'issue' => 'Volume 17',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc17.jpg',
                'jptext' => '編集のコアイトウさんがエルピス・コステロに似ていると思うのはオレだけでしょーか？（いい意味で）',
            ),
        ),
        array(
		    'tankobon' => 'Super Fainting in Agony Mega Bitch',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex18.jpg',
                'jp' => '魂の爆砕スクラム!!',
                'eng' => 'The soul\'s Blasting Scrum!!',
            ),
            'magazine' => array(
                'title' => '0EX Vol.18',
                'date' => '2009/05/09',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex18.jpg',
                'smallimage' => 'mangalist/ex/ex18.jpg',
                'alt' => '0EX Volume 18 Cover',
                'img_title' => '0EX Volume 18 Cover',
                'issue' => 'Volume 18',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc18.jpg',
                'jptext' => '新しいマジンガーの初回は色々と衝撃的でした・・・',
            ),
        ),
        array(
		    'tankobon' => 'Super Fainting in Agony Mega Bitch',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex19.jpg',
                'jp' => 'トーキョードランクプリン',
                'eng' => 'Tokyo Drunk Pudding',
            ),
            'magazine' => array(
                'title' => '0EX Vol.19',
                'date' => '2009/06/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex19.jpg',
                'smallimage' => 'mangalist/ex/ex19.jpg',
                'alt' => '0EX Volume 19 Cover',
                'img_title' => '0EX Volume 19 Cover',
                'issue' => 'Volume 19',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc19.jpg',
                'jptext' => 'グレンにＴ４エヴァ、個人的にひさびさの映画ラッシュです。',
            ),
        ),
        array(
		    'tankobon' => 'Muchi Muchi!! Fainting in Agony Fever Plus',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex20.jpg',
                'jp' => 'バーニングJETピーチ',
                'eng' => 'Burning JET Peach',
            ),
            'magazine' => array(
                'title' => '0EX Vol.20',
                'date' => '2009/07/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex20.jpg',
                'smallimage' => 'mangalist/ex/ex20.jpg',
                'alt' => '0EX Volume 20 Cover',
                'img_title' => '0EX Volume 20 Cover',
                'issue' => 'Volume 20',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc20.jpg',
                'jptext' => '新しい単行本が発売中です。今回もハードでマニアックに仕上がっております。',
            ),
        ),
        array(
		    'tankobon' => 'Super Fainting in Agony Mega Bitch',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex21.jpg',
                'jp' => 'マグマスライダー',
                'eng' => 'Magma Slider',
            ),
            'magazine' => array(
                'title' => '0EX Vol.21',
                'date' => '2009/08/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex21.jpg',
                'smallimage' => 'mangalist/ex/ex21.jpg',
                'alt' => '0EX Volume 21 Cover',
                'img_title' => '0EX Volume 21 Cover',
                'issue' => 'Volume 21',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc21.jpg',
                'jptext' => 'サイン会に遠方から来た方、差し入れをくれた方、会社をサボって着てくれた方、多くの方々ありがとうございました。',
            ),
        ),
        array(
		    'tankobon' => 'Super Fainting in Agony Mega Bitch',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex22.jpg',
                'jp' => '常夏シェービング',
                'eng' => 'Shaving on Neverending Summer',
            ),
            'magazine' => array(
                'title' => '0EX Vol.22',
                'date' => '2009/09/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex22.jpg',
                'smallimage' => 'mangalist/ex/ex22.jpg',
                'alt' => '0EX Volume 22 Cover',
                'img_title' => '0EX Volume 22 Cover',
                'issue' => 'Volume 22',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc22.jpg',
                'jptext' => '親睦会行きたかったな・・・案内状を入手したのは宴の三日後でした・・・（涙）',
            ),
        ),
        array(
		    'tankobon' => 'Super Fainting in Agony Mega Bitch',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex23.jpg',
                'jp' => '非道のエチケット',
                'eng' => 'Inhuman Etiquette',
            ),
            'magazine' => array(
                'title' => '0EX Vol.23',
                'date' => '2009/10/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex23.jpg',
                'smallimage' => 'mangalist/ex/ex23.jpg',
                'alt' => '0EX Volume 23 Cover',
                'img_title' => '0EX Volume 23 Cover',
                'issue' => 'Volume 23',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc23.jpg',
                'jptext' => 'ランシド、グリーンディ、プロディジーの新作が同じ年に出た事は個人的にはうれしすぎます。',
            ),
        ),
        array(
		    'tankobon' => 'Monzetsu System!',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex24.jpg',
                'jp' => 'コンプレックスオンリーワン！',
                'eng' => 'Complex Only One!',
            ),
            'magazine' => array(
                'title' => '0EX Vol.24',
                'date' => '2009/11/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex24.jpg',
                'smallimage' => 'mangalist/ex/ex24.jpg',
                'alt' => '0EX Volume 24 Cover',
                'img_title' => '0EX Volume 24 Cover',
                'issue' => 'Volume 24',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc24.jpg',
                'jptext' => 'ようやくＰＳ３を購入！問題はやりたいソフトが日本で発売されるかです・・・',
            ),
        ),
        array(
		    'tankobon' => 'Super Fainting in Agony Mega Bitch',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex25.jpg',
                'jp' => '幻のサイキックホームラン',
                'eng' => 'Phantom Sidekick Homerun',
            ),
            'magazine' => array(
                'title' => '0EX Vol.25',
                'date' => '2009/12/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex25.jpg',
                'smallimage' => 'mangalist/ex/ex25.jpg',
                'alt' => '0EX Volume 25 Cover',
                'img_title' => '0EX Volume 25 Cover',
                'issue' => 'Volume 25',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc25.jpg',
                'jptext' => '昔の単行本がリニューアルされて発売中です、書きおろし触手マンガもあります。',
            ),
        ),
        array(
		    'tankobon' => 'Super Fainting in Agony Mega Bitch',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex26.jpg',
                'jp' => '逆襲のニードルバルカン!!',
                'eng' => 'The Counter attack\'s needle balkan!!',
            ),
            'magazine' => array(
                'title' => '0EX Vol.26',
                'date' => '2010/01/09',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex26.jpg',
                'smallimage' => 'mangalist/ex/ex26.jpg',
                'alt' => '0EX Volume 26 Cover',
                'img_title' => '0EX Volume 25 Cover',
                'issue' => 'Volume 26',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc26.jpg',
                'jptext' => '新年一発目からこんな全力でニッチなマンガでスイマセン・・・',
            ),
        ),
        array(
		    'tankobon' => 'Super Fainting in Agony Mega Bitch',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex27.jpg',
                'jp' => '情熱のメガネヒモビッチ',
                'eng' => 'Passionate Four-eyed String Bitch',
            ),
            'magazine' => array(
                'title' => '0EX Vol.27',
                'date' => '2010/02/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex27.jpg',
                'smallimage' => 'mangalist/ex/ex27.jpg',
                'alt' => '0EX Volume 27 Cover',
                'img_title' => '0EX Volume 27 Cover',
                'issue' => 'Volume 27',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc27.jpg',
                'jptext' => '劇場版マクロスＦｎｏ女性客の多さに少しおどろきました。',
            ),
        ),
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex28.jpg',
                'jp' => 'ドル箱メス豚プリンセス',
                'eng' => 'Female Pig Princess Cash Cow',
            ),
            'magazine' => array(
                'title' => '0EX Vol.28',
                'date' => '2010/03/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex28.jpg',
                'smallimage' => 'mangalist/ex/ex28.jpg',
                'alt' => '0EX Volume 28 Cover',
                'img_title' => '0EX Volume 28 Cover',
                'issue' => 'Volume 28',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc28.jpg',
                'jptext' => 'アシストパッドをつけたらＰＳ３コンが生まれ変わった・・・こいつはスゴイ。',
            ),
        ),
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex29.jpg',
                'jp' => 'シャッターファッカーシスター',
                'eng' => 'Shutter Fucker Sister',
            ),
            'magazine' => array(
                'title' => '0EX Vol.29',
                'date' => '2010/04/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex29.jpg',
                'smallimage' => 'mangalist/ex/ex29.jpg',
                'alt' => '0EX Volume 29 Cover',
                'img_title' => '0EX Volume 29 Cover',
                'issue' => 'Volume 29',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc29.jpg',
                'jptext' => '友人二人から誕プレにオッパイの小物とヘルレイザーのフィギュアをもらった。わかってるなァ・・・',
            ),
        ),
        array(
		    'tankobon' => 'Search and Fainting in Agony Destroy',
            'manga' => array(
                'imgurl' => '/assets/images/mangalist/ex/ch/ex30.jpg',
                'jp' => 'ウォシュレットテラー',
                'eng' => 'Toilet Spirit',
            ),
            'magazine' => array(
                'title' => '0EX Vol.30',
                'date' => '2010/05/10',
            ),
            'cover' => array(
                'bigimage' => 'mangalist/ex/ex30.jpg',
                'smallimage' => 'mangalist/ex/ex30.jpg',
                'alt' => '0EX Volume 30 Cover',
                'img_title' => '0EX Volume 30 Cover',
                'issue' => 'Volume 30',
                'artworkby' => 'Komiya Yuuta',
            ),
            'comment' => array(
                'cimg' => 'ex/comment/exc30.jpg',
                'jptext' => '単行本が発売中です。物好きでマニアックな方は是非!!',
            ),
        ),
    );
?>