<!-- Render TankoubonList-->
<?php function renderTankoubonList($contents) { ?>
    <?php foreach($contents as $content): ?>
        <div class="chwrapper">
            <div class="chmag"><?php echo $content['release'] ?></div>
                <div class="chimg"><img src="/assets/images/<?php echo $content['image']['manga'] ?>"></div>
                <div class="chimg"><img src="/assets/images/<?php echo $content['image']['chapter'] ?>"></div>
                <div class="chtitle">
                    <span class="entitle"><?php echo $content['title']['en'] ?></span><br>
                    <span class="jptitle"><?php echo $content['title']['jp'] ?></span><br>
                </div>
            <div class="clear"></div>
        </div>
    <?php endforeach ?>
<?php } ?>

<!-- Render CoverJumboGallery -->
<?php function renderJumboCoverGallery($contents) { ?>
<?php $i=0; ?>
<div class="cgcenter">
    <?php foreach($contents as $content): ?>
<?php if ($i != 0 && ($i%5 == 0)) print '</div><div class="cgcenter">' ?>
        <div class="kepszoveg"><img src="/assets/images/<?php echo $content['image'] ?>" alt="cover" title="cover"><br><?php echo $content['date'] ?><br><?php echo $content['issue'] ?></div>
    <?php $i++; ?>
<?php endforeach ?>
</div>
<?php } ?>

<!-- Render AnthologyList-->
<?php function renderAnthologyList($contents) { ?>
    <?php foreach($contents as $content): ?>
<div class="awrapper">
    <div class="antimg"><img src="<?php echo $content['cover'] ?>"></div>
    <div class="antch"><img src="<?php echo $content['antch'] ?>"></div>
    <div class="antdesc">
    <div class="anthtitle">
        <span class="antjap"><?php echo $content['anthjap'] ?></span><br>
        <span class="antheng"><?php echo $content['antheng'] ?></span>
    </div>
<table class="anthdesc">
    <tr>
        <td class="header">Release:</td>
        <td><?php echo $content['date'] ?></td>
    </tr>
    
    <tr>
        <td class="header">ISBN:</td>
        <td><?php echo $content['isbn'] ?></td>    
    </tr>

    <tr>
        <td class="header">Publisher:</td>
        <td><?php echo $content['publisher'] ?></td>    
    </tr>

    <tr>
        <td class="header">Series:</td>
        <td><?php echo $content['series'] ?></td>
    </tr> 

    <tr>
        <td class="header">Genre:</td>
        <td><?php echo $content['genre'] ?></td>
    </tr>

    <tr>
        <td class="header">Featured JKP:</td>
        <td><?php echo $content['jkp'] ?></td>    
    </tr>        
</table>
    </div>
</div>
    <?php endforeach ?>
<?php } ?>

<!-- Render CollaborationsList-->
<?php function renderCollaborationsList($contents) { ?>
    <?php foreach($contents as $content): ?>
<div class="awrapper">
    <div class="antimg"><img src="<?php echo $content['tcover'] ?>"></div>
    <div class="antch"><img src="<?php echo $content['cimg'] ?>"></div>
    <div class="antdesc">
    <div class="anthtitle">
        <span class="antheng"><?php echo $content['tankeng'] ?></span><br>
        <span class="antjap"><?php echo $content['tankjap'] ?></span>
    </div>
<table class="anthdesc">
    <tr>
        <td class="header">Release:</td>
        <td><?php echo $content['date'] ?></td>
    </tr>
    
    <tr>
        <td class="header">ISBN:</td>
        <td><?php echo $content['isbn'] ?></td>    
    </tr>

    <tr>
        <td class="header">Publisher:</td>
        <td><?php echo $content['publisher'] ?></td>    
    </tr>

    <tr>
        <td class="header">Author:</td>
        <td><?php echo $content['authoreng'] ?> (<?php echo $content['authorjap'] ?>)</td>
    </tr> 

    <tr>
        <td class="header">Collaboration:</td>
        <td><?php echo $content['ceng'] ?> (<?php echo $content['cjap'] ?>)</td>    
    </tr>        
</table>
    </div>
</div>
    <?php endforeach ?>
<?php } ?>


<!-- Render OmakeList-->
<?php function renderOmakeList($contents) { ?>
    <?php foreach($contents as $content): ?>
<div class="awrapper">
    <div class="antimg"><img src="<?php echo $content['tankimg'] ?>"></div>
    <div class="antch"><img src="<?php echo $content['cimg'] ?>"></div>
    <div class="antdesc">
    <div class="anthtitle">
        <span class="antheng"><?php echo $content['en'] ?></span><br>
        <span class="antjap"><?php echo $content['jp'] ?></span>
    </div>
<table class="anthdesc">
    <tr>
        <td class="header">Tankoubon:</td>
        <td><?php echo $content['tankoubon'] ?></td>
    </tr>
    <tr>
        <td class="header">Release:</td>
        <td><?php echo $content['date'] ?></td>
    </tr>
    
    <tr>
        <td class="header">ISBN:</td>
        <td><?php echo $content['isbn'] ?></td>    
    </tr>

    <tr>
        <td class="header">Publisher:</td>
        <td><?php echo $content['publisher'] ?></td>    
    </tr>

    <tr>
        <td class="header">Pages:</td>
        <td><?php echo $content['pages'] ?></td>    
    </tr>
        
</table>
    </div>
</div>
    <?php endforeach ?>
<?php } ?>

<!-- Render Magazine Manga List -->
<?php function renderMagazineManga($contents) { ?>
    <?php foreach($contents as $content): ?>
    <tr>
		<td class="cim"><a class="nagyobb" href="<?php echo $content['manga']['imgurl'] ?>"><?php echo $content['manga']['jp'] ?><br><?php echo $content['manga']['eng'] ?></a></td>
		<td><div class="mag-date"><?php echo $content['magazine']['title'] ?><br><?php echo $content['magazine']['date'] ?></div></td>
		<td class="tank"><?php echo $content['tankobon'] ?></td>
    </tr>    
    <?php endforeach ?>
<?php } ?>

<!-- Render Magazine Comment List -->
<?php function renderMagazineComment($contents) { ?>
    <?php foreach($contents as $content): ?>
		<tr>
			<td class="cim"><?php echo $content['manga']['jp'] ?><br><?php echo $content['manga']['eng'] ?></td>
			<td><div class="mag-date"><?php echo $content['magazine']['title'] ?></div></td>
			<td><img src="/assets/images/mangalist/<?php echo $content['comment']['cimg'] ?>"></td>
			<td><?php echo $content['comment']['jptext'] ?></td>
		</tr>
    <?php endforeach ?>
<?php } ?>

<!-- Render CoverGallery -->
<?php function renderCoverGallery($contents) { ?>
<?php $i=0; ?>
<div class="cgcenter">
    <?php foreach($contents as $content): ?>
<?php if ($i != 0 && ($i%5 == 0)) print '</div><div class="cgcenter">' ?>
        <div class="kepszoveg"><a class="nagyobb" href="/assets/images/<?php echo $content['cover']['bigimage'] ?>">
		<img src="/assets/images/<?php echo $content['cover']['smallimage'] ?>" alt="<?php echo $content['cover']['alt'] ?>" title="<?php echo $content['cover']['img_title'] ?>"></a><br><?php echo $content['cover']['issue'] ?><br><?php echo $content['cover']['artworkby'] ?></div> 
    <?php $i++; ?>
<?php endforeach ?>
</div>
<?php } ?>