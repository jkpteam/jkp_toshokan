<?php

/* Main menu */
$menu = array( 
    'news' => array( 'name' => 'News', 'link' => '' ),
    'about' => array( 'name' => 'About', 'link' => '' ),
    'magazine' => array( 'name' => 'Magazine', 'link' => '' ),
    'tankoubon' => array( 'name' => 'Tankoubon', 'link' => '' ),
    'anthology' => array( 'name' => 'Anthology', 'link' => '' ),
);

$submenus = array(
/* a 'news' page submenu */
		'news' => array( /* 'almenünév' => array( 'name' => 'kiírtnév', 'link' => 'pagelink' */ ),
/* az 'about' page submenu */
		'about' => array(),
/* a 'magazine' page submenu */
		'magazine' => array(
            'jumbo' => array( 'name' => 'COMIC Jumbo', 'link' => ''  ),
            'megaplus' => array( 'name' => 'COMIC Megaplus', 'link' => ''  ),
            '0EX' => array( 'name' => 'COMIC 0EX', 'link' => ''  ),
            'megamilk' => array( 'name' => 'COMIC MegaMilk', 'link' => ''  ),
            'megastoreh' => array( 'name' => 'COMIC Megastore H', 'link' => ''  ),
            'magnum' => array ('name' => 'COMIC Magnum', 'link' => ''),
            'kuriberon' => array ('name' => 'COMIC Kuriberon', 'link' => ''),
            'megastoredeep' => array ('name' => 'COMIC Megastore Deep', 'link' => ''),
            'megastorea' => array ('name' => 'COMIC Megastore Alpha', 'link' => ''),
            'masyo' => array ('name' => 'COMIC Masyo', 'link' => ''),
            'megastore' => array ('name' => 'COMIC Megastore', 'link' => ''),
            'hotmilk' => array ('name' => 'COMIC Hotmilk', 'link' => ''),
            'omake' => array( 'name' => 'Tankoubon exclusives', 'link' => ''  ),
            'others' => array( 'name' => 'Other works', 'link' => '' ),
            'collaborations' => array( 'name' => 'Collaborations', 'link' => '' ),
										 ),
/* a 'tankoubon' page submenu */
    'tankoubon' => array( 
                                            'caligula' => array( 'name' => 'Caligula Machine', 'link' => ''  ),
											'curriculum' => array( 'name' => 'Curriculum', 'link' => ''  ),
											'operation' => array( 'name' => 'Operation', 'link' => '' ),
											'fever' => array( 'name' => 'Fever', 'link' => '' ),
											'jkpworld' => array( 'name' => "John K. Pe-Ta's World", 'link' => '' ),
											'explosion' => array( 'name' => 'Explosion', 'link' => '' ),
											'license' => array( 'name' => 'License', 'link' => '' ),
											'screamer' => array( 'name' => 'Screamer', 'link' => '' ),
											'curriculumplus' => array( 'name' => 'Curriculum Plus', 'link' => '' ),
											'balkan' => array( 'name' => 'Balkan!!', 'link' => '' ),
											'operationplus' => array( 'name' => 'Operation Plus', 'link' => '' ),
											'monfest' => array( 'name' => 'Monfest XI', 'link' => '' ),
											'feverplus' => array( 'name' => 'Fever Plus', 'link' => '' ),
											'megabitch' => array( 'name' => 'Megabitch', 'link' => '' ),
											'explosionplus' => array( 'name' => 'Explosion Plus', 'link' => '' ),
											'destroy' => array( 'name' => 'Search & Destroy', 'link' => '' ),
											'system' => array( 'name' => 'System!', 'link' => '' ),
											'caligulaplus' => array( 'name' => 'Caligula Machine Plus', 'link' => '' ),
											'miraclehole' => array( 'name' => 'Miracle Hole', 'link' => '' ),
											'maison' => array( 'name' => 'Maison', 'link' => '' ),
											'daigaku' => array( 'name' => 'Private Slug Holes Academy', 'link' => '' ),	
											'acmate' => array( 'name' => 'My Lovely Acmate', 'link' => '' ),	
    								        'opportunity' => array( 'name' => 'Opportunity', 'link' => '' ),
    								        'kimochi' => array( 'name' => 'Every Hole Feels Good', 'link' => '' ),	
    								        'freestyle' => array( 'name' => 'Freestyle', 'link' => '' ),
    								        'mesuana' => array( 'name' => 'Mesu ana Kanojo', 'link' => '' ),
    								        'anameito' => array( 'name' => 'Gucchori ana Meito', 'link' => '' ),
        								    'meathole' => array( 'name' => 'Meat Hole Full Package', 'link' => '' ),	
        								    'shikoana' => array( 'name' => 'Mubobi na Siko Ana', 'link' => '' ),	
										 ),
/* az 'anthology' page submenu */
		'anthology' => array(),
				);